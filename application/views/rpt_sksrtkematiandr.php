<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";


$pr =pg_query($koneksi, "select * from srtkematian where id='$_GET[id]'");
	$r=pg_fetch_array($pr);
	$kdkel=$r['kodekel'];
	
	
?>

<?php 	
	$lurah=pg_query($koneksi, "select * from kelurahan where kode='$kdkel' ");
	$dtlur=pg_fetch_array($lurah);
	
?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman", Times, serif; font-size: 18px; font-style: normal; font-weight: bold; text-align: center; text-decoration: underline; }
body {
	margin-left: 1cm;
}
.style6 {font-size: 18px; font-style: normal; text-align: center; font-family: "Times New Roman", Times, serif;}
.style7 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 24px; font-style: normal; text-align: center; }
</style>
<table width="97%">
  <tr>
    <td><table width="100%">
        
        <tr>
          <td class="style2"><span class="nomor"><img src="../../images/kopsurat.PNG" width="1125" height="198" /></span></td>
        </tr>
        <tr>
          <td class="style2">&nbsp;</td>
        </tr>
        <tr>
          <td class="style2">SURAT KETERANGAN KEMATIAN </td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="nomor"><strong>Nomor :  <?php echo $r['nosurat']; ?></strong></td>
        </tr>
      </table>
      <table width="100%">
        
        

        <tr>
          <td width="400%" colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4"><p>Dengan ini  menerangkan bahwa :</p></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td><table width="100%">
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Nama</td>
              <td>:</td>
              <td><?php echo $r['nama']; ?></td>
            </tr>
            <tr>
              <td width="7%">&nbsp;</td>
              <td width="31%" class="isi">NIK</td>
              <td width="1%">:</td>
              <td><?php echo $r['nik']; ?></td>
            </tr>
			
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Tempat/ Tangal Lahir</td>
              <td>:</td>
              <td><?php echo $r['tempat'];?>/
                <?php $tgllong=$r['tgllahir'];echo tgl_indo($tgllong); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Jenis Kelamin </td>
              <td>:</td>
              <td width="61%"><?php echo $r['jenkel']; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Pekerjaan</td>
              <td>:</td>
              <td><?php echo $r['pekerjaan']; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Status Perkawinan </td>
              <td>:</td>
              <td><?php echo $r['status']; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Agama</td>
              <td>:</td>
              <td><?php echo $r['agama']; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="isi">Kewarganegaraan</td>
              <td>:</td>
              <td><?php echo $r['warga_negara']; ?></td>
            </tr>
            

            
          </table></td>
        </tr>
      </table>
      <table width="100%">
        
        
        <tr>
          <td colspan="4"><div align="justify">Adalah benar penduduk  Kelurahan <?php echo $r['namakel']; ?> Kecamatan <?php echo $r['namakec']; ?> dan  dengan ini menerangkan bahwa nama tersebut  diatas telah meninggal dunia pada : </div></td>
        </tr>
        
        <tr>
          <td>&nbsp;</td>
          <td>Hari</td>
          <td>:</td>
          <td><?php echo $r['harimati']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Tanggal</td>
          <td>:</td>
          <td><?php $tgllong=$r['tglmati'];echo tgl_indo($tgllong); ?></td>
        </tr>
        <tr>
          <td width="7%">&nbsp;</td>
          <td width="31%">Di</td>
          <td width="1%">:</td>
          <td width="61%"><?php echo $r['tempatmati']; ?></td>
        </tr>
        
        <tr>
          <td colspan="4"><p>Keterangan diatas  diperoleh dari :</p></td>
        </tr>
        
        <tr>
          <td>&nbsp;</td>
          <td>Nama</td>
          <td>:</td>
          <td><?php echo $r['namaket']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Instansi</td>
          <td>:</td>
          <td><?php echo $r['instansi']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Hubungan dengan Alm/Almh</td>
          <td>:</td>
          <td><?php echo $r['hubungan']; ?></td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">Demikian surat keterangan ini dibuat untuk  dipergunakan sebagaimana mestinya</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        
        <tr>
          <td rowspan="8"><div align="center">
            <div align="center"><img src="../../temp/srtkematian.png" /></div></td>
          <td colspan="2">&nbsp;</td>
          <td width="5%">Medan</td>
          <td width="1%">,</td>
          <td width="46%"><?php $tgllong=$r['tglsurat'];echo tgl_indo($tgllong); ?></td>
        </tr>
        
        <tr>
          <td width="25%" rowspan="5">&nbsp;</td>
          <td><div align="right"></div></td>
          <td colspan="3"> <strong>LURAH <?php echo $dtlur['nama_kel']; ?></strong></td>
        </tr>
        <tr>
          <td width="4%">&nbsp;</td>
          <td colspan="3"><strong>KECAMATAN <?php echo $dtlur['nama_kec']; ?></strong></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="3"><strong><?php echo $dtlur['nama_lurah']; ?></strong></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="3"><strong><?php echo $dtlur['pangkat_gol']; ?></strong></td>
        </tr>
        <tr>
          <td width="19%">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
          <td colspan="3"><strong>NIP. <?php echo $dtlur['nip_lurah']; ?></strong></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Kematian";
$namafile = "srtkematian.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>