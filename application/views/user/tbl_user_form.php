<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <h4 class="card-title">Management User</h4>
                </div>
                <div class="card-body">
				
                  <div class="toolbar">
			 
            <form action="<?php echo base_url(); ?>User/proses" id="popup-validation" method="post" enctype="multipart/form-data">
			
            
            <!-- /.box-header -->
            <!-- form start -->
             <div class="row">
        
			
             
			  <div class="col-md-8">
			   <div class="box-body">
                <div class="form-group">
                  <label for="nama">Nama Lengkap</label>
                  <input type="text" class="validate[required] form-control" require="true" name="full_name" >
                </div>
				 <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" class="validate[required] form-control"  require="true" name="username" >
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="validate[required] form-control" require="true" name="password" >
                </div>
				<div class="form-group">
                  <label for="Email">Email</label>
                  <input type="email" class="validate[required] form-control" require="true" name="email" >
                </div>
				<div class="form-group">
                  <label for="No Handpgone">No Handphone</label>
                  <input type="number" class="validate[required] form-control" require="true" name="no_hp">
                </div>
				<div class="form-group">
                  <label for="Alamat">Alamat</label>
                  <input type="text" class="validate[required] form-control" require="true" name="alamat" >
                </div>
                <div class="form-group">
                      <label for="Kecamatan" class="bmd-label-floating">Kecamatan</label>
                      <select type="text" id="select1" class="form-control select"  required="true" name="id_kec" style="margin-top:10px">
                        <option></option>
                        <?php 
                        
                        foreach ($kecamatan as $k)
                        {  
                        ?>
                          <option value="<?php echo $k->id_kec; ?>"><?php echo $k->nama_kec; ?></option>
                        <?php } ?>
					            </select>
                  </div>

                    <div class="form-group">
                      <label for="Kelurahan" class="bmd-label-floating">Kelurahan</label>
                      <select type="text" id="select2" class="form-control select"  required="true" name="id_kel" style="margin-top:10px">
                      <option value=""></option>
                      
                      <?php 
                        
                        foreach ($kelurahan as $k)
                        {  
                        ?>
                          <option data-value="<?php echo $k->id_kec; ?>" value="<?php echo $k->id_kel; ?>"><?php echo $k->nama_kel; ?></option>
                        <?php } ?>
					            </select>
                    </div>
				
				<div class="form-group">
					<label>Level User</label>
					<select class="validate[required] form-control select2" require="true" name="level_user" style="width: 100%;">
					  <option></option>
					  <?php foreach($level as $l){  ?>
					  <option value="<?php echo $l->id_user_level; ?>"><?php echo $l->nama_level; ?></option>
					  <?php } ?>
					  
					</select>
				</div>
				
				<div class="form-group">
					<label>Status Aktif</label>
					<select class="validate[required] form-control select2" require="true" name="is_aktif" style="width: 100%;">
					  <option></option>
					  <option value="y">Ya</option>
					 
					  
					</select>
				</div>
				<button type="submit" name="tambah" class="btn btn-primary left"><i class="fa fa-save"></i> Simpan</button>
				<a href="<?php echo base_url() ?>User" class="btn btn-danger"><i class="fa fa-close"></i> Batal</a>
				</div>
				
				<div class="col-md-4">	
				<div class="box-body">	
                <div class="form-group">
				
                 <label class="col-sm-4 control-label">Foto</label>
                  
				  
									
						<div class="btn btn-default btn-file" >
						 <input type="file"  name="images" onchange="readURLUser(this);" /> 
						
										<img id="preview_gambarUser" style="width: 125px; height: 125px" src="<?php echo base_url(); ?>asets/foto_profil/kosong.jpg"  />
									
						 </div>
                  
                </div>
				</div>
				</div>
                
             
			  
			 
              <!-- /.box-body -->
			
	
			
              
           
          
            </div>  
              
            </form>
          
                
				
		</div>
	</section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>
<script>
			//gambar plank
			function readURLUser(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			 
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>	