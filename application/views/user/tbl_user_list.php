
<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
             
			 <?php  
			 
			 $level=$this->session->userdata('id_user_level'); 
			 if($level=='1'){
			 ?>
			
			<div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <h4 class="card-title">Management User</h4>
                </div>
                <div class="card-body">
				
                  <div class="toolbar">
						<?php echo anchor(base_url('user/create'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah User', 'class="btn btn-info"'); ?>
						</div>
						
						<div class="material-datatables">
						<table class="table table-bordered table-hover" id="mydata">
							<thead>
								<tr>
									<th width="20" style="text-align:center">No</th>
									<th>Nama Lengkap</th>
									<th>Username</th>
									<th>Email</th>
									<th>No Handphone</th>
									<th>Alamat</th>
									<th>Kecamatan</th>
									<th>Kelurahan</th>
									<th>Aktif</th>
									<th>Level</th>
									<th width="80" style="text-align:center">Aksi</th>
								</tr>
							</thead>
							<tbody id="show_data">
							
				
							</tbody>
							
						</table>
						</div>
						
					</div>
				</div>
				<?php 
			 } elseif($level=='2') {
			?>	
			 <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <h4 class="card-title">Management User</h4>
                </div>
        
					<div class="box-body">
						<div style="padding-bottom: 10px;">
						
						</div>
						
						<div class="box-body table-responsive no-padding">
						<table class="table table-bordered table-hover" id="mydata_ses">
							<thead>
								<tr>
									<th width="20" style="text-align:center">No</th>
									<th>Nama Lengkap</th>
									<th>Username</th>
									
									<th>Aktif</th>
									<th>Level</th>
									
									<th width="80" style="text-align:center">Aksi</th>
								</tr>
							</thead>
							<tbody id="show_data_ses">
							
				
							</tbody>
							
						</table>
						</div>
						
					</div>
				</div>
			
				<?php
			 } else {
				 echo alert('alert-danger', 'Perhatian', 'Anda Tidak Memiliki Hak Akses');
			 }	 
				?>
            </div>
            </div>
    </section>
</div>
<script src="<?php echo base_url('asets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('asets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('asets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data_user();	
		
		$('#mydata').dataTable();
		 
		//fungsi tampil barang
		function tampil_data_user(){
		    $.ajax({
		        type  : 'Get',
		        url   : '<?php echo base_url()?>User/data_user',
		        async : false,
		        dataType : 'json',
		        success : function(data){
		            var html = '';
		            var i;
					var no=1;
		            for(i=0; i<data.length; i++){
						
		                html += '<tr>'+
								'<td style="text-align:center">'+no+++'</td>'+
		                  		'<td>'+data[i].full_name+'</td>'+
		                        '<td>'+data[i].username+'</td>'+
								'<td>'+data[i].email+'</td>'+
								'<td>'+data[i].no_hp+'</td>'+
								'<td>'+data[i].alamat+'</td>'+
								'<td>'+data[i].nama_kec+'</td>'+
								'<td>'+data[i].nama_kel+'</td>'+
		                        '<td>'+data[i].is_aktif+'</td>'+
								'<td>'+data[i].nama_level+'</td>'+
		                        '<td style="text-align:center;">'+
								'<table>'+ 
										'<tr>'+
											'<td width="100">'+
                                    '<form action="<?php echo base_url() ?>User/edit" method="POST"><input name="id" value="'+data[i].id_users+'" type="hidden"><button name="edit_user"  data-toggle="tooltip" data-placement="top" title="Edit Data User" class="btn btn-primary"><i class="fa fa-edit"></i></button></form>'+' '+
                                    	'</td>'+
										'<td>'+
                                    '<form action="<?php echo base_url() ?>User/hapus" method="POST"><input name="id" value="'+data[i].id_users+'" type="hidden"><button  data-toggle="tooltip" data-placement="top" title="Hapus Data User" class="btn btn-danger"><i class="fa fa-close"></i></button></form>'+' '+
                                    
									        '</td>'+ 
											
								'</tr>'+	
								'</table>'+	
                                '</td>'+
		                        '</tr>';
		            }
		            $('#show_data').html(html);
		        }

		    });
		}

		


		
		


	});

</script>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data_user_ses();	
		
		$('#mydata_ses').dataTable();
		 
		//fungsi tampil barang
		function tampil_data_user_ses(){
		    $.ajax({
		        type  : 'ajax',
		        url   : '<?php echo base_url()?>User/data_user_ses',
		        async : false,
		        dataType : 'json',
		        success : function(data){
		            var html = '';
		            var i;
					var no=1;
		            for(i=0; i<data.length; i++){
		                html += '<tr>'+
								'<td style="text-align:center">'+no+++'</td>'+
		                  		'<td>'+data[i].full_name+'</td>'+
		                        '<td>'+data[i].username+'</td>'+
		                        '<td>'+data[i].is_aktif+'</td>'+
								
		                        '<td style="text-align:center;">'+
								'<table>'+ 
								'<tr>'+
											'<td width="200">'+
                                    '<form action="<?php echo base_url() ?>User/edit" method="POST"><input name="id" value="'+data[i].id_users+'" type="hidden"><button name="edit_user" class="btn btn-primary"><i class="fa fa-edit"></i> Edit </button></form>'+' '+
                                    
									        '</td>'+
											
								'</tr>'+	
								'</table>'+	
                                '</td>'+
		                        '</tr>';
		            }
		            $('#show_data_ses').html(html);
		        }

		    });
		}
	});

</script>


		
        