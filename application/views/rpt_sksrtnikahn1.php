<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";
?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Surat Nikah</title>
<style type="text/css">
.style1 {font-size: 14px}
.judul {	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.style4 {font-size: 12px; font-weight: bold; }

</style>
</head>

<body>
<?php foreach($cetak as $n) {
?>
<table width="99%" border="0">
  <tr>
    <td colspan="6"><div align="right" class="style1">Model N-1</div></td>
  </tr>
  <tr>
    <td width="83"><span class="style1">KELURAHAN</span></td>
    <td width="14">:</td>
    <td width="410"><span class="style1"><?php echo $n->nama_kel; ?></span></td>
  </tr>
  <tr>
    <td><span class="style1">KECAMATAN </span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $n->nama_kec; ?></span></td>
  </tr>
  <tr>
    <td><span class="style1">KOTA</span></td>
    <td>:</td>
    <td><span class="style1">BINJAI</span></td>
  </tr>
 <?php } ?>
</table>

<?php foreach($nomor as $n) {
?>
<table width="99%" border="0">
  <tr>
    <td colspan="4">&nbsp;</td>
    <td width="1">&nbsp;</td>
    <td width="103">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><div align="center"><strong><u>SURAT</u></strong><strong><u> PENGANTAR PERKAWINAN</u></strong></div></td>
  </tr>
  <tr >
      <?php
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $bln = $array_bln[date('n')];
      ?>
    <td colspan="6"><div align="center" style="margin-bottom:10px">Nomor : &nbsp;<?php echo $n->kode; ?> / <?php echo $n->nosurat; ?>/ <?php echo $n->kode_lurah; ?> /<?php echo $bln; ?> /<?php echo date(Y) ?></div></td>
  </tr>
</table>
<?php }?>

<?php foreach($cetak as $n) {
?>
  <table width="99%" border="0">
  <tr>
    <td colspan="6"><div align="center" style="margin-bottom:20px"><span class="style1">Yang bertanda tangan di bawah ini menerangkan  dengan sesungguhnya bahwa :</span></div></td>
  </tr>
  
    <tr>
    <td width="119"><span class="style1"></span></td>
    <td width="151"><span class="style1">1. Nama lengkap </span></td>
    <td width="237"><span class="style1">: <?php echo $n->nama; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
    <td><span class="style1">2. NIK </span></td>
    <td><span class="style1">: <?php echo $n->nik; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Jenis Kelamin </span></td>
      <td><span class="style1">: <?php echo $n->jenkel; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->tempat; ?> </span> / <span class="style1"><?php echo $n->tgllahir; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $n->warga_negara; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Agama</span></td>
      <td><span class="style1">: <?php echo $n->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">7. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->namapekerjaan; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">8. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->alamat; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">9. Status perkawinan</span></td>
      <td><span class="style1">: <?php echo $n->status_perkawinan; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">10. Nama Istri/Suami terdahulu</span></td>
      <td><span class="style1">: <?php echo $n->pasangan_terdahulu == null ? '-' : $n->pasangan_terdahulu; ?></span></td> 
    </tr>
</table>
<br />
<?php } ?>

<?php foreach ($ayah as $a){?>
<table width="99%" border="0">
    <tr>
    <td width="8%"></td>
    <td width="92%" colspan="6"><span class="style1">adalah benar anak dari perkawinan seorang pria :</span></td>
  </tr>
  </table>
  
  <table width="99%" border="0">
    <tr>
    <td width="119"><span class="style1"></span></td>
    <td width="154"><span class="style1">1. Nama lengkap </span></td>
    <td width="234"><span class="style1">: <?php echo $a->nama_ayah; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
    <td><span class="style1">2. NIK </span></td>
    <td><span class="style1">: <?php echo $a->nik_ayah; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $a->tempat_ayah; ?> </span> / <span class="style1"><?php echo $a->tgllahir_ayah; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $a->bangsa_ayah; ?></span></td>
    </tr>
    <tr>
    
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Agama</span></td>
      <td><span class="style1">: <?php echo $a->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $a->namapekerjaan; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">7. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $a->alamat_ayah; ?></span></td>
    </tr>
    </table>
    <br />
    <table width="99%" border="0">
     <tr>
     <td width="8%"></td>
    <td colspan="6"><span class="style1">dengan seorang wanita :</span></td>
  </tr>
  </table>
  <?php } ?>

  <?php foreach ($ibu as $n){?>
  <table width="99%" border="0">
    <tr>
    <td width="124"><span class="style1"></span></td>
    <td width="150"><span class="style1">1. Nama lengkap </span></td>
    <td width="233"><span class="style1">: <?php echo $n->nama_ibu; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
    <td><span class="style1">2. NIK </span></td>
    <td><span class="style1">: <?php echo $n->nik_ibu; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->tempat_ibu; ?> </span> / <span class="style1"><?php echo $n->tgllahir_ibu; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $n->bangsa_ibu; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Agama</span></td>
      <td><span class="style1">: <?php echo $n->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->namapekerjaan; ?></span></td>
    </tr>

    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">7. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->alamat_ibu; ?></span></td>
    </tr>
    </table>
    <?php } ?>

    <?php foreach ($lurah as $n){?>
    <br />
    <table width="99%" border="0">

  <tr>
    <td colspan="6"><div align="center"><span class="style1" style="margin-left:100px">Demikianlah surat pengantar ini dibuat untuk digunakan sebagaimana mestinya.</span></div></td>
  </tr>

  <tr>
    <td colspan="4">&nbsp;</td>
    <td width="5">&nbsp;</td>
    <td width="169">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  
  <tr>
    <td rowspan="7"><p>&nbsp;</p></td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td width="4">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7"><table width="100%" border="0">
      <tr>
        <td><span class="style1">Binjai,  <?php echo date('d  F  Y');?> 
        </span></td>
      </tr>
      <tr>
        <td colspan="2"><span class="style1">LURAH <?php echo $n->nama_kel; ?></span></td>
      </tr>
     
      
      <tr>
        <td>&nbsp;</td>
        
      </tr>
      <tr>
        <td>&nbsp;</td>
        
      </tr>
      <tr>
        <td><span class="style1"><?php echo $n->nama_lurah; ?></span></td>
      </tr>
      
      <tr>
        <td><span class="style1">NIP. <?php echo $n->nip; ?></span></td>
      </tr>
    </table></td>
    <td width="7" rowspan="7"><p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="30" rowspan="7">&nbsp;</td>
    
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<?php } ?>
</body>
</html>
<?php
include "../../phpqrcode/qrlib.php"; 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Nikah";
$namafile = "srtnikah.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


// QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>