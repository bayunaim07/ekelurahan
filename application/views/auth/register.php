<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Daftar Akun</title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">

    <!-- Icons font CSS-->
    <link href="<?php echo base_url(); ?>assets/assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/assets/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/assets/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/main.css">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Pendaftaran Pengguna Aplikasi e-Kelurahan Kota Binjai</h2>
                </div>
                <div class="card-body">
                    <form method="POST" id="reg">
                        <div class="form-row m-b-55">
                            <div class="name">NIK</div>
                            <div class="value">
                                <div class="row row-space">
                                    <div class="col-6">
                                        <div class="input-group-desc">
											<input class="input--style-5" type="text" name="nik" id="nik" maxlength="16">
											<label class="label--desc">NIK (hanya angka)</label>
										</div>
                                    </div>
                                    <div class="col-2">
										<button class="btn btn--radius-2 btn--blue" type="button" id="ceknik" onclick="cek()"> <i class="fa fa-search"></i> Cek NIK</button>
                                    </div>
                                </div>
                            </div>
						</div>
						<div id="cek" style="display : none;">
							<div class="form-row">
								<div class="name">Nama</div>
								<div class="value">
									<div class="input-group">
										<input class="input--style-5" type="text" name="nama" readonly>
									</div>
								</div>
								
							</div>
							
								
								<div class="value">
									<div class="input-group">
										<input class="input--style-5" type="hidden" name="kel" readonly>
									</div>
								</div>
								
								<div class="value">
									<div class="input-group">
										<input class="input--style-5" type="hidden" name="kec" readonly>
									</div>
								</div>
								
							
							<div class="form-row">
								<div class="name" data-validate="Email harus diisi">Email</div>
								<div class="value">
									<div class="input-group-desc">
										<input class="form-control input--style-5" type="email" name="email">
										<label class="label--desc label-email"></label>
									</div>
									
								</div>
							</div>
							<div class="form-row">
								<div class="name" data-validate="Password harus diisi">Password</div>
								<div class="value">	
									<div class="input-group-desc">
										<input class="form-control input--style-5"  type="password" name="password">
										<label class="label--desc label-password"></label>
									</div>
									
								</div>
							</div>
							<div style="text-align:center;">
								<button class="btn btn--radius-2 btn--blue daftar" type="button"><i class="fa fa-telegram"></i> Daftar</button>
							</div>
						</div>
						<div style="text-align:center;">
							<br><a href="<?php echo base_url(); ?>beranda"><button class="btn btn--radius-2 btn--green" type="button">Kembali</button></a>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="<?php echo base_url(); ?>assets/assets/vendor/select2/select2.min.js"></script>

    <!-- Main JS-->

</body>

</html>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
	$(document).ready(function () {
		$('#cek').hide();
	});
	$(document).on('keyup','#nik',function () { 
		$('#cek').hide();
		$('[name="nama"]').val('');
		$('[name="kel"]').val('');
		$('[name="kec"]').val('');
		$('[name="email"]').val('');
		$('[name="password"]').val('');
	});

	var input = document.getElementById("nik");
	input.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("ceknik").click();
		}
	});

	function cek(){
		Nik = $('[name="nik"]').val();
		$.ajax({
			url: 'ceknik',
			data: {Nik:Nik},
			dataType: "JSON",
			type: "POST",
			success: function(data){ 
				if(data != null){
					if(data.password != null){
						Swal.fire({
							type: 'error',
							title: 'Data Sudah Digunakan',
							text: 'Data NIK tersebut sudah memiliki akun',
						});
						$('#cek').hide();
					}
					else{
						$('[name=nama]').val(data.nama);
						$('[name=kel]').val(data.kodekel);
						$('[name=kec]').val(data.kodekec);
						$('#cek').show();
					}
				}
				else{
					Swal.fire({
						type: 'error',
						title: 'Tidak Ditemukan',
						text: 'Data NIK tidak ditemukan',
					});
					$('#cek').hide();
				}
			}, error: function(xhr, stat, err){
				Swal.fire({
						type: 'error',
						title: 'Tidak Ditemukan',
						text: 'Data NIK tidak ditemukan',
					});
				$('#cek').hide();
			}
		})
	}

	$('.daftar').click(function () {
		var form = $('#reg');
		Swal.fire({
			title: "Yakin data yang diisi sudah benar?",
			text: "Data tidak bisa diubah lagi!",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "Tidak!",
			confirmButtonText: "Ya!",
		}).then((result) => {
			console.log(result);
			if (result.value) {
				$('.label-email').html('');
				$('.label-password').html('');

				$.ajax({
					url: 'daftar',
					data: form.serialize(),
					dataType: "JSON",
					async : false,
					type: "POST",
					error: function() {
						
					},
					success: function(data) {
						if (data.status){
							swal({
								title: "Data Pengguna Berhasil Disimpan!",
								text: "Silahkan LOGIN Aplikasi",
								type: "success"
							}).then(function() {
								window.location = "<?php echo base_url(); ?>Home";
							});
						}else{
							if (data.errvalidation){
								$.each(data.validationitem, function(index, value){
									$('.label-' + index).html(value);
								});
							}
							swal({
								title: "Data Pengguna Gagal Disimpan!",
								text: data.message,
								type: "error"
							});
						}
					}
				});
			} 
			else {
				swal("Batal", "", "error");
			}
		});
	});	


</script>