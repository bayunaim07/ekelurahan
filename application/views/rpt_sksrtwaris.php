<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";
?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman", Times, serif; font-size: 18px; font-style: normal; font-weight: bold; text-align: center; text-decoration: underline; }
body {
	margin-left: 1cm;
}
.style7 {font-size: 16px; }
.style8 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 24px; font-style: normal; text-align: center; }
.style3 {font-family: Arial, Helvetica, sans-serif;
	font-size: 20pt;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style9 {font-size: 20pt;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
}
.tanggal {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
}
.ttd {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
}
</style>
<?php foreach($cetak as $n) {} 
?>
<table width="98%">
  <tr>
    <td>
      <table width="99%">
        <tr>
          <td class="nomor"><img src="<?php echo base_url() ?>images/kop_surat/<?php echo $n->kop; ?>" width="977" height="250" /></td>
        </tr>
        <tr>
          <td class="nomor">&nbsp;</td>
        </tr>
        <tr>
          <td class="nomor"><span class="style3">SURAT KETERANGAN AHLI WARIS</span></td>
        </tr>
        <tr>
            <?php
              $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $bln = $array_bln[date('n')];
            ?>
          <td class="nomor"><strong class="style9">Nomor : <?php echo $n->kode; ?> / <?php echo $n->nosurat; ?> / <?php echo $n->kode_lurah; ?>/ <?php echo $bln; ?> /<?php echo date(Y) ?></strong></td>
        </tr>
      </table>
      <table width="99%">
        <tr>
          <td class="isi">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" colspan="4"><p align="justify"><span class="style9" style="margin-left:70px">Lurah <?php echo $n->nama_kel;?> Kecamatan <?php echo $n->nama_kel; ?> Pemerintah Kota Binjai, dengan ini menerangkan Ahli Waris bersangkutan benar telah membuat Surat Pernyataan Ahli Waris Tanggal <?php $date = date_create($n->tglsurat); echo date_format($date, "d-m-Y");?> yang menyatakan bahwa Alm. <?php echo $n->nama; ?> telah meninggal dunia pada Tanggal <?php $date = date_create($n->tglmati_ayah); echo date_format($date, "d-m-Y");?> di <?php echo $n->tempat_kematian;?> dan semasa hidupnya telah melangsungkan perkawinan dengan seorang perempuan bernama Almh. <?php echo $n->nama_ibu; ?> yang juga telah meninggal dunia pada Tanggal <?php $date = date_create($n->tglmati_ibu); echo date_format($date, "d-m-Y");?></span></p></td>
        </tr>
        
      </table>
      
      <table width="99%">
        <tr>
          <td colspan="2"><p align="justify"><span class="style9" style="margin-left:70px">Dari perkawinan tersebut telah dikaruniai <?php echo $n->jlhanak ?> orang anak kandung dan sesuai pernyataannya selaku Ahli Waris dari Alm. <?php echo $n->nama;?> dan Almh. <?php echo $n->nama_ibu;?> :</span></p></td>
        </tr>
        	<tr>
              <td width="8%">&nbsp;</td>
              <td width="91%" class="isi">&nbsp;</td>
              <td width="0%">&nbsp;</td>
              <td width="1%">&nbsp;</td>
           </tr>
		</table>
		<table border="1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
      <thead>
        <tr>
          <th width="50"><span class="style9">No</span></th>
          <th><span class="style9">Nama/NIK </span></th>
          <th><span class="style9">Jenis Kelamin<span></th>
          <th><span class="style9" width="100px">Tempat/Tgl. Lahir</span></th>
          <th><span class="style9">Alamat</span></th>
          <th><span class="style9">Status</span></th>
        </tr>
      </thead>
      
      <tbody >
      <?php $no = 1; ?>		
      <?php foreach ($cetak as $m) : ?>
        
          <tr>
              <th scope="row"><span class="style9"><?= $no++; ?></span></th>
              <td><span class="style9"><?= $m->nama;?> / <?=$m->nik; ?><span></td>
              <td><span class="style9"><?= $m->jenkel;?><span></td>
              <td><span class="style9"><?= $m->tempat ; ?> / <?php $date = date_create($n->tgllahir); echo date_format($date, "d-m-Y");?></span></td>
              <td><span class="style9"><?= $m->alamat; ?></span></td>
              <td><span class="style9"><?= $m->status; ?></span></td>
          </tr>
      <?php endforeach; ?>			
			</tbody>
		</table>
    <table>
        <tr>
          <td width="1106" colspan="2"><p align="justify"><span class="style9" style="margin-left:70px">Surat Keterangan ini diperbuat sesuai permintaan ahli waris untuk mengurus berkas <?php $n->peruntukan;?></span></p></td>
        </tr>
    </table>
		<table>
        <tr>
          <td width="1106" colspan="2"><p align="justify"><span class="style9" style="margin-left:70px">Demikian Surat Keterangan Ahli Waris ini diperbuat, dan apabila dikemudian hari terdapat ahli waris lainnya maka Surat Keterangan Ahli Waris ini batal demi hukum dan akan diperbaiki sebagaimana mestinya.</span></p></td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td colspan="4">&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="4">&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="4">&nbsp;</td>
        </tr>
	      <tr>
	        <td class="style9">Dicatat Oleh</td>
	        <td class="style9">:</td>
	        <td class="style9">CAMAT <?php echo $n->nama_kec;?></td>
	        <td class="style9">Binjai, <?php $date = date_create($n->tgl_surat); echo date_format($date, "d-m-Y");?></td>
        </tr>
	      <tr>
	        <td class="style9">Nomor Agenda</td>
	        <td class="style9">:</td>
	        <td class="style9">&nbsp;</td>
	        <td class="style9">LURAH <?php echo $n->nama_kel;?></td>
        </tr>
	      <tr>
	        <td width="21%" class="style9">Tanggal</td>
	        <td width="2%" class="style9">:</td>
	        <td width="40%" class="style9">&nbsp;</td>
	        <td width="37%">&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="3">&nbsp;</td>
	        <td>&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="3">&nbsp;</td>
	        <td>&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="3">&nbsp;</td>
	        <td>&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="3">&nbsp;</td>
	        <td>&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="3">&nbsp;</td>
	        <td>&nbsp;</td>
        </tr>
	      <tr>
	        <td colspan="3" class="style9"><?php echo $n->nama_camat; ?></td>
	        <td class="style9"><?php echo $n->nama_lurah; ?></td>
        </tr>
	      <tr>
	        <td colspan="3" class="style9"><?php echo $n->pangkat_camat; ?></td>
	        <td class="style9"><?php echo $n->jabatan; ?></td>
        </tr>
	      <tr>
	        <td colspan="3" class="style9"><?php echo $n->nip; ?></td>
	        <td class="style9"><?php echo $n->nip_camat; ?></td>
        </tr>
	      <tr>
	        <td colspan="3">&nbsp;</td>
	        <td>&nbsp;</td>
        </tr>
    </table>
    </td>
  </tr>
</table>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Waris";
$namafile = "srtblmnikah.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;



?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>