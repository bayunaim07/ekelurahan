<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";


$pr =pg_query($koneksi, "select * from srttdkmampu where id='$_GET[id]'");
	$r=pg_fetch_array($pr);
	$kdkel=$r['kodekel'];
	
	
?>

<?php 	
	$lurah=pg_query($koneksi, "select * from kelurahan where kode='$kdkel' ");
	$dtlur=pg_fetch_array($lurah);
	
?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {
	font-size: 20pt;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 20pt;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
body {
	margin-left: 1cm;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18pt;
}
.style6 {font-size: 18px; font-style: normal; text-align: center; font-family: "Times New Roman", Times, serif;}
.style7 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 24px; font-style: normal; text-align: center; }
.ttd {
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
}
.tanggal {
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
}
.barcode {
	font-size: 20pt;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
<body class="barcode"><table width="90%">
  <tr>
    <td><table width="100%">
      
    </table>
      <table width="100%">
        
        <tr>
          <td class="style2"><span class="nomor"><img src="../../images/kopsurat.PNG" width="1125" height="250" /></span></td>
        </tr>
        <tr>
          <td class="style2"></td>
        </tr>
        <tr>
          <td class="style2"></td>
        </tr>
        <tr>
          <td class="style2"></td>
        </tr>
        <tr>
          <td class="style2"></td>
        </tr>
        <tr>
          <td class="style2">SURAT KETERANGAN KURANG MAMPU </td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="nomor"><strong class="style1">Nomor :  <?php echo $r['nosurat']; ?></strong></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="isi">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        

        <tr>
          <td width="100%" colspan="4"><p align="justify" class="style1">Lurah Sukaraja&nbsp; Kecamatan Medan Maimun Pemerintah Kota Medan,  dengan ini menerangkan bahwa :</p></td>
        </tr>
        
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td><table width="100%">
            <tr>
              <td width="8%">&nbsp;</td>
              <td><span class="style1">Nama</span></td>
              <td width="2%"><span class="style1">:</span></td>
              <td><span class="style1"><?php echo $r['nama']; ?></span></td>
            </tr>
            
			
            <tr>
              <td>&nbsp;</td>
              <td><span class="style1">Tempat/ Tanggal Lahir</span></td>
              <td><span class="style1">:</span></td>
              <td><span class="style1"><?php echo $r['tempat'];?>/
                <?php $tgllong=$r['tgllahir'];echo tgl_indo($tgllong); ?>
              </span></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td width="36%"><span class="style1">Nomor Induk Kependudukan</span></td>
              <td><span class="style1">:</span></td>
              <td><span class="style1"><?php echo $r['nik']; ?></span></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><span class="style1">Kewarganegaraan / Agama</span></td>
              <td><span class="style1">:</span></td>
              <td width="54%"><span class="style1"><?php echo $r['warga_negara']; ?> / <?php echo $r['agama']; ?></span></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td><span class="style1">Pekerjaan</span></td>
              <td><span class="style1">:</span></td>
              <td><span class="style1"><?php echo $r['pekerjaan']; ?></span></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td><span class="style1">Alamat</span></td>
              <td><span class="style1">:</span></td>
              <td><span class="style1"><?php echo $r['alamat']; ?></span></td>
            </tr>
            

            <tr>
              <td>&nbsp;</td>
              <td class="isi">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            
            
          </table></td>
        </tr>
      </table>
      <table width="100%">
        
        
        <tr>
          <td width="100%" colspan="2"><div align="justify">
            <p class="style1">Sesuai dengan data-data yang ada pada kami, nama  tersebut diatas benar penduduk Kelurahan Sukaraja Kecamatan Medan Maimun Pemerintah  Kota Medan, dan  sampai saat ini masih berdomisili pada alamat tersebut diatas.</p>
          </div></td>
        </tr>
        <tr>
          <td colspan="2"><div align="justify" class="style1">Selanjutnya  disebutkan bahwa nama tersebut diatas benar orang kurang mampu/miskin. Surat  Keterangan ini diberikan Kepada yang bersangkutan untuk <?php echo $r['peruntukan']; ?></div></td>
        </tr>
        <tr>
          <td colspan="2"><p align="justify" class="style1">Demikian Surat Keterangan ini  diperbuat untuk dapat dipergunakan seperlunya. </p></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        
        <tr>
          <td rowspan="8"><div align="center">
            <div align="center" class="style1"><img src="../../temp/srttdkmampu.png" /></div></td>
          <td colspan="2">&nbsp;</td>
          <td width="39%" class="tanggal">Medan,
          <?php $tgllong=$r['tglsurat'];echo tgl_indo($tgllong); ?></td>
        </tr>
        
        <tr>
          <td width="33%" rowspan="6">&nbsp;</td>
          <td></td>
          <td class="ttd"> <strong>LURAH <?php echo $dtlur['nama_kel']; ?></strong></td>
        </tr>
        <tr>
          <td width="9%">&nbsp;</td>
          <td class="ttd"><strong>KECAMATAN <?php echo $dtlur['nama_kec']; ?></strong></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="ttd">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="ttd">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="ttd">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="ttd">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td class="ttd"><strong><?php echo $dtlur['nama_lurah']; ?></strong></td>
        </tr>
        <tr>
          <td width="19%">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
          <td class="ttd"><strong>NIP. <?php echo $dtlur['nip_lurah']; ?></strong></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Kurang Mampu";
$namafile = "srttdkmampu.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>