<?php  
	$level=$this->session->userdata('id_user_level'); 
		if($level=='2'){
?>
<div class="container-fluid">
<div class="row">
            <div class="col-md-12">
			
              <div class="page-categories">
			   <div class="card">
               <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon card-header-danger">
                    <a href="<?php echo base_url() ?>Arsip"><i class="material-icons text-white">close</i></a>
                  </div>
				   <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
				  
                  <h4 class="card-title">Surat Cerai</h4>
				   <br>
                </div>
                  <div class="tab-pane" id="link8">
                    <div class="card">
                      <div class="card-header">
                        <div class="toolbar">
						  </div>
                        <p class="card-category">
						<div class="material-datatables">
							<table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							  <thead>
								<tr>
								<th>No</th>
								<th>Tgl.Daftar</th>
								<th>No.Reg</th>
								<th>Nama Lengkap</th>
								<th>Tanggal Selesai</th>
								</tr>
							  </thead>
							 
							  <tbody id="show_data">
								
								
							  </tbody>
							</table>
						  </div>

                        </p>
                      </div>
                      
                    </div>
                  </div>
                  
                 
                </div>
				</div>
              </div>
			  
			  
            </div>
          </div>
          
          <!-- end row -->
        </div>
<script src="<?php echo base_url('asets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('asets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('asets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data();	
		
		$('#mydata_admin').dataTable();
		 
		
		function tampil_data(){
		    $.ajax({
		        type  : 'GET',
		        url   : '<?php echo base_url();?>Arsip/surat_cerai',
		        async : false,
		        dataType : 'json',
		        success : function(data){
		            var html = '';
		            var i;
					var no=1;
		            for(i=0; i<data.length; i++){
					var ket = data[i].cekopt;
					var lurah = data[i].ceklurah;
		                html += '<tr>'+
								'<td style="text-align:center">'+no+++'</td>'+
		                  		'<td>'+data[i].tgldaftar+'</td>'+
		                        '<td>'+data[i].noreg+'</td>'+
								'<td>'+data[i].nama+'</td>'+
								'<td>'+data[i].tglcetak+'</td>';
								
								'</tr>'+	
								'</table>'+	
                                '</td>'+
		                        '</tr>';
							
		            }
		            $('#show_data').html(html);
		        }

		    });
		}
	});

	

</script>		
<?php }?>