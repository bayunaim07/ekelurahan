<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Profil/simpan" method="POST" >
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">person_add</i>
                    </div>
                    <h4 class="card-title">Tambah Profil</h4>
                  </div>
                  <div class="card-body ">
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Nomor Kartu Keluarga</label>
                      <input type="text" class="form-control" name="kk" required="true">
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK</label>
                      <input type="text" class="form-control"  required="true" name="nik">
                    </div>
                    
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Nama</label>
                      <input type="text" class="form-control" name="nama" required="true">
                    </div>

                    
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Tempat Lahir</label>
                      <input type="text" class="form-control" name="tempat" required="true">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating" style="margin-top:-20px">Tanggal Lahir</label>
                      <input type="date" class="form-control" name="tgllahir"  required="true">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating" >Jenis Kelamin</label>
                      <select type="text" class="validate[required] form-control select2" style="width:100%" name="jenkel" required> 
                          <option></option>
								          <option value='Pria'> Pria </option>
                          <option value='Wanita'> Wanita </option>
                        </select>
                    </div> 

                    <div class="">
                      <label for="" class="bmd-label-floating" style="margin-top:-8px"> Agama </label>
                      <select type="text" class="validate[required] form-control select2" name="agama"  required> 
                          <option></option>
								          <option value='Islam'> Islam </option>
                          <option value='Protestan'> Protestan </option>
                          <option value='Katolik'> Katolik </option>
                          <option value='Hindu'> Hindu </option>
                          <option value='Buddha'> Buddha </option>
                          <option value='Khonghucu'> Khonghucu </option>
                          <option value='Lainnya'> Lainnya </option>  
                        </select>
                    </div>
                    
                   
                   
                    
                  <div class="category form-category text-danger">* Wajib Diisi</div>
                  
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                    <a href="<?php echo base_url(); ?>Profil" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="simpan" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
            
            
            
          </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
