<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <?php if($users==null){?>
                <form action="<?php echo base_url(); ?>Profil/simpan" id="popup-validation" method="post">
                <input type="hidden" class="form-control" name="id_pro"  >  
                  <div class="card ">
                    <div class="card-header card-header-success card-header-icon">
                      <div class="card-icon">
                        <i class="material-icons">person</i>
                      </div>
                      <h4 class="card-title">Profil</h4>
                    </div>
              
                    <div class="card-body ">
                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Nomor Kartu Keluarga</label>
                        <input type="text" class="form-control" name="kk"   >
                      </div>
                      <div class="form-group">
                        <label for="" class="bmd-label-floating"> NIK</label>
                        <input type="text" class="form-control"  name="nik" value="<?php echo $this->session->userdata('username'); ?>" readonly>
                      </div>
                      
                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Nama</label>
                        <input type="text" class="form-control" name="nama"  >
                      </div>

                      
                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat"   >
                      </div>

                      <div class="form-group">
                        <label for="" class="" >Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tgllahir"   >
                      </div>

                      <div class="form-group">
                        <label for="" class="bmd-label-floating" >Jenis Kelamin</label>
                        <select type="text" class="validate[required] form-control select2" style="width:100%" name="jenkel" > 
                            <option value='Pria'> Pria </option>
                            <option value='Wanita'> Wanita </option>
                          </select>
                      </div> 

                      <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                        <select type="text" class="form-control select2"  required="true" name="agama" >
                        <option></option>
                        <?php 
                          $this->db->select('*');
                          $agama=$this->db->get('mstagama');
                          foreach ($agama->result() as $a)
                          {  
                          ?>
                            <option value="<?php echo $a->id ?>"><?php echo $a->agama; ?></option>
                          <?php } ?>	
                        </select>

                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Kewarganegaraan</label> 
                        <input type="text" class="form-control" name="bangsa"   >
                      </div>

                      
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                    
                    <div class="card-footer text-right">
                      <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Profil" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                      </div>
                      <button name="simpan" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                    </div>
                  </div>
                </form>
                <?php } else { ?>
                <?php foreach ($users as $p){?>
                <form action="<?php echo base_url(); ?>Profil/edit" id="popup-validation" method="post">
                <input type="hidden" class="form-control" value="<?php echo $p->id_pro; ?>"  name="id_pro" >
                  <div class="card ">
                    <div class="card-header card-header-success card-header-icon">
                      <div class="card-icon">
                        <i class="material-icons">person</i>
                      </div>
                      <h4 class="card-title">Profil</h4>
                    </div>
              
                    <div class="card-body ">
                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Nomor Kartu Keluarga</label>
                        <input type="text" class="form-control" name="kk"  value="<?php echo $p->kk;?>" >
                      </div>
                      <div class="form-group">
                        <label for="" class="bmd-label-floating"> NIK</label>
                        <input type="text" class="form-control"  name="nik" value="<?php echo $p->nik; ?> " readonly>
                      </div>
                      
                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Nama</label>
                        <input type="text" class="form-control" name="nama"  value="<?php echo $p->nama ?>">
                      </div>

                      
                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat"   value="<?php echo $p->tempat ?>">
                      </div>

                      <div class="form-group">
                        <label for="" class="">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tgllahir"   value="<?php echo $p->tgllahir; ?>">
                      </div>

                      <div class="form-group">
                        <label for="" class="bmd-label-floating" >Jenis Kelamin</label>
                        <select type="text" class="form-control select2"  required="true" name="jenkel" >
                        <option value="<?php echo $p->jenkel; ?>"><?php echo $p->jenkel; ?></option>
                        <option value="Laki-Laki">Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                      </div> 

                      <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama" >
					 	            <?php foreach ($agama as $a) { ?>
                          <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                        <?php } ?>
					            </select>
                    </div>

                      <div class="form-group">
                        <label for="" class="bmd-label-floating">Kewarganegaraan</label>
                        <input type="text" class="form-control" name="bangsa" value="<?php echo $p->bangsa; ?>">
                      </div>
                      
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                    
                    <div class="card-footer text-right">
                      <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Profil" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                      </div>
                      <button name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                    </div>
                  </div>
                </form>
                <?php } ?>
              <?php } ?>

            </div>
          </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>