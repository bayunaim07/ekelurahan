 <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Profil/edit" method="POST">
              <input type="hidden" class="form-control" value="<?php echo $edit['id_pro']; ?>"  name="id_pro" >
              <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">edit</i>
                    </div>
                    <h4 class="card-title">Edit Data Profil</h4>
                  </div>
                  <div class="card-body ">
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Nomor Kartu Keluarga</label>
                      <input type="text" class="form-control" name="kk" value="<?php echo $edit['kk']; ?>" required="true">
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK</label>
                      <input type="text" class="form-control" value="<?php echo $edit['nik']; ?>"  required="true" name="nik">
                    </div>
                    
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Nama</label>
                      <input type="text" class="form-control" name="nama" value="<?php echo $edit['nama']; ?>" required="true">
                    </div>

                    
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Tempat Lahir</label>
                      <input type="text" class="form-control" name="tempat" value="<?php echo $edit['tempat']; ?>" required="true">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating" style="margin-top:-20px">Tanggal Lahir</label>
                      <input type="date" class="form-control" name="tgllahir" value="<?php echo $edit['tgllahir']; ?>" required="true">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating" >Jenis Kelamin</label>
                      <select type="text" class="validate[required] form-control select2" style="width:100%" name="jenkel" required> 
                          <option value="<?php echo $edit['jenkel']; ?>"> <?php echo  $edit['jenkel']; ?></option>
								          <option value='Pria'> Pria </option>
                          <option value='Wanita'> Wanita </option>
                        </select>
                    </div> 

                    <div class="">
                      <label for="" class="bmd-label-floating" style="margin-top:-8px"> Agama </label>
                      <select type="text" class="validate[required] form-control select2" name="agama"  required> 
                      <option value="<?php echo $edit['agama']; ?>"> <?php echo  $edit['agama']; ?></option>
								          <option value='Islam'> Islam </option>
                          <option value='Protestan'> Protestan </option>
                          <option value='Katolik'> Katolik </option>
                          <option value='Hindu'> Hindu </option>
                          <option value='Buddha'> Buddha </option>
                          <option value='Khonghucu'> Khonghucu </option>
                          <option value='Lainnya'> Lainnya </option>  
                        </select>
                    </div>
                  
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Profil" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
                                   
                
              </form>
            </div>
            
            
            
          </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script type="text/javascript">
function PreviewImage() {
var oFReader = new FileReader();
oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
oFReader.onload = function (oFREvent)
 {
    document.getElementById("uploadPreview").src = oFREvent.target.result;
};
};
</script>
