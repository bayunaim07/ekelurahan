<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";

?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman", Times, serif; font-size: 18px; font-style: normal; font-weight: bold; text-align: center; text-decoration: underline; }
body {
	margin-left: 1cm;
}
.style7 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 24px; font-style: normal; text-align: center; }
.style3 {font-family: Arial, Helvetica, sans-serif;
	font-size: 20pt;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style8 {font-size: 20pt;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
}
.tanggal {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
}
.ttd {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
}
</style>

<?php foreach($cetak as $n) {} 
?>
<table width="97%">
  <tr>
    <td><table width="100%">
     
    </table>
      <table width="100%">
        
        <tr>
          <td class="style2"><span class="nomor"><img src="../images/kop_surat/kop.png" width="1125" height="250" /></span></td>
        </tr>
        <tr>
          <td class="style2">&nbsp;</td>
        </tr>
        <tr>
          <td class="style2"><span class="style3">SURAT KETERANGAN</span></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="nomor"><strong class="style8">Nomor : <?php echo $n->nosurat; ?></strong></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="isi">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        

        <tr>
          <td width="100%" colspan="4"><p align="justify"><span class="style8">Lurah <?php echo $n->nama_kel; ?>&nbsp; Kecamatan <?php echo $n->nama_kec; ?> Pemerintah Kota Binjai,  dengan ini menerangkan bahwa :</span></p></td>
        </tr>
        
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td><table width="100%">
            <tr>
              <td width="8%">&nbsp;</td>
              <td class="isi"><span class="style8">Nama Lengkap </span></td>
              <td width="2%" class="style8">:</td>
              <td class="style8"><?php echo $n->nama; ?></td>
            </tr>
            
			
            <tr>
              <td>&nbsp;</td>
              <td class="isi"><span class="style8">Tempat/ Tangal Lahir</span></td>
              <td class="style8">:</td>
              <td class="style8"><?php echo $n->tempat;?>/
                <?php $n->tgllahir; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td width="34%" class="isi"><span class="style8">Nomor Induk Kependudukan</span></td>
              <td class="style8">:</td>
              <td class="style8"><?php echo $n->nik; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="style8">Kewarganegaraan / Agama</td>
              <td class="style8">:</td>
              <td width="56%" class="style8"><?php echo $n->warga_negara; ?> / <?php echo $n->agama; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style8">Pekerjaan</td>
              <td class="style8">:</td>
              <td class="style8"><?php echo $n->namapekerjaan; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style8">Alamat</td>
              <td class="style8">:</td>
              <td class="style8"><?php echo $n->alamat; ?></td>
            </tr>
            

            <tr>
              <td>&nbsp;</td>
              <td class="isi">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            
            
          </table></td>
        </tr>
      </table>
      <table width="100%">
        
        
        <tr>
          <td width="100%" colspan="2"><div align="justify">
            <p class="style8"><?php echo $n->ket1; ?>.</p>
          </div></td>
        </tr>
        <tr>
          <td colspan="2"><div align="justify" class="style8"><?php echo $n->ket2; ?>.</div></td>
        </tr>
        <tr>
          <td colspan="2"><p align="justify" class="style8">Demikian Surat Keterangan ini  diperbuat untuk dapat dipergunakan seperlunya. </p></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        
        <tr>
          <td rowspan="9"><div align="center">
            <div align="center"><img src="../../temp/srtketerangan.png" /></div></td>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><span class="tanggal">Binai,
            <?php $n->tglsurat; ?>
          </span></div></td>
        </tr>
        
        <tr>
          <td width="33%" rowspan="5">&nbsp;</td>
          <td><div align="right"></div></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td><div align="center"><strong class="ttd">LURAH <?php echo $n->nama_kel; ?></strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div align="center"><strong class="ttd">KECAMATAN <?php echo $n->nama_kec; ?></strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><strong><?php echo $n->nama_lurah; ?></strong></div></td>
        </tr>
        <tr>
          <td width="19%">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><strong class="ttd">NIP. <?php echo $n->nip_lurah; ?></strong></div></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan";
$namafile = "srtketerangan.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


// QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>