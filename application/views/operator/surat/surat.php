<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon card-header-rose">
                    <a href="<?php echo base_url() ?>Home"><i class="material-icons text-white">home</i></a>
                  </div>
				  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
				  
                  <h4 class="card-title">Surat</h4>
                </div>
                <div class="card-body">
				<div class="col-md-12">
					<select name="surat" onchange="window.location='<?php echo base_url() ?>Verifikasi_surat/'+this.value" class="form-control select2" data-style="btn btn-primary btn-round" title="Pilih Jenis Surat">
					<option></option>
					<?php foreach($surat as $s){ ?>
					<option value="<?php echo $s->modul1; ?>"><?php echo $s->nama_surat; ?></option>
					
					<?php } ?>
					</select>
                </div>  
                  
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>