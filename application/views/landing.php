<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>E-Kelurahan Pemerintah Kota Binjai</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/css/img/binjai_s.png" rel="icon" >
  <link href="assets/css/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/css/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/css/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/css/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/css/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/css/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/timeline.min.css" />

  <!-- Template Main CSS File -->
  <link href="assets/css/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<style>
    .timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;
    }

    .timeline:before {
    top: 80px;
    bottom: 90px;
    position: absolute;
    content: " ";
    width: 3px;
    background-color: #eeeeee;
    left: 50%;
    margin-left: -1.5px;
    }

    .timeline > li {
    margin-bottom: 20px;
    position: relative;
    }

    .timeline > li:before,
    .timeline > li:after {
    content: " ";
    display: table;
    }

    .timeline > li:after {
    clear: both;
    }

    .timeline > li:before,
    .timeline > li:after {
    content: " ";
    display: table;
    }

    .timeline > li:after {
    clear: both;
    }

    .timeline > li > .timeline-panel {
    width: 46%;
    float: left;
    border: 1px solid #d4d4d4;
    border-radius: 2px;
    padding: 20px;
    position: relative;
    -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline > li > .timeline-badge {
    color: #fff;
    width: 50px;
    height: 50px;
    line-height: 50px;
    font-size: 1.4em;
    text-align: center;
    position: absolute;
    top: 33%;
    left: 50%;
    margin-left: -25px;
    background-color: #999999;
    z-index: 100;
    border-top-right-radius: 50%;
    border-top-left-radius: 50%;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    }

    .timeline > li.timeline-inverted > .timeline-panel {
    float: right;
    }

    .timeline > li.timeline-inverted > .timeline-panel:before {
    border-left-width: 0;
    border-right-width: 15px;
    left: -15px;
    right: auto;
    }

    .timeline > li.timeline-inverted > .timeline-panel:after {
    border-left-width: 0;
    border-right-width: 14px;
    left: -14px;
    right: auto;
    }

    .timeline-heading {
    width: 70%;
    }
    .timeline-clock {
    width: 25%;
    }

    .timeline-title {
    margin-top: 0;
    color: inherit;
    }

    .timeline-body > p,
    .timeline-body > ul {
    margin-bottom: 0;
    }

    .timeline-body > p + p {
    margin-top: 5px;
    }

    .flex-container {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
            flex-flow: row wrap;
    }

    .flex-around {
    -ms-flex-pack: distribute;
        justify-content: space-around;
    }

    .flex-between {
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
    }

    .flex-start {
    -webkit-box-pack: start;
        -ms-flex-pack: start;
            justify-content: flex-start;
    }

    .flex-end {
    -webkit-box-pack: end;
        -ms-flex-pack: end;
            justify-content: flex-end;
    }

    .flex-around {
    -ms-flex-pack: distribute;
        justify-content: space-around;
    }

    .flex-center {
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
    }

    .align-items-center {
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    }

    .responsive {
     width: 100%;
     max-width: 1300px;
     height: auto;
  }
  .img-responsive {
     width: 100%;
     max-width: 900px;
     height: auto;
  }

</style>

<body>
    <nav class="navbar navbar-static-top" style="background-color: #f7f3f3" >
      <div class="navbar-header">
        <img class="responsive"  src="<?php echo base_url() ?>images/apemja.png"  />
   </div>
   </nav>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center">
      <h1 class="logo me-auto"><a href="beranda">E-Kelurahan</a></h1>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="#about">Tentang</a></li>
          <li><a class="nav-link scrollto" href="#portfolio">Alur</a></li>
          <li><a class="nav-link scrollto" href="#services">Rekapitulasi</a></li>
          <li><a class="nav-link scrollto" href="#pricing">Tracking</a></li>
          <li><a class="getstarted scrollto" href="login">Masuk</a></li>
          <li><a class="getstarted scrollto" href="login/register">Daftar</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Aplikasi e-Kelurahan Pemerintah Kota Binjai</h1>
          <h2>Selamat datang masyarakat Kota Binjai</h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="login" class="btn-get-started scrollto">Masuk</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/css/img/hero-img.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Tentang Aplikasi</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p style="text-align:center;">
                <img src="assets/css/img/binjai.png" width="150px" height="200px">
            </p>
          </div>
          <div class="col-lg-6">
              <p> </p>
            <p style="text-align:center;">
                E-Kelurahan merupakan sebuah aplikasi yang berkaitan dengan pelayanan administrasi kependudukan. Aplikasi berbasis web ini bertujuan untuk mempermudah proses pelayanan kepada masyarakat dan solusi data center berbasis "cloud computing" serta integrasi dengan data kependudukan Kota Binjai.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <section id="portfolio" class="portfolio">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Alur Penggunaan</h2>
                <p>Berikut alur penggunaan aplikasi e-Kelurahan Kota Binjai.</p>
            </div>
            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                <div class="col-lg-3 col-md-12 portfolio-item filter-app">
                </div>
                <div class="col-lg-6 col-md-12 portfolio-item filter-app">
                    <img src="images/tahapan.png" alt="" style="width:800px;position: relative;top: 0;right: 100px;">
                </div>
                <div class="col-lg-3 col-md-12 portfolio-item filter-app">
                </div>
            </div>
        </div>
    </section>

    <section id="services" class="portfolio section-bg">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Rekapitulasi</h2>
                <p>Berikut rekapitulasi status surat permohonan yang ada di aplikasi e-Kelurahan.</p>
            </div>

            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-img"><img src="<?php echo base_url() ?>images/nikah.png" class="img-fluid" alt=""></div>
                    <div class="portfolio-info" style="left:0px">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Surat Masuk : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',0);
									$nikah = $this->db->get('srtnikah');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Diterima : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',1);
									$nikah = $this->db->get('srtnikah');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Ditolak : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',2);
									$nikah = $this->db->get('srtnikah');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Selesai : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',3);
									$nikah = $this->db->get('srtnikah');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-img"><img src="<?php echo base_url() ?>images/cerai.png" class="img-fluid" alt=""></div>
                    <div class="portfolio-info" style="left:0px">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Surat Masuk : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',0);
									$nikah = $this->db->get('srtcerai');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Diterima : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',1);
									$nikah = $this->db->get('srtcerai');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Ditolak : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',2);
									$nikah = $this->db->get('srtcerai');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Selesai : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',3);
									$nikah = $this->db->get('srtcerai');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-img"><img src="<?php echo base_url() ?>images/kematian.png" class="img-fluid" alt=""></div>
                    <div class="portfolio-info" style="left:0px">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Surat Masuk : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',0);
									$nikah = $this->db->get('srtkematian');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Diterima : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',1);
									$nikah = $this->db->get('srtkematian');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Ditolak : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',2);
									$nikah = $this->db->get('srtkematian');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Selesai : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',3);
									$nikah = $this->db->get('srtkematian');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-img"><img src="<?php echo base_url() ?>images/kurang_mampu.png" class="img-fluid" alt=""></div>
                    <div class="portfolio-info" style="left:0px">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Surat Masuk : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',0);
									$nikah = $this->db->get('srttdkmampu');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Diterima : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',1);
									$nikah = $this->db->get('srttdkmampu');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Ditolak : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',2);
									$nikah = $this->db->get('srttdkmampu');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Selesai : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',3);
									$nikah = $this->db->get('srttdkmampu');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-img"><img src="<?php echo base_url() ?>images/ahliwaris.png" class="img-fluid" alt=""></div>
                    <div class="portfolio-info" style="left:0px">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Surat Masuk : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',0);
									$nikah = $this->db->get('srtwaris');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Diterima : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',1);
									$nikah = $this->db->get('srtwaris');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Ditolak : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',2);
									$nikah = $this->db->get('srtwaris');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Selesai : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',3);
									$nikah = $this->db->get('srtwaris');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-img"><img src="<?php echo base_url() ?>images/skck.png" class="img-fluid" alt=""></div>
                    <div class="portfolio-info" style="left:0px">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Surat Masuk : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',0);
									$nikah = $this->db->get('srtskck');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Diterima : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',1);
									$nikah = $this->db->get('srtskck');
									echo $jlh_nikah = $nikah->num_rows();?>  </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Ditolak : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',2);
									$nikah = $this->db->get('srtskck');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                            <div class="col-md-6">
                                <p>Surat Selesai : <?php
									$this->db->select('*');
                                    $this->db->where('cekopt',3);
									$nikah = $this->db->get('srtskck');
									echo $jlh_nikah = $nikah->num_rows();?> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Services Section -->

    <section id="pricing" class="pricing section-bg">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Tracking</h2>
                <p style="text-align:center">Peserta E-Kelurahan dapat melakukan pelacakan proses yang telah diajukan dengan cara memasukkan  Nomor Identitas Kependudukan (NIK)</p>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <form class="form-horizontal"  action="<?php echo base_url() ?>" method = "POST">
                            <div class="form-group row">
                                <div class="col-xs-8 col-xs-push-2 col-md-4 col-md-push-2">
                                    <select name="jenis_input" class="form-control select2" style="width:100%;" id="jenis_input" >
                                        <option value="" >Pilih Jenis Surat</option>
                                        <option value="srtnikah" >Surah Pengantar Perkawinan</option>
                                        <option value="srtblmnikah" >Surah Belum Nikah Untuk Bekerja</option>
                                        <option value="srtwaris" >Surah Keterangan Ahli Waris</option>
                                        <option value="srtkematian" >Surah Keterangan Akta Kematian</option>
                                        <option value="srttdkmampu" >Surah Miskin</option>
                                        <option value="srtusaha" >Surah Usaha</option>
                                        <option value="srtdomisili" >Surah Domisili</option>
                                        <option value="srtortu" >Surah Izin Orang Tua</option>
                                        <option value="srtsengketa" >Surah Laporan Silang Sengketa</option>
                                        <option value="srtskck" >Surah Keterangan Pengurusan SKCK</option>
                                    </select>
                                </div>
                                <div class="col-xs-8 col-xs-push-2 col-md-4 col-md-push-2">
                                    <input type="text" class="form-control" id="keyword" name="nik" minlength="16" maxlength="16"  value="" placeholder="Masukkan NIK Anda">
                                </div>
                            </div>
                            <div class="form-group text-center row">
                                <div class="col-md-12">
                                    <button name="cari"  class="btn btn-primary"><i class="fa fa-search">Tracking</i> </button>
                                </div>
                            </div>
                        </form>
                <?php if (isset($_POST['cari'])) { ?>
				  <?php if ($_POST['jenis_input']=='srtnikah' && $_POST['nik']!= null) { ?>
                    <?php foreach($surat_nikah as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtblmnikah' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_belum_nikah as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtwaris' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_waris as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtkematian' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_kematian as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtskck' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_skck as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtwaris' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_waris as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtsengketa' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_sengketa as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtortu' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_ortu as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtdomisili' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_domisili as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srtusaha' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_usaha as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } else if ($_POST['jenis_input']=='srttdkmampu' && $_POST['nik']!= null) { ?>
                        <?php foreach($surat_kurang_mampu as $n) {  ?>
                        <?php if($n->cekopt == 3 && $n->ceklurah== 1){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 2 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Tolak Admin</h4>
                                            <h4 class="timeline-title" style="font-size:20px">Periksa Kembali Data Anda</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-remove" style="position: relative;float: right;top: auto;color: red "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 1 && $n->ceklurah== null){?>
                        <div class="container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                </div>
                                <ul class="timeline">
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">1</div>
                                    <div class="timeline-panel" style="float:left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">2</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">3</div>
                                    <div class="timeline-panel" style="float: left">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    
                                    <li class="timeline-inverted">
                                    <div class="timeline-badge">4</div>
                                    <div class="timeline-panel">
                                        <div class="flex-container flex-around">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                            <p>Silahkan Datang Ke Kantor Lurah.</p>
                                        </div>
                                        <div class="timeline-clock">
                                            <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php } else if($n->cekopt == 0 && $n->ceklurah== null){?>
                            <div class="container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Tracking NIK <?php echo $_POST['nik'] ?></h3>
                                    </div>
                                    <ul class="timeline">
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">1</div>
                                        <div class="timeline-panel" style="float:left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Dikirim</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p>
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">2</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Admin</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto; "></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">3</div>
                                        <div class="timeline-panel" style="float: left">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Di Verifikasi Lurah</h4>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                        
                                        <li class="timeline-inverted">
                                        <div class="timeline-badge">4</div>
                                        <div class="timeline-panel">
                                            <div class="flex-container flex-around">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title" style="font-size:20px">Data Sudah Terbit.</h4>
                                                <p>Silahkan Datang Ke Kantor Lurah.</p>
                                            </div>
                                            <div class="timeline-clock">
                                                <!-- <p><i class="glyphicon glyphicon-ok" style="position: relative;float: right;top: auto;"></i></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php } ?>
                <?php } ?>
                </div> 
            </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container footer-bottom clearfix">
      <div class="copyright">
        Dinas Komunikasi dan Informasi Kota Binjai - 2022
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/css/vendor/aos/aos.js"></script>
  <script src="assets/css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/css/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/css/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/css/vendor/php-email-form/validate.js"></script>
  <script src="assets/css/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/css/vendor/waypoints/noframework.waypoints.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/css/js/main.js"></script>
  <script src="assets/css/js/timeline.min.js"></script>
  <script src="assets/css/js/jquery.js"></script>
  <script>
    $(document).ready(function(){
        jQuery('.timeline').timeline();
    });
    </script>


</body>

</html>