<div class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
               
                <form enctype="multipart/form-data" id="LoginValidation" action="<?php echo base_url() ?>Verifikasi_surat/tolak_surat_skck" method="POST">
				
                    <input type="hidden" name="id_sk"  value="<?php echo $_POST['id_sk']; ?>" class="form-control"  >
					
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">contacts</i>
                            </div>
                            <h4 class="card-title">Tolak Verifikasi</h4>
                        </div>
                        <div class="card-body ">

                            
                            <div class="form-group">

                                <label for="" class="bmd-label"> Alasan <span class="text-danger">*<span></label>
                                <br>
                                <textarea class="form-control" rows="5" name="keterangan" required="true" ></textarea>
                            </div>


                            <div class="category form-category text-danger">* Harus Diisi</div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
						<table>
							<tr>
								<td>
								<button name="simpan" type="submit" class="btn btn-rose"><i class="fa fa-trash"></i> Tolak</button>
								</form>	
								</td>
						
								<td>
								<form action="<?php echo base_url(); ?>Verifikasi_surat/srtskck" method="POST">
								<button name="id_sk" class="btn btn-danger"><i class="fa fa-close"></i>
								</button>
								</form>
								</td>
							</tr>
						</table>	
                        </div>
                    </div>
                
            </div>


        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
</div>
</div>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>

