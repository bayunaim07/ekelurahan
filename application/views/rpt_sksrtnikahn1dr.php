<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";


$pr =pg_query($koneksi, "select * from srtnikah where id='$_GET[id]'");
	$r=pg_fetch_array($pr);
	$kdkel=$r['kodekel'];
	
	
?>

<?php 	
	$lurah=pg_query($koneksi, "select * from kelurahan where kode='$kdkel' ");
	$dtlur=pg_fetch_array($lurah);
	
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Surat Nikah</title>
<style type="text/css">
<!--
.style1 {font-size: 12px}
.judul {	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.style4 {font-size: 12px; font-weight: bold; }
-->
</style>
</head>

<body>
<table width="99%" border="0">
  <tr>
    <td colspan="6"><div align="right" class="style1">Lampiran 7 KMA No. 298 Tahun 2003<br />
      Pasal 5 ayat (1)<br />
    Model N-1</div></td>
    <td>&nbsp;</td>
    <td colspan="6"><div align="right"><span class="style1">Lampiran 7 KMA No. 298 Tahun 2003<br />
      Pasal 5 ayat (1)<br />
    Model N-1</span></div></td>
  </tr>
  <tr>
    <td colspan="4"><span class="style1">KANTOR DESA / KELURAHAN </span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $dtlur['nama_kel']; ?></span></td>
    <td>&nbsp;</td>
    <td colspan="4"><span class="style1">KANTOR DESA / KELURAHAN </span></td>
    <td width="1%">:</td>
    <td width="27%"><span class="style1"><?php echo $dtlur['nama_kel']; ?></span></td>
  </tr>
  <tr>
    <td colspan="4"><span class="style1">KECAMATAN</span></td>
    <td width="1%">:</td>
    <td width="25%"><span class="style1"><?php echo $dtlur['nama_kec']; ?></span></td>
    <td>&nbsp;</td>
    <td colspan="4"><span class="style1">KECAMATAN</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $dtlur['nama_kec']; ?></span></td>
  </tr>
  <tr>
    <td colspan="4"><span class="style1">KABUPATEN / KOTAMADYA</span></td>
    <td>:</td>
    <td><span class="style1">MEDAN</span></td>
    <td>&nbsp;</td>
    <td colspan="4"><span class="style1">KABUPATEN / KOTAMADYA</span></td>
    <td>:</td>
    <td><span class="style1">MEDAN</span></td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><div align="center"><strong><u>SURAT</u></strong><strong><u> KETERANGAN UNTUK NIKAH</u></strong></div></td>
    <td>&nbsp;</td>
    <td colspan="6"><div align="center"><strong><u>SURAT</u></strong><strong><u> KETERANGAN UNTUK NIKAH</u></strong></div></td>
  </tr>
  <tr>
    <td colspan="6"><div align="center">Nomor : &nbsp;<?php echo $r['nosurat']; ?></div></td>
    <td>&nbsp;</td>
    <td colspan="6"><div align="center">Nomor : &nbsp;<?php echo $r['nosurat']; ?></div></td>
  </tr>
  
  <tr>
    <td colspan="6"><span class="style1">Yang bertanda tangan di bawah ini menerangkan  dengan sesungguhnya bahwa :</span></td>
    <td>&nbsp;</td>
    <td colspan="6"><span class="style1">Yang bertanda tangan di bawah ini menerangkan  dengan sesungguhnya bahwa :</span></td>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="2%"><span class="style1">1.</span></td>
    <td colspan="2"><span class="style1">Nama lengkap dan alias </span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['nama']; ?></span></td>
    <td>&nbsp;</td>
    <td width="1%">&nbsp;</td>
    <td width="2%"><span class="style1">1.</span></td>
    <td colspan="2"><span class="style1">Nama lengkap dan alias </span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['nama']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">2.</span></td>
    <td colspan="2"><span class="style1">Jenis Kelamin </span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['jenkel']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">2.</span></td>
    <td colspan="2"><span class="style1">Jenis Kelamin </span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['jenkel']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">3.</span></td>
    <td colspan="2"><span class="style1">Tempat dan tanggal lahir</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['tempat']; ?> </span> / <span class="style1">
      <?php $tgllong=$r['tgllahir'];echo tgl_indo($tgllong); ?>
    </span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">3.</span></td>
    <td colspan="2"><span class="style1">Tempat dan tanggal lahir</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['tempat']; ?> </span> / <span class="style1">
    <?php $tgllong=$r['tgllahir'];echo tgl_indo($tgllong); ?>
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">4.</span></td>
    <td colspan="2"><span class="style1">Warga Negara</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['warga_negara']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">4.</span></td>
    <td colspan="2"><span class="style1">Warga Negara</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['warga_negara']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">5.</span></td>
    <td colspan="2"><span class="style1">A g a m a</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['agama']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">5.</span></td>
    <td colspan="2"><span class="style1">A g a m a</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['agama']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">6.</span></td>
    <td colspan="2"><span class="style1">Pekerjaan</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['pekerjaan']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">6.</span></td>
    <td colspan="2"><span class="style1">Pekerjaan</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['pekerjaan']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">7.</span></td>
    <td colspan="2"><span class="style1">Tempat Tinggal</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['alamat']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">7.</span></td>
    <td colspan="2"><span class="style1">Tempat Tinggal</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['alamat']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">8.</span></td>
    <td colspan="2"><span class="style1">Bin /Binti</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['nama_ayah']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">8.</span></td>
    <td colspan="2"><span class="style1">Bin /Binti</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['nama_ayah']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">9.</span></td>
    <td colspan="2"><span class="style1">Status perkawinan</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['status']; ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">9.</span></td>
    <td colspan="2"><span class="style1">Status perkawinan</span></td>
    <td>:</td>
    <td><span class="style1"><?php echo $r['status']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="1%"><span class="style1">a.</span></td>
    <td width="18%" rowspan="2"><div align="justify"><span class="style1">Jika pria, terangkan  jejaka, duda atau beristri dan berapa istrinya</span></div></td>
    <td rowspan="2">:</td>
    <td rowspan="2"><div align="justify"><span class="style1"><?php echo $r['ketpria']; ?></span></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="1%"><span class="style1">a.</span></td>
    <td width="17%" rowspan="2"><div align="justify"><span class="style1">Jika pria, terangkan  jejaka, duda atau beristri dan berapa istrinya</span></div></td>
    <td rowspan="2">:</td>
    <td rowspan="2"><div align="justify"><span class="style1"><?php echo $r['ketpria']; ?></span></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td rowspan="2"><span class="style1">b.</span></td>
    <td rowspan="2"><span class="style1">Jika wanita, terangkan  perawan atau janda</span></td>
    <td rowspan="2">:</td>
    <td rowspan="2"><div align="justify"><span class="style1"><?php echo $r['ketwanita']; ?></span></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td rowspan="2"><span class="style1">b.</span></td>
    <td rowspan="2"><span class="style1">Jika wanita, terangkan  perawan atau janda</span></td>
    <td rowspan="2">:</td>
    <td rowspan="2"><div align="justify"><span class="style1"><?php echo $r['ketwanita']; ?></span></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="style1">10.</span></td>
    <td colspan="2"><span class="style1">Nama Istri / Suami  terdahulu</span></td>
    <td>:</td>
    <td><div align="justify"><span class="style1"><?php echo $r['pasangan_terdahulu']; ?></span></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span class="style1">10.</span></td>
    <td colspan="2"><span class="style1">Nama Istri / Suami  terdahulu</span></td>
    <td>:</td>
    <td><div align="justify"><span class="style1"><?php echo $r['pasangan_terdahulu']; ?></span></div></td>
  </tr>
  
  <tr>
    <td colspan="6"><span class="style1">Demikianlah surat  keterangan ini dibuat dengan mengingat sumpah jabatan dan untuk digunakan &nbsp;seperlunya.</span></td>
    <td>&nbsp;</td>
    <td colspan="6"><span class="style1">Demikianlah surat  keterangan ini dibuat dengan mengingat sumpah jabatan dan untuk digunakan &nbsp;seperlunya.</span></td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  
  <tr>
    <td rowspan="7"><p>&nbsp;</p></td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td>&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7"><table width="100%" border="0">
      <tr>
        <td><span class="style4">Medan, 
          <?php $tgllong=$r['tglsurat'];echo tgl_indo($tgllong); ?>
        </span></td>
      </tr>
      <tr>
        <td><span class="style1"><strong>LURAH <?php echo $dtlur['nama_kel']; ?></strong></span></td>
      </tr>
      <tr>
        <td><span class="style4">KECAMATAN <strong><?php echo $dtlur['nama_kec']; ?></strong></span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><span class="style1"><strong><?php echo $dtlur['nama_lurah']; ?></strong></span></td>
      </tr>
      
      <tr>
        <td><span class="style1"><strong>NIP. <?php echo $dtlur['nip_lurah']; ?></strong></span></td>
      </tr>
    </table></td>
    <td width="3%" rowspan="7"><p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7">&nbsp;</td>
    <td rowspan="7"><table width="100%" border="0">
      <tr>
        <td><span class="style4">Medan,
            <?php $tgllong=$r['tglsurat'];echo tgl_indo($tgllong); ?>
        </span></td>
      </tr>
      <tr>
        <td><span class="style1"><strong>LURAH <?php echo $dtlur['nama_kel']; ?></strong></span></td>
      </tr>
      <tr>
        <td><span class="style4">KECAMATAN <strong><?php echo $dtlur['nama_kec']; ?></strong></span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><span class="style1"><strong><?php echo $dtlur['nama_lurah']; ?></strong></span></td>
      </tr>
      
      <tr>
        <td><span class="style1"><strong>NIP. <?php echo $dtlur['nip_lurah']; ?></strong></span></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="style1">*) nama terang</span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Nikah";
$namafile = "srtnikah.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>