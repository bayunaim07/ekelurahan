 <div class="container-fluid">
   <?php foreach($surat_belum_nikah as $n) {} 
   ?>
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Surat/edit_srtblmnikah" method="POST" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="id_bn" value="<?php echo $n->id_bn; ?>" >  
              <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit Surat Belum Nikah</h4>
                  </div>
				  
                  <div class="card-body">
					   <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>I. DATA WARGA</b></h4>
					   </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> KK *</label>
                      <input type="text" class="form-control" name="kk"  required="true" value="<?php echo $n->kk; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK *</label>
                      <input type="text" class="form-control" name="nik"  required="true" value="<?php echo $n->nik; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama" value="<?php echo $n->nama; ?>">
                    </div>
					          <div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat" value="<?php echo $n->nik; ?>" >
                    </div>
					        <div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir" value="<?php echo $n->tgllahir; ?>">
                    </div>
          
                  <div class="form-group">
                      <label for="" class="bmd-label-floating">Kecamatan</label>
                      <select type="text" id="select1" class="form-control select"  required="true" name="namakec" style="margin-top:10px">
                        <?php foreach ($kecamatan as $ke) { ?>
                          <option <?php if($ke->id_kec == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $ke->id_kec; ?>"><?php echo $ke->nama_kec;?> </option>
                        <?php } ?>
					            </select>
                  </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Kelurahan</label>
                      <select type="text" id="select2" class="form-control select"  required="true" name="namakel" style="margin-top:10px">
                      <?php foreach ($kelurahan as $kel) { ?>
                          <option <?php if($kel->id_kel == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $kel->id_kel; ?>"><?php echo $kel->nama_kel;?> </option>
                        <?php } ?>
					            </select>
                    </div>

					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan"  >
					              <?php foreach ($pekerjaan as $pe) { ?>
                          <option <?php if($pe->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $pe->id; ?>"><?php echo $pe->namapekerjaan;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Status Perkawinan</label>
                      <select type="text" class="form-control select2"  required="true" name="status"  >
					              <?php foreach ($status as $st) { ?>
                          <option <?php if($st->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $st->id; ?>"><?php echo $st->status_perkawinan;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama" >
					 	            <?php foreach ($agama as $a) { ?>
                          <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Jenis Kelamin</label>
                      <select type="text" class="form-control select2"  required="true" name="jenkel" >
                        <option value="<?php echo $n->jenkel; ?>"><?php echo $n->jenkel; ?></option>
                        <option value="Laki-Laki">Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                    </div>
					          <div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat"  value="<?php echo $n->alamat; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Peruntukan *</label>
                      <input type="text" class="form-control"  required="true" name="peruntukan"  value="<?php echo $n->peruntukan; ?>">
                    </div>

                     <h5 for="" class="title"> Upload KTP (dalam Bentuk JPG atau PNG) <span class="text-danger">*</span></h5>
                            <div class="form-group" id="foto_ktp">
                                <div class="col-md-6 col-sm-6">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <?php
                                            if(empty($n->foto_ktp)){

                                                ?>
                                                <img src="<?php echo base_url() ?>../images/logo/dokumen.png" >
                                            <?php } else { ?>
                                                <img src="<?php echo base_url() ?>images/surat_blm_nikah/<?php echo $n->foto_ktp ?>" >
                                            <?php } ?>
                                        </div>

                                    </div>

                                </div>

                                <span id="foto_ktp_error" class="text-danger"></span>
                            </div>
                            <span class="uploaded_image"></span>
                            <div>
                                <?php
                                if(empty($n->foto_ktp)){

                                    ?>
                                    <input type="file" name="foto_ktp" id="file" class="foto_ktp" />
                                <?php } else { ?>
                                    <input type="file" name="foto_ktp" id="file"  />
                                <?php } ?>
                            </div>
                            <br>
                            <hr>	

                   <h5 for="" class="title"> Upload KK (dalam Bentuk JPG atau PNG) <span class="text-danger">*</span></h5>
                            <div class="form-group" id="foto_kk">
                                <div class="col-md-6 col-sm-6">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <?php
                                            if(empty($n->foto_kk)){

                                                ?>
                                                <img src="<?php echo base_url() ?>../images/logo/dokumen.png" >
                                            <?php } else { ?>
                                                <img src="<?php echo base_url() ?>images/surat_blm_nikah/<?php echo $n->foto_kk ?>" >
                                            <?php } ?>
                                        </div>

                                    </div>

                                </div>

                                <span id="foto_kk_error" class="text-danger"></span>
                            </div>
                            <span class="uploaded_image2"></span>
                            <div>
                                <?php
                                if(empty($n->foto_kk)){

                                    ?>
                                    <input type="file" name="foto_kk" id="file2" class="foto_kk" />
                                <?php } else { ?>
                                    <input type="file" name="foto_kk" id="file2"  />
                                <?php } ?>
                            </div>
                            <br>
                            <hr>
                    
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                   <?php if ($n->cekopt==null){?>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Surat/srtblmnikah" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                  <?php } elseif($n->cekopt==2){ ?>
                    <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Surat/srtblmnikah" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                  <?php }else{?>
                    <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Surat/srtblmnikah" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </form>
            </div>
          </div>
        
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
			function readURLUser(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
			function readURLUser1(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser1') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
    $(function () {
     $('input[type="file"]').change(function () {
          if ($(this).val() != "") {
                 $(this).css('color', '#333');
          }else{
                 $(this).css('color', 'transparent');
          }
     });
})
</script>


<script>
    $(document).ready(function(){
        $(document).on('change', '#file2', function(){
            var name = document.getElementById("file2").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
            {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file2").files[0]);
            var f = document.getElementById("file2").files[0];
            var fsize = f.size||f.fileSize;
            if(fsize > 20000000000)
            {
                alert("Ukuran File Gambar Terlalu Besar Maksimal 20MB");
            }
            else
            {
                form_data.append("file2", document.getElementById('file2').files[0]);
                $.ajax({
                    url:"<?php echo base_url() ?>index.php/Surat/simpan_foto_kk_belum_nikah",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        $('.uploaded_image2').html("<label class='text-success'>Sedang Mengupload Gambar...</label>");
                    },
                    success:function(data)
                    {
                        $('.uploaded_image2').html("<h4 class='text-success'><span class='fa fa-check-circle-o'></span> Data Berhasil Diupload</h4>");
                        $("#foto_kk").load(" #foto_kk");

                    }
                });
            }
        });
    });
</script>


<script>
    $(document).ready(function(){
        $(document).on('change', '#file', function(){
            var name = document.getElementById("file").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
            {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file").files[0]);
            var f = document.getElementById("file").files[0];
            var fsize = f.size||f.fileSize;
            if(fsize > 20000000000)
            {
                alert("Ukuran File Gambar Terlalu Besar Maksimal 20MB");
            }
            else
            {
                form_data.append("file", document.getElementById('file').files[0]);
                $.ajax({
                    url:"<?php echo base_url() ?>index.php/Surat/simpan_foto_ktp_belum_nikah",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        $('.uploaded_image').html("<label class='text-success'>Sedang Mengupload Gambar...</label>");
                    },
                    success:function(data)
                    {
                        $('.uploaded_image').html("<h4 class='text-success'><span class='fa fa-check-circle-o'></span> Data Berhasil Diupload</h4>");
                        $("#foto_ktp").load(" #foto_ktp");

                    }
                });
            }
        });
    });
</script>
