<div class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
               
                <form enctype="multipart/form-data" id="LoginValidation" action="<?php echo base_url() ?>Verifikasi_surat/terima_surat_belum_nikah" method="POST">
				
                    <input type="hidden" name="id_bn"  value="<?php echo $_POST['id_bn']; ?>" class="form-control"  >
					
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">contacts</i>
                            </div>
                            <h4 class="card-title">Terima Verifikasi</h4>
                        </div>
                        <div class="card-body ">

                            
                             <div class="form-group">

                                <label for="" class="bmd-label"> No. Surat <span class="text-danger">*<span></label>
                                <br>
                                <input type="text" class="form-control"  name="nosurat" required="true" ></input>
                            </div>


                            <div class="category form-category text-danger">* Harus Diisi</div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
						<table>
							<tr>
								<td>
								<button name="simpan" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
								</form>	
								</td>
						
								<td>
								<form action="<?php echo base_url(); ?>Verifikasi_surat/srtblmnikah" method="POST">
								<button name="id_bn" class="btn btn-danger"><i class="fa fa-close"></i>
								</button>
								</form>
								</td>
							</tr>
						</table>	
                        </div>
                    </div>
                
            </div>


        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
</div>
</div>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>

