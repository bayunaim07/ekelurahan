 <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Master_surat/edit" method="POST">
			   <input type="hidden" value="<?php echo $edit['id']; ?>" class="form-control" name="id" >
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit Surat</h4>
                  </div>
                  <div class="card-body ">
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kode *</label>
                      <input type="text" value="<?php echo $edit['kode']; ?>" class="form-control" name="kode" required="true">
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Surat *</label>
                      <input type="text" value="<?php echo $edit['nama_surat']; ?>" class="form-control"  required="true" name="nama_surat">
                    </div>
                    
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Master_surat" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
            
            
            
          </div>
</div>