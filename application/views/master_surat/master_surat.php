<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Master Surat</h4>
                </div>
                <div class="card-body">
				
                  <div class="toolbar">
				  <form action="<?php echo base_url(); ?>Master_surat/tambah_surat" method="POST">
                    <button name="tambah_surat" class="btn btn-primary btn-round btn-fab"  data-toggle="tooltip" data-placement="top" title="Tambah Surat">
                      <i class="material-icons">add</i>
                    </button>
				  </form>	
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
						  <th>No</th>
                          <th>Kode</th>
                          <th>Nama Surat</th>
                         
                          
                          <th width="130"><center>Aksi</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                        <?php 
						$no=1;
						foreach($surat as $s) { ?>
                        <tr>
						  <td><?php echo $no++; ?></td>
                          <td><?php echo $s->kode; ?></td>
                          <td><?php echo $s->nama_surat; ?></td>
                          
                          <td>
						  <table class="table table-striped table-no-bordered table-hover">
							<tr>
								<td>
									<form action="<?php echo base_url() ?>Master_surat/edit" method="POST">
									<input type="hidden" name="id" value="<?php echo $s->id; ?>">
										<button  class="btn btn-round btn-fab btn-info"><i class="material-icons">edit</i></button>
									</form>	
								</td>
								<td>
									<form action="<?php echo base_url() ?>Master_surat/hapus" method="POST">
									<input type="hidden" name="id_hapus" value="<?php echo $s->id; ?>">
									<button  class="btn btn-round btn-fab btn-danger"><i class="material-icons">close</i></button>
									</form>
								</td>
							</tr>
						  </table>	
                          </td>
                        </tr>
                        <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>