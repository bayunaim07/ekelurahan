<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";
?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Surat IZIN ORANG TUA</title>
<style type="text/css">
.style1 {font-size: 14px}
.judul {	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.style4 {font-size: 12px; font-weight: bold; }

</style>
</head>

<body>
<?php foreach($cetak as $n) {
?>
<table width="99%" border="0">
  <tr>
    <td colspan="6"><div align="right" class="style1">Model N 4</div></td>
  </tr>
 <?php } ?>
</table>

<?php foreach($nomor as $n) {
?>
<table width="99%" border="0">
  <tr>
    <td colspan="4">&nbsp;</td>
    <td width="1">&nbsp;</td>
    <td width="103">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><div align="center"><strong><u>SURAT</u></strong><strong><u> IZIN ORANG TUA</u></strong></div></td>
  </tr>
  <tr >
      <?php
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $bln = $array_bln[date('n')];
      ?>
    <td colspan="6"><div align="center" style="margin-bottom:10px">Nomor : &nbsp;<?php echo $n->kode; ?> / <?php echo $n->nosurat; ?>/ <?php echo $n->kode_lurah; ?> /<?php echo $bln; ?> /<?php echo date(Y) ?></div></td>
  </tr>
</table>
<?php }?>

<?php foreach($ayah as $n) {
?>
  <table width="99%" border="0">
  <tr>
    <td colspan="6"><div style="position:relative; left:150px; margin-bottom:20px"><span class="style1">Yang bertanda tangan di bawah ini :</span></div></td>
  </tr>
  
    <tr>
    <td width="119"><span class="style1" style="position:relative; left:150px">I.</span></td>
    <td width="151"><span class="style1">1. Nama lengkap </span></td>
    <td width="237"><span class="style1">: <?php echo $n->nama_ayah; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">2. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->tempat_ayah; ?> </span> / <span class="style1"><?php echo $n->tgllahir_ayah; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $n->bangsa_ayah; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Agama</span></td>
      <td><span class="style1">: <?php echo $n->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->namapekerjaan; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->alamat_ayah; ?></span></td>
    </tr>
    
</table>
<br />
<?php } ?>

  <?php foreach ($ibu as $n){?>
  <table width="99%" border="0">
    <tr>
    <td width="119"><span class="style1" style="position:relative; left:150px">II.</span></td>
    <td width="150"><span class="style1">1. Nama lengkap </span></td>
    <td width="233"><span class="style1">: <?php echo $n->nama_ibu; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">2. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->tempat_ibu; ?> </span> / <span class="style1"><?php echo $n->tgllahir_ibu; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $n->bangsa_ibu; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Agama</span></td>
      <td><span class="style1">: <?php echo $n->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->namapekerjaan; ?></span></td>
    </tr>

    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->alamat_ibu; ?></span></td>
    </tr>
    </table>
    <?php } ?>

    <br />
    
  <?php foreach($cetak as $n) { ?>
  <table width="99%" border="0">
  <tr>
    <td colspan="6"><div style="margin-bottom:20px"><span class="style1">adalah ayah dan ibu kandung dari :</span></div></td>
  </tr>
  
    <tr>
    <td width="119"><span class="style1" style="position:relative; left:150px"></span></td>
    <td width="151"><span class="style1">1. Nama lengkap </span></td>
    <td width="237"><span class="style1">: <?php echo $n->nama; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">2. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->tempat; ?> </span> / <span class="style1"><?php echo $n->tgllahir; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $n->warga_negara; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Agama</span></td>
      <td><span class="style1">: <?php echo $n->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->namapekerjaan; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->alamat; ?></span></td>
    </tr>
    
</table>
<br />
<?php } ?>

<?php foreach($calon as $n) { ?>
  <table width="99%" border="0">
  <tr>
    <td colspan="6"><div style="margin-bottom:20px"><span class="style1">memberikan izin kepadanya untuk melakukan pernikahan dengan :</span></div></td>
  </tr>
  
    <tr>
    <td width="119"><span class="style1" style="position:relative; left:150px"></span></td>
    <td width="151"><span class="style1">1. Nama lengkap </span></td>
    <td width="237"><span class="style1">: <?php echo $n->nama_calon; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">2. Tempat / Tgl. Lahir</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->tempat_calon; ?> </span> / <span class="style1"><?php echo $n->tgllahir_calon; ?>
      </span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">3. Kewarganegaraan</span></td>
      <td><span class="style1">: <?php echo $n->bangsa_calon; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">4. Agama</span></td>
      <td><span class="style1">: <?php echo $n->agama; ?></span></td>
    </tr>
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">5. Pekerjaan</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->namapekerjaan; ?></span></td>
    </tr>
    
    <tr>
    <td><span class="style1"></span></td>
      <td><span class="style1">6. Alamat</span></td>
      <td colspan="4"><span class="style1">: <?php echo $n->alamat_calon; ?></span></td>
    </tr>
    
</table>
<br />
<?php } ?>

<table width="99%" border="0">
  <tr>
    <td colspan="6"><div align="center"><span class="style1" style="margin-left:100px">Demikianlah Surat Izin ini dibuat dengan tanpa paksaan dari siapapun juga dan untuk di pergunakan seperlunya.</span></div></td>
  </tr>

  
  
  <tr>
    <td rowspan="1"><p>&nbsp;</p></td>
    <td rowspan="1">&nbsp;</td>
    <td rowspan="1">&nbsp;</td>
    <td width="4">&nbsp;</td>
    <td rowspan="1">&nbsp;</td>
    <td rowspan="1">
      <table width="100%" border="0">
        <tr>
          <td><div align="center" class="style1">Binjai,  <?php echo date('d  F  Y');?> </div></td>
        </tr>
      </table>
      <table width="100%">
          <tr>
            <td rowspan="10"><div align="center">
              <div align="center" class="style9">I. Ayah</div>
              <br/><br/><br/><br/><br/>
              <div align="center"><?php echo $n->nama_ayah?></div>
            </td>
          </tr>
        
        <tr>
          <td width="13%" rowspan="6">&nbsp;</td>
          <td><div align="right"></div></td>
        </tr>
        <tr>
            <td rowspan="10"><div align="center">
              <div align="center" class="style9">II. Ibu</div>
              <br/><br/><br/><br/><br/>
              <div align="center"><?php echo $n->nama_ibu?></div>
            </td>
          </tr>
      </table>
    </td>
    <td width="7" rowspan="7"><p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="1" rowspan="7">&nbsp;</td>
    <td width="30" rowspan="7">&nbsp;</td>
    
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
<?php
include "../../phpqrcode/qrlib.php"; 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Izin Orang Tua";
$namafile = "srtnikah.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


// QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>