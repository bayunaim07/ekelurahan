<?php  
	$level=$this->session->userdata('id_user_level'); 
		if($level=='4'){
?>
<div class="container-fluid">
<div class="row">
            <div class="col-md-12">
			
              <div class="page-categories">
			   <div class="card">
               <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon card-header-danger">
                    <a href="<?php echo base_url() ?>Verifikasi_surat"><i class="material-icons text-white">close</i></a>
                  </div>
				   <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
				  
                  <h4 class="card-title">Surat Keterangan Ahli Waris</h4>
				   <br>
                </div>
                  <div class="tab-pane" id="link8">
                    <div class="card">
                      <div class="card-header">
                        <div class="toolbar">
						  </div>
                        <p class="card-category">
						<div class="material-datatables">
							<table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							  <thead>
								<tr>
								  <th width="50">No</th>
								   <th>Tgl.Daftar</th>
								<th>No.Reg</th>
								<th>Nama</th>
								<th>Status</th>
								<th width="80"><center>Aksi</center></th>
								</tr>
							  </thead>
							 
							  <tbody id="show_data">
								
								
							  </tbody>
							</table>
						  </div>

                        </p>
                      </div>
                      
                    </div>
                  </div>
                  
                 
                </div>
				</div>
              </div>
			  
			  
            </div>
          </div>
          
          <!-- end row -->
        </div>
<script src="<?php echo base_url('asets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('asets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('asets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data();	
		
		$('#mydata_admin').dataTable();
		 
		
		function tampil_data(){
		    $.ajax({
		        type  : 'GET',
		        url   : '<?php echo base_url();?>Verifikasi_lurah/surat_waris',
		        async : false,
		        dataType : 'json',
		        success : function(data){
		            var html = '';
		            var i;
					var no=1;
		            for(i=0; i<data.length; i++){
					var ket = data[i].cekopt;
					var lurah = data[i].ceklurah;
		                html += '<tr>'+
								'<td style="text-align:center">'+no+++'</td>'+
		                  		'<td>'+data[i].tgldaftar+'</td>'+
		                        '<td>'+data[i].noreg+'</td>'+
								'<td>'+data[i].nama+'</td>';
								
								if(lurah==1){
									html += '<td class="btn btn-success" style="margin-top:25px">Sudah Diverifikasi</td>';
								} 
								else{
									html += '<td class="btn btn-danger" style="margin-top:25px"> Belum Diverifikasi </td>';
								}

		                   	html += '<td width="80" style="text-align:center;">'+
								'<table class="table table-striped table-no-bordered table-hover">'+ 
										'<tr>'+
										'<td>'+
                                    '<form action="<?php echo base_url() ?>Verifikasi_lurah/verifikasi_surat_waris" method="POST"><input name="id_wa" value="'+data[i].id_wa+'" type="hidden"><button  data-toggle="tooltip" data-placement="top" title="Verifikasi Surat Waris" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button></form>'+' '+
                                    
									    '</td>'+
								'</tr>'+	
								'</table>'+	
                                '</td>'+
		                        '</tr>';
							
		            }
		            $('#show_data').html(html);
		        }

		    });
		}
	});

	

</script>		
<?php }?>