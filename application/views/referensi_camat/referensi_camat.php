<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <h4 class="card-title">Referensi camat</h4>
                </div>
                <div class="card-body">
				
                  <div class="toolbar">
				  <form action="<?php echo base_url(); ?>Referensi_camat/tambah_camat" method="POST">
                    <button name="tambah_camat" class="btn btn-primary btn-round btn-fab"  data-toggle="tooltip" data-placement="top" title="Tambah camat">
                      <i class="material-icons">person_add</i>
                    </button>
				  </form>	
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
						              <th>No</th>
                          <th>NIP</th>
                          <th>Nama camat</th>
                          <th>Kecamatan</th>
                          <th>Kelurahan</th>
                          <th>Pangkat</th>
                          <th>Golongan</th>
                          <th>Jabatan</th>
                         
                          
                          <th width="130"><center>Aksi</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                        <?php 
						$no=1;
						foreach($camat as $c) { ?>
                        <tr>
						  <td><?php echo $no++; ?></td>
                          <td><?php echo $c->nip_camat; ?></td>
                          <td><?php echo $c->nama_camat; ?></td>
                          <td><?php echo $c->nama_kec; ?></td>
                          <td><?php echo $c->nama_kel; ?></td>
                          <td><?php echo $c->pangkat_camat; ?></td>
                          <td><?php echo $c->golongan_camat; ?></td>
                          <td><?php echo $c->jabatan_camat; ?></td>
                         
                          
                          <td>
						  <table class="table table-striped table-no-bordered table-hover">
							<tr>
								<td>
									<form action="<?php echo base_url() ?>Referensi_camat/edit" method="POST">
									<input type="hidden" name="id_camat" value="<?php echo $c->id_camat; ?>">
										<button  class="btn btn-round btn-fab btn-info"><i class="material-icons">edit</i></button>
									</form>	
								</td>
								<td>
									<form action="<?php echo base_url() ?>Referensi_camat/hapus" method="POST">
									<input type="hidden" name="id_camat" value="<?php echo $c->id_camat; ?>">
									<button  class="btn btn-round btn-fab btn-danger"><i class="material-icons">close</i></button>
									</form>
								</td>
							</tr>
						  </table>	
                          </td>
                        </tr>
                        <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>