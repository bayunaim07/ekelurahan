 <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Referensi_camat/edit" method="POST">
              <input type="hidden" class="form-control" value="<?php echo $edit['id_camat']; ?>"  name="id_camat" >
              <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">edit</i>
                    </div>
                    <h4 class="card-title">Edit Data camat</h4>
                  </div>
                  <div class="card-body ">
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">NIP</label>
                      <input type="text" class="form-control" name="nip_camat" required="true" value="<?php echo $edit['nip_camat']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama camat</label>
                      <input type="text" class="form-control"  required="true" name="nama_camat" value="<?php echo $edit['nama_camat']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="Kecamatan" class="bmd-label-floating">Kecamatan</label>
                      <select type="text" id="select1" class="form-control select"  required="true" name="id_kec" style="margin-top:10px" >
                        <?php foreach($join_kec as $jk){  ?>
					              <option value="<?php echo $jk->id_kec; ?>"><?php echo $jk->nama_kec; ?></option>
					                <?php } ?>
                        <?php 
                        
                        foreach ($kecamatan as $k)
                        {  
                        ?>
                          <option value="<?php echo $k->id_kec; ?>"><?php echo $k->nama_kec; ?></option>
                        <?php } ?>
					            </select>
                  </div>

                    <div class="form-group">
                      <label for="Kelurahan" class="bmd-label-floating">Kelurahan</label>
                      <select type="text" id="select2" class="form-control select"  required="true" name="id_kel" style="margin-top:10px">
                      <?php foreach($join_kel as $jkl){  ?>
					              <option value="<?php echo $jkl->id_kel; ?>"><?php echo $jkl->nama_kel; ?></option>
					            <?php } ?>
                      
                      <?php 
                        foreach ($kelurahan as $k)
                        {  
                        ?>
                          <option data-value="<?php echo $k->id_kec; ?>" value="<?php echo $k->id_kel; ?>"><?php echo $k->nama_kel; ?></option>
                        <?php } ?>
					            </select>
                    </div>
                    
                    <div class="form-group" style="margin-top:20px">
                      <label for="" class="bmd-label-floating"> Pangkat </label>
                      <input type="text" class="form-control"  required="true" name="pangkat_camat" value="<?php echo $edit['pangkat_camat']; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Golongan </label>
                      <input type="text" class="form-control"  required="true" name="golongan_camat" value="<?php echo $edit['golongan_camat']; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Jabatan </label>
                      <input type="text" class="form-control"  required="true" name="jabatan_camat" value="<?php echo $edit['jabatan_camat']; ?>">
                    </div>
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Referensi_camat" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
						 </div>
                  
              </form>
            </div>
            
            
            
          </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script type="text/javascript">
function PreviewImage() {
var oFReader = new FileReader();
oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
oFReader.onload = function (oFREvent)
 {
    document.getElementById("uploadPreview").src = oFREvent.target.result;
};
};
</script>
