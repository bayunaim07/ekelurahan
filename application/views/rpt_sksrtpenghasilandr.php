<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";
include "../../config/fungsi_terbilang.php";

$pr =pg_query($koneksi, "select * from srtpenghasilan where id='$_GET[id]'");
	$r=pg_fetch_array($pr);
	$kdkel=$r['kodekel'];
	
	
?>

<?php 	
	$lurah=pg_query($koneksi, "select * from kelurahan where kode='$kdkel' ");
	$dtlur=pg_fetch_array($lurah);
	
?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman", Times, serif; font-size: 18px; font-style: normal; font-weight: bold; text-align: center; text-decoration: underline; }
body {
	margin-left: 0.5cm;
}
.style6 {font-size: 18px; font-style: normal; text-align: center; font-family: "Times New Roman", Times, serif;}
.style22 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 24px; font-style: normal; text-align: center; }
.style3 {font-family: Arial, Helvetica, sans-serif;
	font-size: 20pt;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style23 {font-size: 20pt;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
}
.tanggal {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
}
.ttd {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
}
</style>
<title>Keterangan Berpenghasilan</title><table width="90%">
  <tr>
    <td><table width="100%">
     
    </table>
      <table width="100%">
        
        <tr>
          <td class="style2"><span class="nomor"><img src="../../images/kopsurat.PNG" width="1064" height="250" /></span></td>
        </tr>
        <tr>
          <td class="style2">&nbsp;</td>
        </tr>
        <tr>
          <td class="style2"><span class="style3">SURAT KETERANGAN</span></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="nomor"><strong class="style23">Nomor : <?php echo $r['nosurat']; ?></strong></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="isi">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        

        <tr>
          <td width="100%" colspan="4"><p align="justify"><span class="style23">Lurah Sukaraja&nbsp; Kecamatan Medan Maimun Pemerintah Kota Medan,  dengan ini menerangkan bahwa :</span></p></td>
        </tr>
        
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td><table width="100%">
            <tr>
              <td width="8%">&nbsp;</td>
              <td class="isi"><span class="style23">Nama Lengkap </span></td>
              <td width="2%" class="style23">:</td>
              <td class="style23"><?php echo $r['nama']; ?></td>
            </tr>
            
			
            <tr>
              <td>&nbsp;</td>
              <td class="isi"><span class="style23">Tempat/ Tangal Lahir</span></td>
              <td class="style23">:</td>
              <td class="style23"><?php echo $r['tempat'];?>/
                <?php $tgllong=$r['tgllahir'];echo tgl_indo($tgllong); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td width="33%" class="isi"><span class="style23">Nomor Induk Kependudukan</span></td>
              <td class="style23">:</td>
              <td class="style23"><?php echo $r['nik']; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="isi"><span class="style23">Kewarganegaraan / Agama</span></td>
              <td class="style23">:</td>
              <td width="57%" class="style23"><?php echo $r['warga_negara']; ?> / <?php echo $r['agama']; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style23">Pekerjaan</td>
              <td class="style23">:</td>
              <td class="style23"><?php echo $r['pekerjaan']; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style23">Alamat</td>
              <td class="style23">:</td>
              <td class="style23"><?php echo $r['alamat']; ?></td>
            </tr>
            

            <tr>
              <td>&nbsp;</td>
              <td class="isi">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            
            
          </table></td>
        </tr>
      </table>
      <table width="100%">
        
        
        <tr>
          <td width="100%" colspan="2"><div align="justify">
            <p class="style23">Selanjutnya sesuai dengan pernyataan yang  bersangkutan tanggal 
              <?php $tgllong=$r['tglpernyataan'];echo tgl_indo($tgllong); ?>
            bahwasannya benar <?php echo $r['nama']; ?> Pekerjaannya seorang <?php echo $r['pekerjaan']; ?> dan berpenghasilan lebih kurang 
           <?php $rupiah=number_format($r[penghasilan],0,",","."); echo"Rp. $rupiah,-"; ?>
            . Adapun surat keterangan ini dibuat untuk <?php echo $r['peruntukan']; ?></p>
          </div></td>
        </tr>
        <tr>
          <td colspan="2"><div align="justify"></div></td>
        </tr>
        <tr>
          <td colspan="2"><p align="justify" class="style23">Demikian Surat Keterangan ini  diperbuat untuk dapat dipergunakan seperlunya. </p></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        
        <tr>
          <td rowspan="8"><div align="center">
            <div align="center"><img src="../../temp/srtpenghasilan.png" /></div></td>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><span class="tanggal">Medan,
            <?php $tgllong=$r['tglsurat'];echo tgl_indo($tgllong); ?>
          </span></div></td>
        </tr>
        
        <tr>
          <td width="33%" rowspan="5">&nbsp;</td>
          <td><div align="right"></div></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td><div align="center"><strong class="ttd">LURAH <?php echo $dtlur['nama_kel']; ?></strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div align="center"><strong class="ttd">KECAMATAN <?php echo $dtlur['nama_kec']; ?></strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td><div align="center" class="ttd"><strong><?php echo $dtlur['nama_lurah']; ?></strong></div></td>
        </tr>
        <tr>
          <td width="19%">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><strong class="ttd">NIP. <?php echo $dtlur['nip_lurah']; ?></strong></div></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Berpenghasilan";
$namafile = "srtpenghasilan.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>