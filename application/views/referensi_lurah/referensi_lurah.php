<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <h4 class="card-title">Referensi Lurah</h4>
                </div>
                <div class="card-body">
				
                  <div class="toolbar">
				  <form action="<?php echo base_url(); ?>Referensi_lurah/tambah_lurah" method="POST">
                    <button name="tambah_lurah" class="btn btn-primary btn-round btn-fab"  data-toggle="tooltip" data-placement="top" title="Tambah Lurah">
                      <i class="material-icons">person_add</i>
                    </button>
				  </form>	
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
						              <th>No</th>
                          <th>NIP</th>
                          <th>Nama Lurah</th>
                          <th>Kecamatan</th>
                          <th>Kelurahan</th>
                          <th>Kode</th>
                          <th>KOP</th>
                         
                          
                          <th width="130"><center>Aksi</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                        <?php 
						$no=1;
						foreach($lurah as $l) { ?>
                        <tr>
						  <td><?php echo $no++; ?></td>
                          <td><?php echo $l->nip; ?></td>
                          <td><?php echo $l->nama_lurah; ?></td>
                          <td><?php echo $l->nama_kec; ?></td>
                          <td><?php echo $l->nama_kel; ?></td>
                          <td><?php echo $l->kode_lurah; ?></td>
                          <td><?php echo $l->kop; ?></td>
                          
                          <td>
						  <table class="table table-striped table-no-bordered table-hover">
							<tr>
								<td>
									<form action="<?php echo base_url() ?>Referensi_lurah/edit" method="POST">
									<input type="hidden" name="id_lurah" value="<?php echo $l->id_lurah; ?>">
										<button  class="btn btn-round btn-fab btn-info"><i class="material-icons">edit</i></button>
									</form>	
								</td>
								<td>
									<form action="<?php echo base_url() ?>Referensi_lurah/hapus" method="POST">
									<input type="hidden" name="id_lurah" value="<?php echo $l->id_lurah; ?>">
									<button  class="btn btn-round btn-fab btn-danger"><i class="material-icons">close</i></button>
									</form>
								</td>
							</tr>
						  </table>	
                          </td>
                        </tr>
                        <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>