<?php 
if(isset($_POST['tambah_lurah'])){
?>
<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Referensi_lurah/simpan" method="POST" enctype="multipart/form-data">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">person_add</i>
                    </div>
                    <h4 class="card-title">Tambah Lurah</h4>
                  </div>
                  <div class="card-body ">
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">NIP</label>
                      <input type="text" class="form-control" name="nip" required="true">
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lurah</label>
                      <input type="text" class="form-control"  required="true" name="nama_lurah">
                    </div>
                    <div class="form-group">
                      <label for="Kecamatan" class="bmd-label-floating">Kecamatan</label>
                      <select type="text" id="select1" class="form-control select"  required="true" name="id_kec" style="margin-top:10px" >
                        <option></option>
                        <?php 
                        
                        foreach ($kecamatan as $k)
                        {  
                        ?>
                          <option value="<?php echo $k->id_kec; ?>"><?php echo $k->nama_kec; ?></option>
                        <?php } ?>
					            </select>
                  </div>

                    <div class="form-group">
                      <label for="Kelurahan" class="bmd-label-floating">Kelurahan</label>
                      <select type="text" id="select2" class="form-control select"  required="true" name="id_kel" style="margin-top:10px">
                      <option value=""></option>
                      
                      <?php 
                        
                        foreach ($kelurahan as $k)
                        {  
                        ?>
                          <option data-value="<?php echo $k->id_kec; ?>" value="<?php echo $k->id_kel; ?>"><?php echo $k->nama_kel; ?></option>
                        <?php } ?>
					            </select>
                    </div>
                    
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">KODE</label>
                      <input type="text" class="form-control" name="kode_lurah" required="true">
                    </div>

                    <div class="">
                    <label for="">KOP</label><br>
                  <div>
                  <img id="uploadPreview" style="width: 1000px; height: 235px" src="<?php echo base_url(); ?>assets/assets/images/kop_surat/kop_surat.png"  /> 
                  <input type="file"  name="kop" id="uploadImage" onchange="PreviewImage();" />
                  </div>  
                  </div>
                    
                  <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                    <a href="<?php echo base_url(); ?>Referensi_surat" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="simpan" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
            
            
            
          </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<!-- <script>
			//gambar plank
			function readURLUser(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			 
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(255); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script> -->
<script type="text/javascript">
function PreviewImage() {
var oFReader = new FileReader();
oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
oFReader.onload = function (oFREvent)
 {
    document.getElementById("uploadPreview").src = oFREvent.target.result;
};
};
</script>

<?php  } else { ?>
<script type='text/javascript'>
  setTimeout(function () {  
   swal({
    title: 'DATA SLIDER',
    text:  'Tidak ada Hak Akses',
    type:  'warning',
    timer: 2000,
    showConfirmButton: true
   });  
  },10); 
  window.setTimeout(function(){ 
   window.location.replace('<?php echo base_url('Slider'); ?>');
  } ,2000); 
</script>
<?php } ?>