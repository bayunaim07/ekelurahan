<?php
error_reporting(0);

?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman", Times, serif; font-size: 18px; font-style: normal; font-weight: bold; text-align: center; text-decoration: underline; }
body {
	margin-left: 1cm;
}
.style7 {font-size: 16px; }
.style8 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 24px; font-style: normal; text-align: center; }
.style3 {font-family: Arial, Helvetica, sans-serif;
	font-size: 20pt;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style9 {font-size: 20pt;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
}
.tanggal {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
}
.ttd {text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
}
</style>
<?php foreach($cetak as $n) {} 
?>
<table width="1000">
  <tr>
    <td width="1238">
      <table width="100%">
        <tr>
          <td class="nomor"><img src="<?php echo base_url() ?>images/kop_surat/<?php echo $n->kop; ?>" width="977" height="250" /></td>
        </tr>
        <tr>
          <td class="nomor">&nbsp;</td>
        </tr>
        <tr>
          <td class="nomor"><span class="style3">SURAT KETERANGAN</span></td>
        </tr>
        <tr>
            <?php
              $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $bln = $array_bln[date('n')];
            ?>
          <td class="nomor"><strong class="style9">Nomor : <?php echo $n->kode; ?> / <?php echo $n->nosurat; ?> / <?php echo $n->kode_lurah; ?>/ <?php echo $bln; ?> /<?php echo date(Y) ?></strong></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="isi">&nbsp;</td>
        </tr>
      </table>
      <table width="97%">
        <tr>
          <td width="100%" colspan="4"><p align="justify"><span class="style9">Lurah <?php echo $n->nama_kel;?> Kecamatan <?php echo $n->nama_kel; ?> Pemerintah Kota Binjai,  dengan ini menerangkan berdasarkan Kartu Keluarga Nomor <?php echo $n->nik; ?> bahwa :</span></p></td>
        </tr>
        
        
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <table width="97%">
        <tr>
          <td>
            <table width="101%">
            <tr>
              <td width="11%">&nbsp;</td>
              <td width="33%" class="style9">1. Nama Lengkap </td>
              <td width="2%" class="style9">:</td>
              <td width="54%" class="style9"><?php echo $n->nama; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style9">2. NIK</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->nik;?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">3. NO. KK</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->kk;?></td>
            </tr>
			
            <tr>
              <td>&nbsp;</td>
              <td class="style9">4. Tempat/ Tangal Lahir</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->tempat;?>/<?php $date = date_create($n->tgllahir); echo date_format($date, "d-m-Y");?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">5. Jenis Kelamin</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->jenkel; ?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">6. Kewarganegaraan</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->warga_negara; ?></td>
            </tr>
           
            <tr>
              <td>&nbsp;</td>
              <td class="style9">7. Agama</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->agama; ?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">8. Status Perkawinan</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->status; ?></td>
            </tr>
            
            
            <tr>
              <td>&nbsp;</td>
              <td class="style9">9. Pekerjaan</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->namapekerjaan; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style9">10. Alamat</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->alamat; ?></td>
            </tr>
            

            <tr>
              <td>&nbsp;</td>
              <td class="isi">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            
            
          </table>
        </td>
        </tr>
      </table>
      <table width="97%">
        <tr>
          <td width="99%" colspan="2"><p align="justify" class="style9">dilaporkan telah meninggal dunia pada :</p></td>
        </tr>
       	</table>
         <br/>
         <table width="97%">
        <tr>
          <td>
            <table width="101%">
            <tr>
              <td width="11%">&nbsp;</td>
              <td width="33%" class="style9">11. Tanggal </td>
              <td width="2%" class="style9">:</td>
              <td width="54%" class="style9"><?php echo $n->tglmati; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style9">12. Tempat Kematian</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->tempatmati;?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">13. Kelurahan</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->nama_kel;?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9"> Kelurahan</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->nama_kec;?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9"> Kota</td>
              <td class="style9">:</td>
              <td class="style9">Binjai</td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9"> Provinsi</td>
              <td class="style9">:</td>
              <td class="style9">Sumatera Utara</td>
            </tr>
			
            <tr>
              <td>&nbsp;</td>
              <td class="style9">14. Sebab Kematian</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->sebab;?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">15. Yang Menentukan</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->namaket; ?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">16. Ket. Visum</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->visum; ?></td>
            </tr>
           
            <tr>
              <td>&nbsp;</td>
              <td class="style9">17. Yang Melapor</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->pelapor; ?></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td class="style9">18. Status Keluarga</td>
              <td class="style9">:</td>
              <td class="style9"><?php echo $n->hubungan; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="isi">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            
            
          </table>
        </td>
        </tr>
      </table>
		<table width="97%">
        <tr>
          <td width="1106" colspan="2"><p align="center" class="style9">Demikian Surat Pengantar ini di buat atas permintaaan pelapor untuk pengurusan AKTA KEMATIAN di Dinas Kependudukan dan Catatan Sipil Kota Binjai</p></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
     
      <table width="100%">
        
            <tr>
              	<td rowspan="10"><div align="center">
				  <div align="center" class="style9">Yang Melapor</div>
					<br/><br/><br/><br/><br/><br/><br/>
					<div align="center">_________________</div>
				</td>
            </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td width="52%">
            <div align="center">
              <span class="tanggal">Binjai, <?php $date = date_create($n->tgl_surat); echo date_format($date, "d-m-Y");?></span>
            </div></td>
        </tr>
        
        <tr>
          <td width="13%" rowspan="6">&nbsp;</td>
          <td><div align="right"></div></td>
        </tr>
        <tr>
          <td width="4%">&nbsp;</td>
          <td><div align="center"><strong class="ttd">LURAH <?php echo $n->nama_kel; ?></strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td><div align="center" class="ttd"><strong><?php echo $n->nama_lurah; ?></strong></div></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td><div align="center" class="ttd"><strong><?php echo $n->jabatan; ?></strong></div></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
          <td><div align="center"><strong class="ttd">NIP.<?php echo $n->nip; ?></strong></div></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br/>
<table>
  <tr>
    <td colspan="2"> NB : Surat Keterangan ini berlaku selama 1 (satu) bulan sejak dikeluarkan.</td>
  </tr>
</table>

<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>