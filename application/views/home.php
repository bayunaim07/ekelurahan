<div class="content">
          <div class="container-fluid">
            
                     
                        <br>
                        <div class="row">
                          <div class="col-md-4">
                            <div class="card card-product">
                              <div class="card-header card-header-image" data-header-animation="true">
                                <a href="#pablo">
                                  <img class="img" src="<?php echo base_url() ?>images/nikah.png">
                                </a>
                              </div>
                              <div class="card-body">
                                
                                <h4 class="card-title">
                                  <a href="#pablo">Surat Keterangan Nikah</a>
                                </h4>
                                <div class="card-description">
								 <strong>
								<?php
									$this->db->select('*');
								
									
									$nikah = $this->db->get('srtaktanikah');
									echo $jlh_nikah = $nikah->num_rows();
									
									?>
                                </strong>
                                </div>
                              </div>
                              <div class="card-footer">
                               <a class="btn btn-success btn-block" href="<?php echo base_url() ?>Surat/srtnikah">Lihat selengkapnya <i class="fa fa-arrow-circle-o-right"></i></a>
                              </div>
                            </div>
                          </div>
						  <div class="col-md-4">
                            <div class="card card-product">
                              <div class="card-header card-header-image" data-header-animation="true">
                                <a href="#pablo">
                                  <img class="img" src="<?php echo base_url() ?>images/kematian.png">
                                </a>
                              </div>
                              <div class="card-body">
                                
                                <h4 class="card-title">
                                  <a href="#pablo">Surat Keterangan Kematian</a>
                                </h4>
                                <div class="card-description">
                                  <strong>
								   <?php
									$this->db->select('*');							
									$mati = $this->db->get('srtkematian');
									echo $jlh_mati = $mati->num_rows();
									
									?>
                                </strong>
                                </div>
                              </div>
                              <div class="card-footer">
                               <a class="btn btn-success btn-block" href="">Lihat selengkapnya <i class="fa fa-arrow-circle-o-right"></i></a>
                              </div>
                            </div>
                           </div>
                          <div class="col-md-4">
                            <div class="card card-product">
                              <div class="card-header card-header-image" data-header-animation="true">
                                <a href="">
                                  <img class="img" src="<?php echo base_url() ?>images/ahliwaris.png">
                                </a>
                              </div>
                              <div class="card-body">
                                
                                <h4 class="card-title">
                                  <a href="">Surat Keterangan Ahli Waris</a>
                                </h4>
                                <div class="card-description">
                                  <strong>
								   <?php
									$this->db->select('*');							
									$waris = $this->db->get('srtwaris');
									echo $jlh_waris = $waris->num_rows();
									
									?>
                                </strong>
                                </div>
                              </div>
                              <div class="card-footer">
                               <a class="btn btn-success btn-block" href="">Lihat selengkapnya <i class="fa fa-arrow-circle-o-right"></i></a>
                              </div>
                            </div>
                           </div>
							<div class="col-md-4">
                            <div class="card card-product">
                              <div class="card-header card-header-image" data-header-animation="true">
                                <a href="">
                                  <img class="img" src="<?php echo base_url() ?>images/cerai.png">
                                </a>
                              </div>
                              <div class="card-body">
                                
                                <h5 class="card-title">
                                  <a href=""><small>Surat Keterangan Pengurusan Perceraian</small></a>
                                </h5>
                                <div class="card-description">
                                  <strong>
								   <?php
									$this->db->select('*');							
									$waris = $this->db->get('srtwaris');
									echo $jlh_waris = $waris->num_rows();
									
									?>
                                </strong>
                                </div>
                              </div>
                              <div class="card-footer">
                               <a class="btn btn-success btn-block" href="">Lihat selengkapnya <i class="fa fa-arrow-circle-o-right"></i></a>
                              </div>
                            </div>
                           </div>
						   <div class="col-md-4">
                            <div class="card card-product">
                              <div class="card-header card-header-image" data-header-animation="true">
                                <a href="">
                                  <img class="img" src="<?php echo base_url() ?>images/kurang_mampu.png">
                                </a>
                              </div>
                              <div class="card-body">
                                
                                <h5 class="card-title">
                                  <a href="">Surat Keterangan Kurang Mampu</a>
                                </h5>
                                <div class="card-description">
                                  <strong>
								   <?php
									$this->db->select('*');							
									$waris = $this->db->get('srttdkmampu');
									echo $jlh_waris = $waris->num_rows();
									
									?>
                                </strong>
                                </div>
                              </div>
                              <div class="card-footer">
                               <a class="btn btn-success btn-block" href="">Lihat selengkapnya <i class="fa fa-arrow-circle-o-right"></i></a>
                              </div>
                            </div>
                           </div>
						   <div class="col-md-4">
                            <div class="card card-product">
                              <div class="card-header card-header-image" data-header-animation="true">
                                <a href="">
                                  <img class="img" src="<?php echo base_url() ?>images/skck.png">
                                </a>
                              </div>
                              <div class="card-body">
                                
                                <h5 class="card-title">
                                  <a href=""><small>Surat Keterangan Pengurusan SKCK</small></a>
                                </h5>
                                <div class="card-description">
                                  <strong>
								   <?php
									$this->db->select('*');							
									$waris = $this->db->get('srtskck');
									echo $jlh_waris = $waris->num_rows();
									
									?>
                                </strong>
                                </div>
                              </div>
                              <div class="card-footer">
                               <a class="btn btn-success btn-block" href="">Lihat selengkapnya <i class="fa fa-arrow-circle-o-right"></i></a>
                              </div>
                            </div>
                           </div>
                        </div>
                      </div>
                    </div>