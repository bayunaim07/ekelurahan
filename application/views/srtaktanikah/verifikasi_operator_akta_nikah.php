<?php foreach($verifikasi_akta_nikah as $n) {} 
   ?>
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Verifikasi_surat/verifikasi_surat_akta_nikah" method="POST" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="id_an" value="<?php echo $n->id_an; ?>" >  
              <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Verifikasi Surat Akta Nikah</h4>
                  </div>
				  
                  <div class="card-body">
					    <div class="form-group">
                            <br>
                            <h4 class="bmd-label-floating text-success"><b>I. DATA WARGA</b></h4>
				        </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK *</label>
                      <input type="text" class="form-control" name="nik"  required="true" value="<?php echo $n->nik; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama" value="<?php echo $n->nama; ?>">
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat" value="<?php echo $n->nik; ?>" >
                    </div>

					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir" value="<?php echo $n->tgllahir; ?>">
                    </div>
          
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Kecamatan</label>
                        <select type="text" id="select1" class="form-control select"  required="true" name="namakec" style="margin-top:10px">
                            <?php foreach ($kecamatan as $ke) { ?>
                            <option <?php if($ke->id_kec == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $ke->id_kec; ?>"><?php echo $ke->nama_kec;?> </option>
                            <?php } ?>
					    </select>
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Kelurahan</label>
                        <select type="text" id="select2" class="form-control select"  required="true" name="namakel" style="margin-top:10px">
                            <?php foreach ($kelurahan as $kel) { ?>
                            <option <?php if($kel->id_kel == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $kel->id_kel; ?>"><?php echo $kel->nama_kel;?> </option>
                            <?php } ?>
					    </select>
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                        <select type="text" class="form-control select2"  required="true" name="pekerjaan"  >
                            <?php foreach ($pekerjaan as $pe) { ?>
                                <option <?php if($pe->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $pe->id; ?>"><?php echo $pe->namapekerjaan;?> </option>
                            <?php } ?>
					    </select>
                    </div>

					<div class="form-group">
                        <label for="" class="bmd-label-floating">Status Perkawinan</label>
                        <select type="text" class="form-control select2"  required="true" name="status"  >
                            <?php foreach ($status as $st) { ?>
                                <option <?php if($st->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $st->id; ?>"><?php echo $st->status_perkawinan;?> </option>
                            <?php } ?>
                        </select>
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                        <select type="text" class="form-control select2"  required="true" name="agama" >
					 	    <?php foreach ($agama as $a) { ?>
                                <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                            <?php } ?>
					    </select>
                    </div>

				    <div class="form-group">
                      <label for="" class="bmd-label-floating">Jenis Kelamin</label>
                      <select type="text" class="form-control select2"  required="true" name="jenkel" >
                        <option value="<?php echo $n->jenkel; ?>"><?php echo $n->jenkel; ?></option>
                        <option value="Laki-Laki">Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                    </div>

		            <div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat"  value="<?php echo $n->alamat; ?>">
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Lampirkan KTP <br>(*  file berupa jpg,png,jpeg)</label>
                        <div class="btn btn-default btn-file" style="width : 500px">
                            <a href="#" class="pop">
                                <img src="<?php echo base_url(); ?>images/surat_akta_nikah/<?php echo $n->foto_ktp; ?>" style="width: 400px; height: 264px;">
                            </a>
                        </div>
                    </div>	

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Lampirkan Kartu Keluarga <br>(*  file berupa jpg,png,jpeg)</label>
                        <div class="btn btn-default btn-file" style="width : 500px">
                        <a href="#" class="pop">
                                <img src="<?php echo base_url(); ?>images/surat_akta_nikah/<?php echo $n->foto_kk; ?>" style="width: 400px; height: 264px;">
                            </a>
                        </div>
                    </div>	

                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                   <?php if ($n->cekopt==1){?>
                  <div class="card-footer text-right" style="">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Verifikasi_surat/srtaktanikah" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                  </div>
                  <?php } else if ($n->cekopt==2){?>
                  <div class="card-footer text-right" style="">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Verifikasi_surat/srtaktanikah" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                  </div>
                  <?php } else{ ?>
                    <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                    </div>
                  </div>
                </div>
              </form>
              <div class="row" style="justify-content:center">
              <form method="POST" action="<?php echo base_url(); ?>Verifikasi_surat/terima_surat_akta_nikah"  enctype="multipart/form-data" style="position: relative;top: -90px;left: 20px;">
                  <input type="hidden" name="id_an" value="<?php echo $n->id_an ?>">
                  <button  name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Terima</button>
              </form>

              <form method="POST" action="<?php echo base_url(); ?>Verifikasi_surat/tolak_akta_nikah"  enctype="multipart/form-data" style="position: relative;top: -90px;left: 20px;">
                  <input type="hidden" name="id_an" value="<?php echo $n->id_an ?>">
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Tolak</button>
              </form>
              </div>
            <?php }?>

            </div>
          </div>
        
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style=" overflow-y: initial !important">
    <div class="modal-content">              
      <div class="modal-body" style=" height: 80vh;overflow-y: auto;">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;">
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
			function readURLUser(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
			function readURLUser1(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser1') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
    $(function () {
     $('input[type="file"]').change(function () {
          if ($(this).val() != "") {
                 $(this).css('color', '#333');
          }else{
                 $(this).css('color', 'transparent');
          }
     });
})
</script>

<script>
    $(function() {
		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});		
});
</script>
