<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Surat/simpan_srtnikah" method="POST" enctype="multipart/form-data">
					
			    <input type="hidden" class="form-control" value="<?php echo $kode; ?>" name="nosurat" >
				
			    <input type="hidden" class="form-control" name="tgl_surat" value="<?php  echo  Date('Y-m-d'); ?>" >
				<input type="hidden" class="form-control" name="tgl_daftar" value="<?php  echo  Date('Y-m-d'); ?>" >
				<input type="hidden" class="form-control" name="tahun" value="<?php  echo  Date('Y'); ?>" >
				<?php
				$a= Date('Ymd');
				?>
				<input type="hidden" class="form-control" name="noreg" value="<?php  echo $a.$kode; ?>" >
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Tambah Surat Pengantar Perkawinan</h4>
                  </div>
				  
                  <div class="card-body">
					   <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>I. DATA WARGA</b></h4>
					   </div>
             <?php foreach ($profil as $p) {?>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK *</label>
                      <input type="text" class="form-control" name="nik" required="true" value="<?php echo $p->nik;?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama" value="<?php echo $p->nama;?>" readonly>
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat" value="<?php echo $p->tempat;?>" readonly>
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir" value="<?php echo $p->tgllahir;?>" readonly>
                    </div>

					
					
					<div class="form-group">
            <label for="" class="bmd-label-floating">Agama</label>
            <select type="text" class="form-control select2"  required="true" name="agama"  >
					    <option value="<?php echo $p->id;?>"><?php echo $p->agama;?></option>
					  </select>
          </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating">Jenkel</label>
                      <select type="text" class="form-control select2"  required="true" name="jenkel"  >
					  <option value="<?php echo $p->jenkel;?>"><?php echo $p->jenkel;?></option>
					  </select>
                    </div>
					
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganegaraan *</label>
                      <input type="text" class="form-control"  required="true" name="warga_negara" value="<?php echo $p->bangsa;?>" readonly>
                    </div>

                    <?php }?>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan" >
					  <option></option>
					  <?php 
					  
					  foreach ($pekerjaan as $p)
						{  
						?>
					    <option value="<?php echo $p->id; ?>"><?php echo $p->namapekerjaan; ?></option>
						<?php } ?>
					  </select>
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat" >
                    </div>
                    

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Kecamatan</label>
                      <select type="text" id="select1" class="form-control select"  required="true" name="namakec" style="margin-top:10px">
                        <option></option>
                        <?php 
                        
                        foreach ($kecamatan as $k)
                        {  
                        ?>
                          <option value="<?php echo $k->id_kec; ?>"><?php echo $k->nama_kec; ?></option>
                        <?php } ?>
					            </select>
                  </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Kelurahan</label>
                      <select type="text" id="select2" class="form-control select"  required="true" name="namakel" style="margin-top:10px">
                      <option value=""></option>
                      
                      <?php 
                        
                        foreach ($kelurahan as $k)
                        {  
                        ?>
                          <option data-value="<?php echo $k->id_kec; ?>" value="<?php echo $k->id_kel; ?>"><?php echo $k->nama_kel; ?></option>
                        <?php } ?>
					            </select>
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Status Perkawinan</label>
                      <select type="text" class="form-control select2"  required="true" name="status" >
					  <option></option>
					  <?php 
					  $this->db->select('*');
					  $st_kawin=$this->db->get('status_perkawinan');
					  foreach ($st_kawin->result() as $s)
						{  
						?>
					    <option value="<?php echo $s->id; ?>"><?php echo $s->status_perkawinan; ?></option>
						<?php } ?>
					  </select>
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Pasangan Terdahulu</label>
                      <input type="text" class="form-control"  name="pasangan_terdahulu" >
                    </div>

                    

					<div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>II. DATA AYAH</b></h4>
					   </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Ayah *</label>
                      <input type="text" class="form-control"   required="true" name="nama_ayah" >
                    </div>

                     <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK Ayah *</label>
                      <input type="text" class="form-control"   required="true" name="nik_ayah" >
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir  *</label>
                      <input type="text" class="form-control"  required="true" name="tempat_ayah" >
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama_ayah" >
					 <option></option>
					 <?php 
					  $this->db->select('*');
					  $agama=$this->db->get('mstagama');
					  foreach ($agama->result() as $a)
						{  
						?>
							<option value="<?php echo $a->id ?>"><?php echo $a->agama; ?></option>
						<?php } ?>	
					  </select>
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganegaraan *</label>
                      <input type="text" class="form-control"  required="true" name="bangsa_ayah">
                    </div>

					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir  *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir_ayah">
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat Ayah  *</label>
                      <input type="text" class="form-control"  required="true" name="alamat_ayah" >
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan Ayah</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan_ayah" >
					  <option></option>
					  <?php 
					  
					  foreach ($pekerjaan as $p)
						{  
						?>
					    <option value="<?php echo $p->id; ?>"><?php echo $p->namapekerjaan; ?></option>
						<?php } ?>
					  </select>
                    </div>

                    <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>III. DATA IBU</b></h4>
					   </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Ibu *</label>
                      <input type="text" class="form-control"   required="true" name="nama_ibu" >
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK Ibu *</label>
                      <input type="text" class="form-control"   required="true" name="nik_ibu" >
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir  *</label>
                      <input type="text" class="form-control"  required="true" name="tempat_ibu" >
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir  *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir_ibu">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama_ibu" >
					 <option></option>
					 <?php 
					  $this->db->select('*');
					  $agama=$this->db->get('mstagama');
					  foreach ($agama->result() as $a)
						{  
						?>
							<option value="<?php echo $a->id ?>"><?php echo $a->agama; ?></option>
						<?php } ?>	
					  </select>
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganegaraan *</label>
                      <input type="text" class="form-control"  required="true" name="bangsa_ibu">
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat Ibu  *</label>
                      <input type="text" class="form-control"  required="true" name="alamat_ibu" >
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan Ibu</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan_ibu" >
					  <option></option>
					  <?php 
					  
					  foreach ($pekerjaan as $p)
						{  
						?>
					    <option value="<?php echo $p->id; ?>"><?php echo $p->namapekerjaan; ?></option>
						<?php } ?>
					  </select>
                    </div>


                    <h5 for="" class="title"> Upload KTP (dalam Bentuk JPG atau PNG) <span class="text-danger">*</span></h5>
                            <div class="form-group" id="foto_ktp">
                                <div class="col-md-6 col-sm-6">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <?php
                                            if(empty($n->foto_ktp)){

                                                ?>
                                                <img src="<?php echo base_url() ?>../images/logo/dokumen.png" >
                                            <?php } else { ?>
                                                <img src="<?php echo base_url() ?>images/surat_nikah/<?php echo $n->foto_ktp ?>" >
                                            <?php } ?>
                                        </div>

                                    </div>

                                </div>

                                <span id="foto_ktp_error" class="text-danger"></span>
                            </div>
                            <span class="uploaded_image"></span>
                            <div>
                                <?php
                                if(empty($n->foto_ktp)){

                                    ?>
                                    <input type="file" name="foto_ktp" id="file" class="foto_ktp" />
                                <?php } else { ?>
                                    <input type="file" name="foto_ktp" id="file"  />
                                <?php } ?>
                            </div>
                            <br>
                            <hr>	

                   <h5 for="" class="title"> Upload KK (dalam Bentuk JPG atau PNG) <span class="text-danger">*</span></h5>
                            <div class="form-group" id="foto_kk">
                                <div class="col-md-6 col-sm-6">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <?php
                                            if(empty($n->foto_kk)){

                                                ?>
                                                <img src="<?php echo base_url() ?>../images/logo/dokumen.png" >
                                            <?php } else { ?>
                                                <img src="<?php echo base_url() ?>images/surat_nikah/<?php echo $n->foto_kk ?>" >
                                            <?php } ?>
                                        </div>

                                    </div>

                                </div>

                                <span id="foto_kk_error" class="text-danger"></span>
                            </div>
                            <span class="uploaded_image2"></span>
                            <div>
                                <?php
                                if(empty($n->foto_kk)){

                                    ?>
                                    <input type="file" name="foto_kk" id="file2" class="foto_kk" />
                                <?php } else { ?>
                                    <input type="file" name="foto_kk" id="file2"  />
                                <?php } ?>
                            </div>
                            <br>
                            <hr>

                   

                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Surat/srtnikah" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="simpan" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
    $(document).ready(function(){
        $(document).on('change', '#file2', function(){
            var name = document.getElementById("file2").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
            {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file2").files[0]);
            var f = document.getElementById("file2").files[0];
            var fsize = f.size||f.fileSize;
            if(fsize > 20000000000)
            {
                alert("Ukuran File Gambar Terlalu Besar Maksimal 20MB");
            }
            else
            {
                form_data.append("file2", document.getElementById('file2').files[0]);
                $.ajax({
                    url:"<?php echo base_url() ?>index.php/Surat/simpan_foto_kk",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        $('.uploaded_image2').html("<label class='text-success'>Sedang Mengupload Gambar...</label>");
                    },
                    success:function(data)
                    {
                        $('.uploaded_image2').html("<h4 class='text-success'><span class='fa fa-check-circle-o'></span> Data Berhasil Diupload</h4>");
                        $("#foto_kk").load(" #foto_kk");

                    }
                });
            }
        });
    });
</script>


<script>
    $(document).ready(function(){
        $(document).on('change', '#file', function(){
            var name = document.getElementById("file").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
            {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file").files[0]);
            var f = document.getElementById("file").files[0];
            var fsize = f.size||f.fileSize;
            if(fsize > 20000000000)
            {
                alert("Ukuran File Gambar Terlalu Besar Maksimal 20MB");
            }
            else
            {
                form_data.append("file", document.getElementById('file').files[0]);
                $.ajax({
                    url:"<?php echo base_url() ?>index.php/Surat/simpan_foto_ktp",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        $('.uploaded_image').html("<label class='text-success'>Sedang Mengupload Gambar...</label>");
                    },
                    success:function(data)
                    {
                        $('.uploaded_image').html("<h4 class='text-success'><span class='fa fa-check-circle-o'></span> Data Berhasil Diupload</h4>");
                        $("#foto_ktp").load(" #foto_ktp");

                    }
                });
            }
        });
    });
</script>

<script>
    $(function () {
     $('input[type="file"]').change(function () {
          if ($(this).val() != "") {
                 $(this).css('color', '#333');
          }else{
                 $(this).css('color', 'transparent');
          }
     });
})
</script>


<script>
    $(document).ready(function(){
        $(document).on('change', '#file2', function(){
            var name = document.getElementById("file2").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
            {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file2").files[0]);
            var f = document.getElementById("file2").files[0];
            var fsize = f.size||f.fileSize;
            if(fsize > 20000000000)
            {
                alert("Ukuran File Gambar Terlalu Besar Maksimal 20MB");
            }
            else
            {
                form_data.append("file2", document.getElementById('file2').files[0]);
                $.ajax({
                    url:"<?php echo base_url() ?>index.php/Surat/simpan_foto_kk",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        $('.uploaded_image2').html("<label class='text-success'>Sedang Mengupload Gambar...</label>");
                    },
                    success:function(data)
                    {
                        $('.uploaded_image2').html("<h4 class='text-success'><span class='fa fa-check-circle-o'></span> Data Berhasil Diupload</h4>");
                        $("#foto_kk").load(" #foto_kk");

                    }
                });
            }
        });
    });
</script>


<script>
    $(document).ready(function(){
        $(document).on('change', '#file', function(){
            var name = document.getElementById("file").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
            {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file").files[0]);
            var f = document.getElementById("file").files[0];
            var fsize = f.size||f.fileSize;
            if(fsize > 20000000000)
            {
                alert("Ukuran File Gambar Terlalu Besar Maksimal 20MB");
            }
            else
            {
                form_data.append("file", document.getElementById('file').files[0]);
                $.ajax({
                    url:"<?php echo base_url() ?>index.php/Surat/simpan_foto_ktp",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        $('.uploaded_image').html("<label class='text-success'>Sedang Mengupload Gambar...</label>");
                    },
                    success:function(data)
                    {
                        $('.uploaded_image').html("<h4 class='text-success'><span class='fa fa-check-circle-o'></span> Data Berhasil Diupload</h4>");
                        $("#foto_ktp").load(" #foto_ktp");

                    }
                });
            }
        });
    });
</script>
