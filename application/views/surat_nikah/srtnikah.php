
<script type="text/javascript">
    $(function() {
        $( "#tanggal" ).datepicker({ altFormat: 'yy-mm-dd' });
        $( "#tanggal" ).change(function() {
             $( "#tanggal" ).datepicker( "option", "dateFormat","yy-mm-dd" );
         });
    });
    </script>
	
<script type="text/javascript">
    $(function() {
        $( "#tgllahir" ).datepicker({ altFormat: 'yy-mm-dd' });
        $( "#tgllahir" ).change(function() {
             $( "#tgllahir" ).datepicker( "option", "dateFormat","yy-mm-dd" );
         });
    });
    </script>
	

<?php
if (empty($_SESSION['ses_user']) AND empty($_SESSION['ses_password'])){  
  header('location:404.php');
}
else{
	$aksi = "modul/srtnikah/aksi_srtnikah.php";
	 // mengatasi variabel yang belum di definisikan (notice undefined index)
  $act = isset($_GET['act']) ? $_GET['act'] : ''; 

  switch($act){
    default:
	 $srtnikah = pg_query($koneksi, "SELECT * FROM srtnikah where tahun='$_SESSION[thnaktif]'");
  $count=pg_num_rows($srtnikah);
	echo"
	
	<div class='box'>
                <div class='box-header'>
                  <h2 class='box-title'>TAMBAH DATA SURAT KETERANGAN NIKAH</h2>";
                ?>
				<form method="post" name="frm">
		
			<div style="text-align:right">		
		  <a  class="btn bg-green margin"  data-toggle="tooltip" data-placement="top" title="Menu Jenis Surat" href="?module=jenissurat"><i class="fa fa-send"></i>Jenis Surat</a>
          <a  class="btn bg-purple margin"  data-toggle="tooltip" data-placement="top" title="Tambah" href="?module=srtnikah&act=tambahsrtnikah"><i class="fa fa-send"></i> Tambah</a> 
		  
		<?php
		if($count > 0)
        {
		?>			
		   <button class="btn bg-orange btn-flat margin" data-toggle="tooltip" data-placement="top" title="lihat"  onClick="view_records_srtnikah();" ><i class="fa fa-desktop"></i> Lihat</button>
		  <button class="btn bg-olive btn-flat margin" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="update_records_srtnikah();" ><i class="fa fa-edit"></i> Edit</button> 
		  
		   <button class="btn bg-red btn-flat margin" data-toggle="tooltip" data-placement="top" title="Hapus"  onClick="delete_records_srtnikah();" ><i class="fa fa-remove"></i> Hapus</button> 
		  
		  </div>
			
		<?php } ?>
		
		 <div class='box-body'>
				<div class="box-body table-responsive no-padding">
                  <table id='example3' class='table table-bordered table-striped'>
                    <thead>
                      <tr>
					   <th>
					   <input type="checkbox"  name="select_all" id="select_all" value=""/>
					   </th>
                       <th>No</th>
						 <th></th>
						<th>Tgl.Daftar</th>
						<th>No.Reg</th>
						<th>NIK</th>
                        <th>Nama</th>
                        <th>Alamat</th>
						<th>Pekerjaan</th>
						<th>Agama</th>
						<th>User Entry</th>
						<th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php  
					$no = 1;
					$tim =pg_query($koneksi, "select * from srtnikah where tahun='$_SESSION[thnaktif]' order by tgldaftar desc");
					while ($tm=pg_fetch_array($tim)){       
					?>
                       <tr>
					  <td style='text-align:center'>
					  
					<?php 
						$cekopt=$tm[cekopt];
						if($cekopt=='1'){
					   ?> 
						<input  type="checkbox" name="chk[]" class="chk-box" value="<?php echo $tm['id'];?>"/>
						
					<?php } else { ?>
						<input  type="checkbox" disabled name="chk[]" class="chk-box" value="<?php echo $tm['id'];?>"/>
					<?php } ?>
					
                        </td> 
						<td><?php echo" $no"; ?></td>
						<td><?php
						$st=$tm['cekopt'];
						if($st=='1'){
							?>
						<span class="btn btn-success"><i class="fa fa-check"></i></span>	
							<?php
						} else {
							?>
							   <span class="btn btn-danger"><i class="fa fa-remove"></i></span>
							<?php
							echo "";
						}			
						?></td>
                       <td><?php echo" $tm[tgldaftar]"; ?></td>
                        <td><?php echo" $tm[noreg]"; ?></td>
                        <td><?php echo" $tm[nik]"; ?></td>
						<td><?php echo" $tm[nama]"; ?></td>
						<td><?php echo" $tm[alamat]"; ?></td>
						<td><?php echo" $tm[pekerjaan]"; ?></td>
						<td><?php echo" $tm[agama]"; ?></td>
						<td><?php echo" $tm[userentry]"; ?></td>
						<td><?php echo" $tm[keterangan]"; ?></td>
                      </tr>
					  <?php
                $no++;
              }
              ?> 
                    </tbody>
                    
                  </table>
				  </div>
				</form>
               </div>
			   
                
              </div>
	<?php		  
	 break;
	   case "tambahsrtnikah":
	   $a=$tgl_kode;
	   $rand4 = randpass4(3);
	 ?>
	 <center><h3 class="box-title">TAMBAH DATA SURAT KETERANGAN NIKAH</h3></center>
 
			<div class="box box-info">

                <div class="box-header with-border">
                  
                </div><!-- /.box-header -->
                <!-- form start -->
				
                <form class="form-horizontal" action="<?php echo $aksi;?>?module=srtnikah&act=input" method="POST" id="popup-validation" enctype="multipart/form-data">
				<div class="col-md-6">
                  <div class="box-body">
				  
				  <div class="form-group">
					  <label for="noreg" class="col-sm-4 control-label">
					  No.Reg <span class="text-danger"> *</span></label>
                      <div class="col-sm-5">
                        <input type="text" class="validate[required,custom[number]] form-control" readonly name="noreg" placeholder="No.Pendaftaran" value="<?php echo "$a$rand4"; ?>"  >
                      </div>
					</div>
				  
				  <div class="form-group">
					  <label for="tgldaftar" class="col-sm-4 control-label">Tgl. Daftar <span class="text-danger"> *</span></label>
                      <div class="col-sm-5">
                        <input type="text" class="validate[required,custom[date]] form-control" id="tanggal" name="tgldaftar" placeholder="YYYY-MM-DD"  value="<?php echo "$tgl_sekarang"; ?>">
                      </div>
				  </div>
				  
				  <div class="form-group">
                     <label for="warga" class="col-sm-4 control-label">I. Data Warga</label>
				   </div>
				   
                    <div class="form-group">
					<label for="nik" class="col-sm-4 control-label">NIK<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
					  <input type="hidden"  id="id" class="form-control" />
                        <input type="text" class="validate[required,custom[number]] form-control" name="nik" id="nik" placeholder="NIK">
                      </div>
					 </div>
					  
					  <div class="form-group">
					  <label for="nama" class="col-sm-4 control-label">Nama<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <input type="text" class="validate[required] form-control" name="nama" id="nama" placeholder="Nama Warga">
                       </div>
					  </div>
				   
				   <div class="form-group">
					  <label for="tempat" class="col-sm-4 control-label">Tempat Lahir<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <input type="text" class="validate[required] form-control" name="tempat"  placeholder="Tempat Lahir">
                       </div>
					  </div>
				   
				   <div class="form-group">
					  <label for="tgllahir" class="col-sm-4 control-label">Tgl.Lahir<span class="text-danger"> *</span></label>
                      <div class="col-sm-5">
                        <input type="text" class="validate[required,custom[date]] form-control" id="tgllahir" name="tgllahir" placeholder="YYYY-MM-DD">
                      </div>
				  </div>
				   
				    <div class="form-group">
					 <label for="jenkel" class="col-sm-4 control-label">Jenis Kelamin<span class="text-danger"> *</span></label>
					  <div class="col-sm-5">
                        <select class=" validate[required] form-control" name='jenkel'  id='jenkel'  >
							<option></option>
							<option value="Laki-Laki">Laki-Laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
                      </div>
					</div>
				   
				     <div class="form-group">
					 <label for="status" class="col-sm-4 control-label">Status Perkawinan<span class="text-danger"> *</span></label>
					  <div class="col-sm-5">
                        <select class=" validate[required] form-control" name='status' >
							<option><?php echo $r[status];?></option>
							<option value="Belum Pernah Menikah">Belum Pernah Menikah</option>
							<option value="Menikah">Menikah</option>
							<option value="Janda">Janda</option>
							<option value="Duda">Duda</option>
						</select>
                      </div>
					</div>
				   
				    <div class="form-group">
					  <label for="alamat" class="col-sm-4 control-label">Alamat<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <textarea type="text" rows="3" class="validate[required] form-control" name="alamat" id="alamat" placeholder="Alamat"></textarea>
                       </div>
					 </div>
					 
					 <div class="form-group">	
					  <label for="Pekerjaan" class="col-sm-4 control-label">Pekerjaan<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                       <select class="validate[required] form-control select2" name='pekerjaan' id='pekerjaan'>
						<option></option>
						<?php
									$pekerjaan = pg_query($koneksi, "SELECT * FROM mstpekerjaan order by id"); 
										while($p = pg_fetch_array($pekerjaan)){
													
											echo"
												<option value=\"$p[namapekerjaan]\">$p[namapekerjaan]</option>\n";
											}
										echo"";	
															  
															  
						?>									  								  
						</select>				
                      </div>
					</div>
					 
					  <div class="form-group">
					 <label for="agama" class="col-sm-4 control-label">Agama<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <select class=" validate[required] form-control" name='agama'  id='agama'  >
							<option></option>
							<option value="Islam">Islam</option>
							<option value="Kristen Protestan">Kristen Protestan</option>
							<option value="Katolik">Katolik</option>
							<option value="Hindu">Hindu</option>
							<option value="Budha">Budha</option>
							<option value="Konghucu">Konghucu</option>
						</select>
                      </div>
					</div>
					 
					 <div class="form-group">
					 <label for="warga_negara" class="col-sm-4 control-label">Warga Negara<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <select class=" validate[required] form-control" name='warga_negara'  id='warga_negara'  >
							<option></option>
							<option value="Indonesia">Indonesia</option>
							<option value="Asing">Asing</option>
						</select>
                      </div>
					</div>
					 
					 <div class="form-group">
                     <label for="ayah" class="col-sm-4 control-label">II. Data Ayah</label>
				    </div>
					
					<div class="form-group">
					<label for="nik_ayah" class="col-sm-4 control-label">NIK<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
					  <input type="hidden"  id="id" class="form-control" />
                        <input type="text" class="validate[required,custom[number]] form-control" name="nik_ayah" id="nik_ayah" placeholder="NIK Ayah">
                      </div>
					 </div>
					  
					  <div class="form-group">
					  <label for="nama_ayah" class="col-sm-4 control-label">Nama Ayah<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <input type="text" class="validate[required] form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Ayah">
                       </div>
					  </div>
				   
				   <div class="form-group">
					  <label for="tempat_ayah" class="col-sm-4 control-label">Tempat Lahir<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <input type="text" class="validate[required] form-control" name="tempat_ayah"  placeholder="Tempat Lahir Ayah">
                       </div>
					  </div>
				   
				   <div class="form-group">
					  <label for="tgllahir_ayah" class="col-sm-4 control-label">Tgl.Lahir<span class="text-danger"> *</span></label>
                      <div class="col-sm-5">
                        <input type="text" class="validate[required,custom[date]] form-control" id="tgllahir_ayah" name="tgllahir_ayah" placeholder="YYYY-MM-DD">
                      </div>
				  </div>
					
					<div class="form-group">
					  <label for="alamat_ayah" class="col-sm-4 control-label">Alamat Ayah<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <textarea type="text" rows="3" class="validate[required] form-control" name="alamat_ayah" id="alamat_ayah" placeholder="Alamat Ayah"></textarea>
                       </div>
					 </div>
					 
					 <div class="form-group">	
					  <label for="pekerjaan_ayah" class="col-sm-4 control-label">Pekerjaan<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                       <select class="validate[required] form-control select2" name='pekerjaan_ayah' id='pekerjaan_ayah'>
						<option></option>
						<?php
									$pekerjaan_ayah = pg_query($koneksi, "SELECT * FROM mstpekerjaan order by id"); 
										while($pa = pg_fetch_array($pekerjaan_ayah)){
													
											echo"
												<option value=\"$pa[namapekerjaan]\">$pa[namapekerjaan]</option>\n";
											}
										echo"";	
															  
															  
						?>									  								  
						</select>				
                      </div>
					</div>
					 
					<div class="form-group">
					 <label for="agama_ayah" class="col-sm-4 control-label">Agama Ayah<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <select class=" validate[required] form-control" name='agama_ayah'  id='agama_ayah'  >
							<option></option>
							<option value="Islam">Islam</option>
							<option value="Kristen Protestan">Kristen Protestan</option>
							<option value="Katolik">Katolik</option>
							<option value="Hindu">Hindu</option>
							<option value="Budha">Budha</option>
							<option value="Konghucu">Konghucu</option>
						</select>
                      </div>
					</div>
					 
					 <div class="form-group">
					 <label for="bangsa_ayah" class="col-sm-4 control-label">Warga Negara Ayah<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <select class=" validate[required] form-control" name='bangsa_ayah'  id='bangsa_ayah'  >
							<option></option>
							<option value="Indonesia">Indonesia</option>
							<option value="Asing">Asing</option>
						</select>
                      </div>
					</div>
					 
                  </div><!-- /.box-body -->
				</div>	
				
			<div class="col-md-6">
                  <div class="box-body">
				  	
					
					
					<div class="form-group">
                     <label for="ibu" class="col-sm-4 control-label">III. Data Ibu</label>
				    </div>
					
					<div class="form-group">
					<label for="nik_ibu" class="col-sm-4 control-label">NIK<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
					  <input type="hidden"  id="id" class="form-control" />
                        <input type="text" class="validate[required,custom[number]] form-control" name="nik_ibu" id="nik_ibu" placeholder="NIK Ibu">
                      </div>
					 </div>
					  
					  <div class="form-group">
					  <label for="nama_ibu" class="col-sm-4 control-label">Nama Ibu<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <input type="text" class="validate[required] form-control" name="nama_ibu" id="nama_ibu" placeholder="Nama Ibu">
                       </div>
					  </div>
				   
				   <div class="form-group">
					  <label for="tempat_ibu" class="col-sm-4 control-label">Tempat Lahir<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <input type="text" class="validate[required] form-control" name="tempat_ibu"  placeholder="Tempat Lahir Ibu">
                       </div>
					  </div>
				   
				   <div class="form-group">
					  <label for="tgllahir_ibu" class="col-sm-4 control-label">Tgl.Lahir<span class="text-danger"> *</span></label>
                      <div class="col-sm-5">
                        <input type="text" class="validate[required,custom[date]] form-control" id="tgllahir_ibu" name="tgllahir_ibu" placeholder="YYYY-MM-DD">
                      </div>
				  </div>
					
					<div class="form-group">
					  <label for="alamat_ibu" class="col-sm-4 control-label">Alamat Ibu<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <textarea type="text" rows="3" class="validate[required] form-control" name="alamat_ibu" id="alamat_ibu" placeholder="Alamat Ibu"></textarea>
                       </div>
					 </div>
					 
					 <div class="form-group">	
					  <label for="pekerjaan_ibu" class="col-sm-4 control-label">Pekerjaan<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                       <select class="validate[required] form-control select2" name='pekerjaan_ibu' id='pekerjaan_ibu'>
						<option></option>
						<?php
									$pekerjaan_ibu = pg_query($koneksi, "SELECT * FROM mstpekerjaan order by id"); 
										while($pa = pg_fetch_array($pekerjaan_ibu)){
													
											echo"
												<option value=\"$pa[namapekerjaan]\">$pa[namapekerjaan]</option>\n";
											}
										echo"";	
															  
															  
						?>									  								  
						</select>				
                      </div>
					</div>
					 
					  <div class="form-group">
					 <label for="agama_ibu" class="col-sm-4 control-label">Agama Ibu<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <select class=" validate[required] form-control" name='agama_ibu'  id='agama_ibu'  >
							<option></option>
							<option value="Islam">Islam</option>
							<option value="Kristen Protestan">Kristen Protestan</option>
							<option value="Katolik">Katolik</option>
							<option value="Hindu">Hindu</option>
							<option value="Budha">Budha</option>
							<option value="Konghucu">Konghucu</option>
						</select>
                      </div>
					</div>
					 
					 <div class="form-group">
					 <label for="bangsa_ibu" class="col-sm-4 control-label">Warga Negara Ibu<span class="text-danger"> *</span></label>
					  <div class="col-sm-7">
                        <select class=" validate[required] form-control" name='bangsa_ibu'  id='bangsa_ibu'  >
							<option></option>
							<option value="Indonesia">Indonesia</option>
							<option value="Asing">Asing</option>
						</select>
                      </div>
					</div>
					
					<div class="form-group">
					  <label for="ketpria" class="col-sm-4 control-label">Jika Pria Terangkan Jejaka</label>
					  <div class="col-sm-7">
                        <textarea type="text" rows="3" class="form-control" name="ketpria" id="ketpria" placeholder="Jika Pria Terangkan Jejaka"></textarea>
                       </div>
					 </div>
					
					<div class="form-group">
					  <label for="ketwanita" class="col-sm-4 control-label">Jika Wanita Terangkan Perawan</label>
					  <div class="col-sm-7">
                        <textarea type="text" rows="3" class="form-control" name="ketwanita" id="ketwanita" placeholder="Jika Wanita Terangkan Perawan"></textarea>
                       </div>
					 </div>
					
					<div class="form-group">
					  <label for="pasangan_terdahulu" class="col-sm-4 control-label">Nama Istri / Suami terdahulu</label>
					  <div class="col-sm-7">
                        <input type="text" class="form-control" name="pasangan_terdahulu" id="pasangan_terdahulu" placeholder="Nama Istri / Suami terdahulu">
                       </div>
					  </div>
					
					<div class="form-group">
                     <label for="syarat" class="col-sm-4 control-label">IV. Syarat Pengurusan Surat</label>
				    </div>
					 
					 <div class="form-group">
					  <label for="" class="col-sm-4 control-label">1.Fotocopy KTP</label>
					  <div class="col-sm-7">
                        <input type="checkbox" name="cek1" value="1" class="chk-box">
                       </div>
					 </div>
					 
					 <div class="form-group">
					  <label for="" class="col-sm-4 control-label">2.Fotocopy KK</label>
					  <div class="col-sm-7">
                        <input type="checkbox" name="cek2" value="1" class="chk-box">
                       </div>
					 </div>
					 
					 <div class="form-group">
					  <label for="" class="col-sm-4 control-label">3.Bukti Lunas PBB</label>
					  <div class="col-sm-7">
                        <input type="checkbox" name="cek3" value="1" class="chk-box">
                       </div>
					 </div>
					 
					 <div class="form-group">
					  <label for="" class="col-sm-4 control-label">4.Surat Pengantar Kepling</label>
					  <div class="col-sm-7">
                        <input type="checkbox" name="cek4" value="1" class="chk-box">
                       </div>
					 </div>
					
					  
					<div class="form-group">
					<label for="userentry" class="col-sm-4 control-label">User Entry</label>
					  <div class="col-sm-7">
					    <input type="text" class="form-control" name="userentry" id="userentry"value="<?php echo $_SESSION['ses_nama'];?>"readonly>
                       </div>
					  </div>
					
					<div class="form-group">
					<label for="userentry" class="col-sm-4 control-label">Waktu Entry</label>
                      <div class="col-sm-4">
					   <input type="text" class="form-control" name="waktuopt" placeholder="hh:mm:ss" value="<?php echo "$jam_sekarang";?>"readonly>
                      </div>
					</div>
					
					<div class="form-group">
					<label for="level" class="col-sm-4 control-label">Level User Entry</label>
					  <div class="col-sm-7">
                        <input type="text" class="form-control" name="level" id="level" value="<?php echo $_SESSION['ses_level'];?>"readonly>
                       </div>
					</div>
					  
					<div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="cekopt" id="cekopt"value="1">
                       </div>
					  </div>
					  
					  <div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="keterangan" id="keterangan"value="Belum Diverifikasi Lurah">
                       </div>
					  </div>
					  
					   <div class="form-group">
                      <div class="col-sm-5">
                        <input type="hidden" class="form-control" id="bulan" name="bulan" placeholder="bulan" value="<?php echo "$bln_sekarang";?>">
                      </div>
					</div>
					  
					 <div class="form-group">
                      <div class="col-sm-5">
                        <input type="hidden" class="form-control" id="tahun" name="tahun" placeholder="yyyy" value="<?php echo "$_SESSION[thnaktif]";?>">
                      </div>
					</div>
					  
					  <div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="kodekel" id="kodekel" value="<?php echo $_SESSION['ses_kodekel'];?>">
                       </div>
					</div>
					
					<div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="namakel" id="namakel" value="<?php echo $_SESSION['ses_namakel'];?>">
                       </div>
					</div>
					
					<div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="kodekec" id="kodekec" value="<?php echo $_SESSION['ses_kodekec'];?>">
                       </div>
					</div>
					
					<div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="namakec" value="<?php echo $_SESSION['ses_namakec'];?>">
                       </div>
					</div>
					  
					  <?php 	
							$datakel=pg_query($koneksi, "select * from kelurahan where kode='$_SESSION[ses_kodekel]' ");
							$dtkel=pg_fetch_array($datakel);
						?>
					  
					  <div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="lurah" value="<?php echo $dtkel['nama_lurah'];?>">
                       </div>
					</div>
					  
					   <div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="nip" value="<?php echo $dtkel['nip_lurah'];?>">
                       </div>
					</div>
					
					 <div class="form-group">
					  <div class="col-sm-7">
                        <input type="hidden" class="form-control" name="pangkat_gol" value="<?php echo $dtkel['pangkat_gol'];?>">
                       </div>
					</div>
					  
                  </div><!-- /.box-body -->
				</div>
				
	
				<div class="col-md-12">	
                  <div class="box-footer">
                    <a type="submit"  href="appmaster.php?module=srtnikah" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                    <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> simpan</button>
                  </div><!-- /.box-footer -->
                </form>
				</div>
		
		
				
	 <?php
			
  }
}
?>				