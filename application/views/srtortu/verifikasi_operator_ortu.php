<?php foreach($verifikasi_ortu as $n) {} 
   ?>
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Verifikasi_surat/verifikasi_surat_ortu" method="POST" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="id_or" value="<?php echo $n->id_or; ?>" >  
              <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Verifikasi Surat Izin Orang Tua</h4>
                  </div>
				  
                  <div class="card-body">
                  <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>I. DATA DIRI</b></h4>
					   </div>

             <div class="form-group">
                      <label for="" class="bmd-label-floating"> NIK *</label>
                      <input type="text" class="form-control"   required="true" name="nik" value="<?php echo $n->nik; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama" value="<?php echo $n->nama; ?>">
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat" value="<?php echo $n->tempat; ?>" >
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir" value="<?php echo $n->tgllahir; ?>">
                    </div>
                  
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan"  >
					              <?php foreach ($pekerjaan as $pe) { ?>
                          <option <?php if($pe->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $pe->id; ?>"><?php echo $pe->namapekerjaan;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama" >
					 	            <?php foreach ($agama as $a) { ?>
                          <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                        <?php } ?>
					            </select>
                    </div>

            <div class="form-group">
                      <label for="" class="bmd-label-floating">Kecamatan</label>
                      <select type="text" id="select1" class="form-control select"  required="true" name="namakec" style="margin-top:10px">
                        <?php foreach ($kecamatan as $ke) { ?>
                          <option <?php if($ke->id_kec == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $ke->id_kec; ?>"><?php echo $ke->nama_kec;?> </option>
                        <?php } ?>
					            </select>
                  </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Kelurahan</label>
                      <select type="text" id="select2" class="form-control select"  required="true" name="namakel" style="margin-top:10px">
                      <?php foreach ($kelurahan as $kel) { ?>
                          <option <?php if($kel->id_kel == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $kel->id_kel; ?>"><?php echo $kel->nama_kel;?> </option>
                        <?php } ?>
					            </select>
                    </div>
                    
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat" value="<?php echo $n->alamat; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganeraaan *</label>
                      <input type="text" class="form-control"  required="true" name="warga_negara" value="<?php echo $n->warga_negara; ?>">
                    </div>

                    <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>II. DATA CALON</b></h4>
					   </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama_calon" value="<?php echo $n->nama_calon; ?>">
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat_calon" value="<?php echo $n->tempat_calon; ?>">
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir_calon" value="<?php echo $n->tgllahir_calon; ?>">
                    </div>
                  
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan_calon"  >
					              <?php foreach ($pekerjaan as $pe) { ?>
                          <option <?php if($pe->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $pe->id; ?>"><?php echo $pe->namapekerjaan;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama_calon" >
					 	            <?php foreach ($agama as $a) { ?>
                          <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                        <?php } ?>
					            </select>
                    </div>
                    
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat_calon" value="<?php echo $n->alamat_calon; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganeraaan *</label>
                      <input type="text" class="form-control"  required="true" name="bangsa_calon" value="<?php echo $n->bangsa_calon; ?>">
                    </div>



					   <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>III. DATA AYAH</b></h4>
					   </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama_ayah" value="<?php echo $n->nama_ayah; ?>">
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat_ayah" value="<?php echo $n->tempat_ayah; ?>">
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir_ayah" value="<?php echo $n->tgllahir_ayah; ?>">
                    </div>
                  
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan_ayah"  >
					              <?php foreach ($pekerjaan as $pe) { ?>
                          <option <?php if($pe->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $pe->id; ?>"><?php echo $pe->namapekerjaan;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama_ayah" >
					 	            <?php foreach ($agama as $a) { ?>
                          <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                        <?php } ?>
					            </select>
                    </div>
                    
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat_ayah" value="<?php echo $n->alamat_ayah; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganeraaan *</label>
                      <input type="text" class="form-control"  required="true" name="bangsa_ayah" value="<?php echo $n->bangsa_ayah; ?>">
                    </div>


                    <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>IV. DATA IBU</b></h4>
					   </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Ibu *</label>
                      <input type="text" class="form-control"   required="true" name="nama_ibu"value="<?php echo $n->nama_ibu; ?>" >
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir  *</label>
                      <input type="text" class="form-control"  required="true" name="tempat_ibu" value="<?php echo $n->tempat_ibu; ?>">
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir  *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir_ibu" value="<?php echo $n->tgllahir_ibu; ?>">
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganegaraan *</label>
                      <input type="text" class="form-control"  required="true" name="bangsa_ibu" value="<?php echo $n->bangsa_ibu; ?>">
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat Ibu  *</label>
                      <input type="text" class="form-control"  required="true" name="alamat_ibu" value="<?php echo $n->alamat_ibu; ?>">
                    </div>
                    <div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan_ibu"  >
					              <?php foreach ($pekerjaan as $pe) { ?>
                          <option <?php if($pe->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $pe->id; ?>"><?php echo $pe->namapekerjaan;?> </option>
                        <?php } ?>
					            </select>
                    </div>
					          
					          <div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama_ibu" >
					 	            <?php foreach ($agama as $a) { ?>
                          <option <?php if($a->id == "your desired id"){ echo 'selected="selected"'; } ?> value="<?php echo $a->id; ?>"><?php echo $a->agama;?> </option>
                        <?php } ?>
					            </select>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Lampirkan KTP <br>(*  file berupa jpg,png,jpeg)</label>
                        <div class="btn btn-default btn-file" style="width : 500px">
                            <a href="#" class="pop">
                                <img src="<?php echo base_url(); ?>images/surat_ortu/<?php echo $n->foto_ktp; ?>" style="width: 400px; height: 264px;">
                            </a>
                        </div>
                    </div>	

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Lampirkan Kartu Keluarga <br>(*  file berupa jpg,png,jpeg)</label>
                        <div class="btn btn-default btn-file" style="width : 500px">
                        <a href="#" class="pop">
                                <img src="<?php echo base_url(); ?>images/surat_ortu/<?php echo $n->foto_kk; ?>" style="width: 400px; height: 264px;">
                            </a>
                        </div>
                    </div>	

                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                   <?php if ($n->cekopt==1){?>
                  <div class="card-footer text-right" style="">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Verifikasi_surat/srtortu" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                  </div>
                  <?php } else if ($n->cekopt==2){?>
                  <div class="card-footer text-right" style="">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Verifikasi_surat/srtortu" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                  </div>
                  <?php } else{ ?>
                    <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                    </div>
                  </div>
                </div>
              </form>
              <div class="row" style="justify-content:center">
              <form method="POST" action="<?php echo base_url(); ?>Verifikasi_surat/terima_ortu"  enctype="multipart/form-data" style="position: relative;top: -90px;left: 20px;">
                  <input type="hidden" name="id_or" value="<?php echo $n->id_or ?>">
                  <button  name="edit" type="submit" class="btn btn-success"><i class="material-icons">save</i> Terima</button>
              </form>

              <form method="POST" action="<?php echo base_url(); ?>Verifikasi_surat/tolak_ortu"  enctype="multipart/form-data" style="position: relative;top: -90px;left: 20px;">
                  <input type="hidden" name="id_or" value="<?php echo $n->id_or ?>">
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Tolak</button>
              </form>
              </div>
            <?php }?>

            </div>
          </div>
        
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style=" overflow-y: initial !important">
    <div class="modal-content">              
      <div class="modal-body" style=" height: 80vh;overflow-y: auto;">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;">
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
			function readURLUser(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
			function readURLUser1(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser1') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
    $(function () {
     $('input[type="file"]').change(function () {
          if ($(this).val() != "") {
                 $(this).css('color', '#333');
          }else{
                 $(this).css('color', 'transparent');
          }
     });
})
</script>

<script>
    $(function() {
		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});		
});
</script>
