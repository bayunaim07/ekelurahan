<div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form id="RegisterValidation" action="<?php echo base_url() ?>Anak_ortu/simpan_anak" method="POST" enctype="multipart/form-data">			
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Tambah Data Anak</h4>
                  </div>
				  
                  <div class="card-body">
					   <div class="form-group">
					   <br>
						 <h4 class="bmd-label-floating text-success"><b>I. DATA Anak</b></h4>
					   </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Nama Lengkap *</label>
                      <input type="text" class="form-control"   required="true" name="nama" >
                    </div>
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Tempat Lahir *</label>
                      <input type="text" class="form-control"  required="true" name="tempat" >
                    </div>
					<div class="form-group">
                      <label for="" class=""> Tanggal Lahir *</label>
                      <input type="date" class="form-control"  required="true" name="tgllahir">
                    </div>

					<div class="form-group">
                      <label for="" class="bmd-label-floating">Pekerjaan</label>
                      <select type="text" class="form-control select2"  required="true" name="pekerjaan" >
					  <option></option>
					  <?php 
					  
					  foreach ($pekerjaan as $p)
						{  
						?>
					    <option value="<?php echo $p->id; ?>"><?php echo $p->namapekerjaan; ?></option>
						<?php } ?>
					  </select>
                    </div>
					
					<div class="form-group">
                      <label for="" class="bmd-label-floating">Agama</label>
                      <select type="text" class="form-control select2"  required="true" name="agama" >
					 <option></option>
					 <?php 
					  $this->db->select('*');
					  $agama=$this->db->get('mstagama');
					  foreach ($agama->result() as $a)
						{  
						?>
							<option value="<?php echo $a->id ?>"><?php echo $a->agama; ?></option>
						<?php } ?>	
					  </select>
                    </div>
					
					<div class="form-group">
                      <label for="" class="bmd-label-floating"> Alamat *</label>
                      <input type="text" class="form-control"  required="true" name="alamat" >
                    </div>

                    <div class="form-group">
                      <label for="" class="bmd-label-floating"> Kewarganegaraan *</label>
                      <input type="text" class="form-control"  required="true" name="warga_negara" >
                    </div>

                   

				
                    <div class="category form-category text-danger">* Wajib Diisi</div>
                  </div>
                  <div class="card-footer text-right">
                    <div class="form-check mr-auto">
                      <a href="<?php echo base_url(); ?>Surat/srtsengketa" class="btn btn-rose"><i class="material-icons">close</i> Kembali</a>
                    </div>
                    <button name="simpan" type="submit" class="btn btn-success"><i class="material-icons">save</i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$("#select1").change(function() {
  if ($(this).data('options') == undefined) {
    $(this).data('options', $('#select2 option').clone());
  }
  var id = $(this).val();
  var options = $(this).data('options').filter('[data-value=' + id + ']');
  $('#select2').html(options).show();
});
</script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
			function readURLUser(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
			function readURLUser1(input) { // Mulai membaca inputan gambar
			if (input.files && input.files[0]) {
			var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
			reader.onload = function (e) { // Mulai pembacaan file
			$('#preview_gambarUser1') // Tampilkan gambar yang dibaca ke area id #preview_gambar
			.attr('src', e.target.result)
			.width(125); // Menentukan lebar gambar preview (dalam pixel)
			//.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
			};
			 
			reader.readAsDataURL(input.files[0]);
			}
			}
</script>

<script>
    $(function () {
     $('input[type="file"]').change(function () {
          if ($(this).val() != "") {
                 $(this).css('color', '#333');
          }else{
                 $(this).css('color', 'transparent');
          }
     });
})
</script>


