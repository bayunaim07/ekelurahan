<?php
error_reporting(0);
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/barcode128.php";

?>


<style type="text/css">
.judul {
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
	font-size: 22px;
	font-style: normal;
	text-align: center;
}
.judulhed {
	font-family: "Times New Roman", Times, serif;
	font-size: 28px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.jln {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: normal;
}
.nomor {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
}
.isi {
	font-family: "Times New Roman", Times, serif;
	font-size: 16px;
	text-align: justify;
}
.nama {
	font-family: "Times New Roman", Times, serif;
	font-size: 29px;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style2 {font-family: Arial, Helvetica, sans-serif; font-size: 20px; font-style: normal; font-weight: bold; text-align: center; text-decoration: underline; }
body {
	margin-left: 1cm;
}
.style3 {	font-family: Arial, Helvetica, sans-serif;
	font-size: 20pt;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-decoration: underline;
}
.style1 {	font-size: 20pt;
	font-weight: normal;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
}
.tanggal {	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
}
.style4 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.ttd {	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
}
</style>

<?php foreach($cetak as $n) {} 
?>
<table width="97%">
  <tr>
    <td><table width="100%">
       
       
      </table>
      <table width="100%">
        <tr>
          <td class="nomor"><img src="../images/kop_surat/kop.png" width="1125" height="250" /></td>
        </tr>
        <tr>
          <td class="nomor">&nbsp;</td>
        </tr>
        <tr>
          <td class="nomor"><span class="style2"><span class="style3">SURAT KETERANGAN</span> </span></td>
        </tr>
        <tr>
          <td class="nomor"><strong class="style1">Nomor : <?php echo $n->nosurat; ?></strong></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td class="isi">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" colspan="4"><p align="justify" class="style1">Lurah <?php echo $n->nama_kel; ?>, &nbsp; Kecamatan <?php echo $n->nama_kel; ?>, Pemerintah Kota Binjai,  dengan ini menerangkan bahwa :</p></td>
        </tr>
        
        
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td><table width="100%">
            <tr>
              <td width="11%">&nbsp;</td>
              <td width="33%" class="style1">Nama Lengkap </td>
              <td width="2%" class="style1">:</td>
              <td width="54%" class="style1"><?php echo $n->nama; ?></td>
            </tr>
            
			
            <tr>
              <td>&nbsp;</td>
              <td class="style1">Tempat/ Tangal Lahir</td>
              <td class="style1">:</td>
              <td class="style1"><?php echo $n->tempat;?>/
                <?php $n->tgllahir; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="style1">Nomor Induk Kependudukan</td>
              <td class="style1">:</td>
              <td class="style1"><?php echo $n->nik; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="style1">Kewarganegaraan / Agama</td>
              <td class="style1">:</td>
              <td class="style1"><?php echo $n->warga_negara; ?> / <?php echo $n->agama; ?></td>
            </tr>
            
            
            <tr>
              <td>&nbsp;</td>
              <td class="style1">Pekerjaan</td>
              <td class="style1">:</td>
              <td class="style1"><?php echo $n->namapekerjaan; ?></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="style1">Alamat</td>
              <td class="style1">:</td>
              <td class="style1"><?php echo $n->alamat; ?></td>
            </tr>
            

            <tr>
              <td>&nbsp;</td>
              <td class="isi">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            
            
          </table></td>
        </tr>
      </table>
      <table width="100%">
        
        
        <tr>
          <td colspan="2"><p align="justify" class="style1">Sesuai dengan data yang ada pada kami nama tersebut  diatas benar penduduk Kelurahan <?php echo $n->nama_kel; ?> Kecamatan <?php echo $n->nama_kec; ?> Pemerintah  Kota Binjai, dan  sampai saat ini masih berdomisili pada alamat tersebut diatas.</p></td>
        </tr>
        <tr>
          <td width="100%" colspan="2"><div align="justify" class="style1">Surat  Keterangan ini diberikan  kepadanya untuk <?php echo $r['peruntukan']; ?>.</div></td>
        </tr>
        <tr>
          <td colspan="2"><p align="justify" class="style1">Demikian Surat Keterangan ini diperbuat untuk dapat  dipergunakan seperlunya.</p></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        
        <tr>
          <td rowspan="9"><div align="center">
            <div align="center"><img src="../../temp/srtaktanikah.png" /></div></td>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><span class="tanggal">Binjai,
            <?php $n->tglsurat; ?>
          </span></div></td>
        </tr>
        
        <tr>
          <td width="25%" rowspan="7">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div align="right"></div></td>
          <td><div align="center"><span class="style1"><strong class="ttd">LURAH <?php echo $n->nama_kel; ?></strong></span></div></td>
        </tr>
        <tr>
          <td width="4%">&nbsp;</td>
          <td><div align="center"><strong class="ttd">KECAMATAN <?php echo $n->nama_kec; ?></strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td><div align="center" class="ttd"><strong><?php echo $n->nama_lurah; ?></strong></div></td>
        </tr>
        
        <tr>
          <td width="19%">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
          <td><div align="center"><strong class="ttd">NIP. <?php echo $n->nip_lurah; ?></strong></div></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
include "../../phpqrcode/qrlib.php"; 
 
$tempdir = "../../temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);

$bar=$r['noreg'];
$bar1=$r['nama'];
$bar2=$r['nik'];
$isi_teks = "No.Reg : $bar
			 NIK : $bar2
			 Nama : $bar1
			 Surat : Keterangan Akta Pernikahan";
$namafile = "srtaktanikah.png";
$quality = 'H'; 
$ukuran = 2; 
$padding = 0;


// QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);

?>
<script>
   
		window.load = print_d();
		function print_d(){
			window.print();
			
		}
		 
	</script>