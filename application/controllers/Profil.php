<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profil extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_profil_model','pr');
       
        
    }
	public function index()
	{
		$data['users']=$this->pr->data_user();
		$data['agama'] = $this->pr->agama();
		$this->template->load('template','profil/profil',$data);
	}	

	public function simpan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->pr->simpan_profil(); 
		echo "<script>alert('Data Profil Berhasil Di simpan ');
			 window.location.replace('../Profil');
			</script>";	
	  } else {
			 redirect(base_url('Profil'));
		 }
	}
	
	
	public function edit()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->pr->edit();
		echo "<script>alert('Data Profil Berhasil di Update');
			 window.location.replace('../Profil');
			</script>	
			";
	   } else {
		 
			 redirect(base_url('Profil'));
		 
	   }  
		 $this->template->load('template', 'profil/profil', $data);
   }  
}
?>