<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ahli_waris extends CI_Controller
{
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_ahli_waris_model','wa');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
    }

    public function simpan_anak()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->wa->simpan_anak(); 
		echo "<script>alert('Data Anak Berhasil Di simpan ');
			 window.location.replace('../Surat/tambah_surat_waris');
			</script>";	
	  } else {
			 redirect(base_url('Surat/tambah_surat_waris'));
		 }
	}	

	public function tambah()
	{
		
		$data['pekerjaan'] = $this->wa->pekerjaan();
		$data['agama'] = $this->wa->agama(); 
		$this->template->load('template','srtwaris/tambah_anak',$data);	
		
    }

	public function edit_anak()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->wa->update_anak();
		
		echo "<script>alert('Data Anak berhasil di update');
			 window.location.replace('../Surat/tambah_surat_waris');
			</script>";
	   } else {
		 	if(isset($_POST['id_ahli'])){
				$data['ahli'] = $this->wa->ahli_waris();
				$data['pekerjaan'] = $this->wa->pekerjaan();
				$data['agama'] = $this->wa->agama();
				$this->template->load('template', 'srtwaris/ubah', $data);
		 } else {
			 redirect(base_url('Surat/srtwaris'));
		 }
	   }  
   }

    

}

