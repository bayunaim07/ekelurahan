<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Master_surat extends CI_Controller
{
	
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_master_model','ma');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
       
    }

    public function index()
    {
		
		
		$data['surat']=$this->ma->master_surat();
        $this->template->load('template','master_surat/master_surat',$data);
    } 
	
	public function tambah_surat()
	{
		 if (isset($_POST['tambah_surat'])) {
		 
		
		$this->template->load('template','master_surat/tambah_surat');	
		 } else {
			 redirect(base_url('Master_surat'));
		 }
    }
	
	public function simpan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->ma->simpan(); 
		
		
		
		echo "<script>alert('Surat Berhasil Ditambah ');
			 window.location.replace('../Master_surat');
			</script>";	
	  } else {
			 redirect(base_url('Master_surat'));
		 }
	}	
	
	
	public function edit()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ma->update();
		
		echo "<script>alert('Master surat berhasil di update');
			 window.location.replace('../Master_surat');
			</script>

					
			";
	   } else {
		 if(isset($_POST['id'])){
		 $id=$this->input->post('id');
		 
		 $data['edit'] = $this->db->get_where('master_surat', array('id' => $id))->row_Array();
		
		
		 
		 } else {
			 redirect(base_url('master_surat'));
		 }
	   }  
		 $this->template->load('template', 'master_surat/edit_surat', $data);
   }
	
   public function hapus()
   {
	   if(isset($_POST['id'])) 
	   {
		$this->ma->hapus();
		
		
		echo "<script>alert('Data Surat Berhasil Dihapus');
			 window.location.replace('../Master_surat');
			</script>	
			";
	   } else {
		 if(isset($_POST['id_hapus'])){
		 $id=$this->input->post('id_hapus');
		 
		$data['edit'] = $this->db->get_where('master_surat', array('id' => $id))->row_Array();
		
		 
		 } else {
			 redirect(base_url('Master_surat'));
		 }
	   }  
		 $this->template->load('template', 'master_surat/hapus_surat', $data);
   }	   	
   
	
    
    

}

