<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
 function __construct()
    {
        parent::__construct();

        is_login();
        $this->load->helper('form');
        $this->load->library(array('session', 'encryption','form_validation')); 
		$this->load->model('Tbl_home_model','t_home');
    }

    public function index() {
        
        $this->template->load('template', 'home');
    }

}
