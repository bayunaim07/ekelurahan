<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Referensi_lurah extends CI_Controller
{
	
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_referensi_lurah_model','rl');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
       
    }

    public function index()
    {
		$data['lurah']=$this->rl->referensi_lurah();
	
        $this->template->load('template','referensi_lurah/referensi_lurah',$data);
    } 
	
	public function tambah_lurah()
	{
		$data['kecamatan']= $this->rl->kecamatan();
		$data['kelurahan']= $this->rl->kelurahan();
		$this->template->load('template','referensi_lurah/tambah_lurah',$data);	
		
    }
	
	public function simpan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->rl->simpan(); 
		echo "<script>alert('Data Lurah Berhasil Ditambah ');
			 window.location.replace('../Referensi_lurah');
			</script>";	
	  } else {
			 redirect(base_url('Referensi_lurah'));
		 }
	}	
	
	
	public function edit()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->rl->update();
		
		echo "<script>alert('Data Lurah berhasil di update');
			 window.location.replace('../Referensi_lurah');
			</script>
			";
	   } else {
		 if(isset($_POST['id_lurah'])){
		 $id=$this->input->post('id_lurah');
		 $data['edit'] = $this->db->get_where('referensi_lurah', array('id_lurah' => $id))->row_Array();
		 $data['kecamatan']= $this->rl->kecamatan();
		 $data['kelurahan']= $this->rl->kelurahan();
		 $data['join_kec'] = $this->rl->get_join_kec();
         $data['join_kel'] = $this->rl->get_join_kel();
		 
		 
		
		 
		 } 
		

		 else {
			 redirect(base_url('referensi_lurah'));
		 }
		 $this->template->load('template', 'referensi_lurah/edit_lurah', $data);
	   }
		 
   }
	
   public function hapus()
   {
	   if(isset($_POST['id_lurah'])) 
	   {
		$this->rl->hapus();
		
		
		echo "<script>alert('Data Lurah Berhasil Dihapus');
			 window.location.replace('../Referensi_lurah');
			</script>	
			";
	   } else {
		 if(isset($_POST['id_hapus'])){
		 $id=$this->input->post('id_hapus');
		 
		$data['edit'] = $this->db->get_where('referensi_lurah', array('id' => $id))->row_Array();
		
		 
		 } else {
			 redirect(base_url('Referensi_lurah'));
		 }
	   }  
		 $this->template->load('template', 'referensi_lurah/hapus_lurah', $data);
   }	   	
   
	
    
    

}

