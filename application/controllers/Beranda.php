<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	function __construct()
    {
		
        parent::__construct();
    }

	public function index()
	{
		$data['surat_nikah'] = $this->surat_nikah();
		$data['surat_belum_nikah'] = $this->surat_belum_nikah();
		$data['surat_waris'] = $this->surat_waris();
		$data['surat_kematian'] = $this->surat_kematian();
		$data['surat_kurang_mampu'] = $this->surat_kurang_mampu();
		// $data['surat_penghasilan'] = $this->surat_penghasilan();
		// $data['surat_tidak_penghasilan'] = $this->surat_tidak_penghasilan();
		$data['surat_skck'] = $this->skck();
		// $data['surat_akta_nikah'] = $this->akta_nikah();
		// $data['surat_skbd'] = $this->surat_skbd();
		// $data['surat_keterangan'] = $this->surat_keterangan();
		$data['surat_domisili'] = $this->surat_domisili();
		$data['surat_usaha'] = $this->surat_usaha();
		$data['surat_ortu'] = $this->surat_ortu();
		$data['surat_sengketa'] = $this->surat_sengketa();
		$this->load->view('landing',$data);
	}

	function surat_nikah(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtnikah');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	} 

	function surat_belum_nikah(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtblmnikah');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_waris(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtwaris');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_kematian(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtkematian');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_kurang_mampu(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srttdkmampu');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_penghasilan(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtpenghasilan');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_tidak_penghasilan(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srttdkpenghasilan');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function skck(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtskck');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function akta_nikah(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtaktanikah');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_skbd(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtskbd');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_keterangan(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtketerangan');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_domisili(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtdomisili');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_usaha(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtusaha');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_sengketa(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtsengketa');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}

	function surat_ortu(){
		$nik = $this->input->post('nik');
		$this->db->select('*');
        $this->db->from('srtortu');
		$this->db->where('nik',$nik);
        $query = $this->db->get();
        return $query->result();
	}
	
}
