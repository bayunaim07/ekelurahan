<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Arsip extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_Arsip_model','ar');
        $this->load->library('form_validation');        
		$this->load->library('datatables');  
    }

    public function index()
    {
		$data['lurah']=$this->ar->surat();
        $this->template->load('template','arsip/surat/surat',$data);
    } 

    public function srtnikah(){
        $data['lurah'] = $this->ar->nikah();
        $this->template->load('template','arsip/surat_nikah/arsip',$data);
    }

    function surat_nikah(){
        $data=$this->ar->srt_nikah();
		echo json_encode($data);
    }

    public function srtblmnikah(){
        $data['lurah'] = $this->ar->surat_belum_nikah();
        $this->template->load('template','arsip/surat_belum_nikah/arsip',$data);
    }

    function surat_belum_nikah(){
        $data=$this->ar->surat_belum_nikah();
		echo json_encode($data);
    }

    public function srtwaris(){
        $data['lurah'] = $this->ar->waris();
        $this->template->load('template','arsip/surat_waris/arsip',$data);
    }

    function surat_waris(){
        $data=$this->ar->surat_waris();
		echo json_encode($data);
    }

     public function srtkematian(){
        $data['lurah'] = $this->ar->mati();
        $this->template->load('template','arsip/surat_kematian/arsip',$data);
    }

     function surat_kematian(){
        $data=$this->ar->surat_kematian();
		echo json_encode($data);
    }

    public function srttdkmampu(){
        $data['lurah'] = $this->ar->tidak_mampu();
        $this->template->load('template','arsip/surat_tidak_mampu/arsip',$data);
    }

    function surat_tidak_mampu(){
        $data=$this->ar->surat_tidak_mampu();
		echo json_encode($data);
    }

    public function srtpenghasilan(){
        $data['lurah'] = $this->ar->penghasilan();
        $this->template->load('template','arsip/surat_penghasilan/arsip',$data);
    }

    function surat_penghasilan(){
        $data=$this->ar->surat_penghasilan();
		echo json_encode($data);
    }

    public function srttdkpenghasilan(){
        $data['lurah'] = $this->ar->tidak_penghasilan();
        $this->template->load('template','arsip/surat_tidak_penghasilan/arsip',$data);
    }

    function surat_tidak_penghasilan(){
        $data=$this->ar->surat_tidak_penghasilan();
		echo json_encode($data);
    }

    public function srtskck(){
        $data['lurah'] = $this->ar->skck();
        $this->template->load('template','arsip/surat_skck/arsip',$data);
    }

    function surat_skck(){
        $data=$this->ar->surat_skck();
		echo json_encode($data);
    }

    public function srtcerai(){
        $data['lurah'] = $this->ar->cerai();
        $this->template->load('template','arsip/surat_cerai/arsip',$data);
    }

    function surat_cerai(){
        $data=$this->ar->surat_cerai(); 
		echo json_encode($data);
    }

    public function srtaktanikah(){
        $data['lurah'] = $this->ar->akta_nikah();
        $this->template->load('template','arsip/surat_akta_nikah/arsip',$data);
    }

    function surat_akta_nikah(){
        $data=$this->ar->surat_akta_nikah();
		echo json_encode($data);
    }

    public function srtskbd(){
        $data['lurah'] = $this->ar->skbd();
        $this->template->load('template','arsip/surat_skbd/arsip',$data);
    }

    function surat_skbd(){
        $data=$this->ar->surat_skbd();
		echo json_encode($data);
    }

    public function srtketerangan(){
        $data['lurah'] = $this->ar->keterangan();
        $this->template->load('template','arsip/surat_keterangan/arsip',$data);
    }

    function surat_keterangan(){
        $data=$this->ar->surat_keterangan();
		echo json_encode($data);
    }

    public function srtusaha(){
        $data['lurah'] = $this->ar->usaha();
        $this->template->load('template','arsip/surat_usaha/arsip',$data);
    }

    function surat_usaha(){
        $data=$this->ar->surat_usaha();
		echo json_encode($data);
    }

    public function srtsengketa(){
        $data['lurah'] = $this->ar->sengketa();
        $this->template->load('template','arsip/surat_sengketa/arsip',$data);
    }

    function surat_sengketa(){
        $data=$this->ar->surat_sengketa();
		echo json_encode($data);
    }

    public function srtortu(){
        $data['lurah'] = $this->ar->ortu();
        $this->template->load('template','arsip/surat_ortu/arsip',$data);
    }

    function surat_ortu(){
        $data=$this->ar->surat_ortu();
		echo json_encode($data);
    }

    public function srtdomisili(){
        $data['lurah'] = $this->ar->domisili();
        $this->template->load('template','arsip/surat_domisili/arsip',$data);
    }

    function surat_domisili(){
        $data=$this->ar->surat_domisili();
		echo json_encode($data);
    }
}