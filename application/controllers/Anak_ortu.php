<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Anak_ortu extends CI_Controller
{
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_anak_ortu_model','an');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
    }

	

    public function simpan_anak()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->an->simpan_anak(); 
		echo "<script>alert('Data Anak Berhasil Di simpan ');
			 window.location.replace('../Surat/tambah_surat_ortu');
			</script>";	
	  } else {
			 redirect(base_url('Surat/tambah_surat_ortu'));
		 }
	}	

	public function tambah()
	{
		if (isset($_POST['tambah'])) {
		$data['pekerjaan'] = $this->an->pekerjaan();
		$data['agama'] = $this->an->agama(); 
		$this->template->load('template','srtortu/tambah_anak',$data);	
		 } else {
			redirect(base_url('Surat/srtortu/tambah_surat_ortu'));
		 }
    }

	public function edit_anak()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->an->update_anak();
		
		echo "<script>alert('Data Anak berhasil di update');
			 window.location.replace('../Surat/tambah_surat_ortu');
			</script>";
	   } else {
		 	if(isset($_POST['id_anak'])){
				$data['anak'] = $this->an->anak();
				$data['pekerjaan'] = $this->an->pekerjaan();
				$data['agama'] = $this->an->agama();
				$this->template->load('template', 'srtortu/ubah', $data);
		 } else {
			 redirect(base_url('Surat/srtortu'));
		 }
	   }  
   }

    

}

