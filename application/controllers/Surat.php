<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Surat extends CI_Controller
{
	
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_surat_model','su');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
       
    }

	function get_kecamatan()
    {
        $id_kec=$this->input->post('id_kec');
        $data=$this->su->kelurahan($id_kec);
        echo json_encode($data);
    }

    public function index()
    {
		$data['surat']=$this->su->master_surat();
        $this->template->load('template','surat/surat',$data);
    } 
	public function tambah_surat_nikah()
	{
		if (isset($_POST['tambah_surat_nikah'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$data['profil'] = $this->su->profil();
		$this->template->load('template','surat_nikah/tambah_surat_nikah',$data);	
		 } else {
			redirect(base_url('Surat/srtnikah'));
		 }
    }

	public function srtnikah()
	{
		$data['surat']=$this->su->srt_nikah();
        $this->template->load('template','surat_nikah/surat_nikah',$data);
	}	

	public  function srt_nikah()
	{
		$data=$this->su->srt_nikah();
		echo json_encode($data);
	}	

	public function tambah_surat()
	{
		 if (isset($_POST['tambah_surat'])) {
		$this->template->load('template','master_surat/tambah_surat');	
		 } else {
			 redirect(base_url('Master_surat'));
		 }
    }
	public function edit_srtnikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_nikah();
		
		echo "<script>alert('Surat Nikah berhasil di update');
			 window.location.replace('../Surat/srtnikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_sn'])){
				$data['surat_nikah'] = $this->su->srt_nikah();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'surat_nikah/edit_surat', $data);
		 } else {
			 redirect(base_url('Surat/srtnikah'));
		 }
	   }  
   }

	public function simpan_srtnikah()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtnikah(); 
		echo "<script>alert('Surat Nikah Berhasil Di simpan ');
			 window.location.replace('../Surat/srtnikah');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtnikah'));
		 }
	}	

	public function hapus_srtnikah() {
         if(isset($_POST['id_sn']))
        {
            $this->su->hapus_nikah();
            echo "<script>alert('Data Nikah Berhasil Dihapus');
			 window.location.replace('../Surat/srtnikah');
			</script>	
			";
        }
    }

	public function kirim_srtnikah() {
         if(isset($_POST['id_sn']))
        {
            $this->su->kirim_nikah();
            echo "<script>alert('Data Nikah Berhasil Dikirim');
			 window.location.replace('../Surat/srtnikah');
			</script>	
			";
        }
    }

	public function kirim_ulang_srtnikah() {
         if(isset($_POST['id_sn']))
        {
            $this->su->kirim_ulang_nikah();
            echo "<script>alert('Data Nikah Berhasil Dikirim');
			 window.location.replace('../Surat/srtnikah');
			</script>	
			";
        }
    }

   public function srtblmnikah(){
	$data['blm_nikah']=$this->su->srt_blm_menikah();
	$this->template->load('template','srtblmnikah/surat_belum_nikah',$data);
   }

   public function tambah_belum_surat_nikah()
	{
		if (isset($_POST['tambah_belum_surat_nikah'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtblmnikah/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtblmnikah'));
		 }
    }

	public function simpan_srtblmnikah()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtblmnikah(); 
		echo "<script>alert('Surat Belum Nikah Berhasil Di simpan ');
			 window.location.replace('../Surat/srtblmnikah');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtblmnikah'));
		 }
	}

	public  function srt_blm_nikah()
	{
		$data=$this->su->srt_nikah_blm();
		echo json_encode($data);
	}	

	public function edit_srtblmnikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_belum_nikah();
		
		echo "<script>alert('Surat Nikah Belum berhasil di update');
			 window.location.replace('../Surat/srtblmnikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_bn'])){
				$data['surat_belum_nikah'] = $this->su->srt_nikah_blm();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtblmnikah/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtblmnikah'));
		 }
	   }  
   }

   public function hapus_srtblmnikah() {
         if(isset($_POST['id_bn']))
        {
            $this->su->hapus_blm_nikah();
            echo "<script>alert('Data Belum Nikah Berhasil Dihapus');
			 window.location.replace('../Surat/srtblmnikah');
			</script>	
			";
        }
    }

	public function kirim_srtblmnikah() {
         if(isset($_POST['id_bn']))
        {
            $this->su->kirim_blm_nikah();
            echo "<script>alert('Data Belum Nikah Berhasil Dikirim');
			 window.location.replace('../Surat/srtblmnikah');
			</script>	
			";
        }
    }

	public function kirim_ulang_srtblmnikah() {
         if(isset($_POST['id_bn']))
        {
            $this->su->kirim_ulang_blm_nikah();
            echo "<script>alert('Data Belum Nikah Berhasil Dikirim');
			 window.location.replace('../Surat/srtblmnikah');
			</script>	
			";
        }
    }

   public function srtwaris(){
	$data['waris']=$this->su->srt_waris();
	$this->template->load('template','srtwaris/surat_waris',$data);
   }

   public function hapus_ahli($id){
	$this->su->hapus_ahli($id);
	echo "<script>alert('Data Anak Berhasil Di hapus ');
		 window.location.replace('../tambah_surat_waris');
		</script>";	
}

   public function tambah_surat_waris()
	{
		
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$data['ahli'] = $this->su->ahli_waris();
		$this->template->load('template','srtwaris/tambah',$data);	
    }

	public function simpan_srtwaris()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtwaris(); 
		echo "<script>alert('Surat Waris Berhasil Di simpan ');
			 window.location.replace('../Surat/srtwaris');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtwaris'));
		 }
	}

	public  function srt_waris()
	{
		$data=$this->su->surat_waris();
		echo json_encode($data);
	}

	public function edit_waris()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_waris();
		
		echo "<script>alert('Surat Waris berhasil di update');
			 window.location.replace('../Surat/srtwaris');
			</script>";
	   } else {
		 	if(isset($_POST['id_wa'])){
				$data['surat_waris'] = $this->su->surat_waris();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtwaris/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtwaris'));
		 }
	   }  
   }

   public function hapus_waris() {
         if(isset($_POST['id_wa']))
        {
            $this->su->hapus_waris();
            echo "<script>alert('Data Surat Waris Berhasil Dihapus');
			 window.location.replace('../Surat/srtwaris');
			</script>	
			";
        }
    }

	public function kirim_waris() {
         if(isset($_POST['id_wa']))
        {
            $this->su->kirim_waris();
            echo "<script>alert('Data Surat Waris Berhasil Dikirim');
			 window.location.replace('../Surat/srtwaris');
			</script>	
			";
        }
    }

	public function kirim_ulang_waris() {
         if(isset($_POST['id_wa']))
        {
            $this->su->kirim_ulang_waris();
            echo "<script>alert('Data Waris Berhasil Dikirim');
			 window.location.replace('../Surat/srtwaris');
			</script>	
			";
        }
    }

   public function srtkematian(){
	$data['kematian']=$this->su->srt_kematian();
	$this->template->load('template','srtkematian/surat_kematian',$data);
   }

    public function tambah_surat_kematian()
	{
		if (isset($_POST['tambah_surat_kematian'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtkematian/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtkematian'));
		 }
    }

	public function simpan_srtkematian()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtkematian(); 
		echo "<script>alert('Surat Kematian Berhasil Di simpan ');
			 window.location.replace('../Surat/srtkematian');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtkematian'));
		 }
	}

	public  function srt_kematian()
	{
		$data=$this->su->surat_kematian();
		echo json_encode($data);
	}

	public function hapus_kematian() {
         if(isset($_POST['id_ma']))
        {
            $this->su->hapus_kematian();
            echo "<script>alert('Data Surat Kematian Berhasil Dihapus');
			 window.location.replace('../Surat/srtkematian');
			</script>	
			";
        }
    }

	public function kirim_kematian() {
         if(isset($_POST['id_ma']))
        {
            $this->su->kirim_kematian();
            echo "<script>alert('Data Surat kematian Berhasil Dikirim');
			 window.location.replace('../Surat/srtkematian');
			</script>	
			";
        }
    }

	public function edit_kematian()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_kematian();
		
		echo "<script>alert('Surat Kematian berhasil di update');
			 window.location.replace('../Surat/srtkematian');
			</script>";
	   } else {
		 	if(isset($_POST['id_ma'])){
				$data['surat_kematian'] = $this->su->surat_kematian();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtkematian/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtkematian'));
		 }
	   }  
   }

   public function kirim_ulang_kematian() {
         if(isset($_POST['id_ma']))
        {
            $this->su->kirim_ulang_kematian();
            echo "<script>alert('Data Kematian Berhasil Dikirim');
			 window.location.replace('../Surat/srtkematian');
			</script>	
			";
        }
    }

   public function srttdkmampu(){
	$data['tidak_mampu']=$this->su->srt_tidak_mampu();
	$this->template->load('template','srttdkmampu/surat_tidak_mampu',$data);
   }

   public function tambah_surat_tidak_mampu()
	{
		if (isset($_POST['tambah_surat_tidak_mampu'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srttdkmampu/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srttdkmampu'));
		 }
    }

	public function simpan_srttidakmampu()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srttidakmampu(); 
		echo "<script>alert('Surat Tidak Mampu Berhasil Di simpan ');
			 window.location.replace('../Surat/srttdkmampu');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srttdkmampu'));
		 }
	}

	public  function srt_tidak_mampu()
	{
		$data=$this->su->surat_tidak_mampu();
		echo json_encode($data);
	}

	public function edit_tidak_mampu()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_tidak_mampu();
		
		echo "<script>alert('Surat Tidak Mampu berhasil di update');
			 window.location.replace('../Surat/srttdkmampu');
			</script>";
	   } else {
		 	if(isset($_POST['id_tm'])){
				$data['surat_tidak_mampu'] = $this->su->surat_tidak_mampu();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srttdkmampu/edit', $data);
		 } else {
			 redirect(base_url('Surat/srttdkmampu'));
		 }
	   }  
   }

   public function hapus_tidak_mampu() {
         if(isset($_POST['id_tm']))
        {
            $this->su->hapus_tidak_mampu();
            echo "<script>alert('Data Surat Tidak Mampu Berhasil Dihapus');
			 window.location.replace('../Surat/srttdkmampu');
			</script>	
			";
        }
    }

	public function kirim_tidak_mampu() {
         if(isset($_POST['id_tm']))
        {
            $this->su->kirim_tidak_mampu();
            echo "<script>alert('Data Surat Tidak Mempu Berhasil Dikirim');
			 window.location.replace('../Surat/srttdkmampu');
			</script>	
			";
        }
    }

	public function kirim_ulang_tidak_mampu() {
         if(isset($_POST['id_tm']))
        {
            $this->su->kirim_ulang_tidak_mampu();
            echo "<script>alert('Data Tidak Mampu Berhasil Dikirim');
			 window.location.replace('../Surat/srttdkmampu');
			</script>	
			";
        }
    }

   public function srtpenghasilan(){
	   $data['penghasilan']= $this->su->srt_penghasilan();
	   $this->template->load('template','srtpenghasilan/surat_penghasilan',$data);
   }

   public function tambah_surat_penghasilan()
	{
		if (isset($_POST['tambah_surat_penghasilan'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtpenghasilan/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtpenghasilan'));
		 }
    }

	public function simpan_srtpenghasilan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtpenghasilan(); 
		echo "<script>alert('Surat Penghasilan Berhasil Di simpan ');
			 window.location.replace('../Surat/srtpenghasilan');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtpenghasilan'));
		 }
	}

	public  function surat_penghasilan()
	{
		$data=$this->su->surat_penghasilan();
		echo json_encode($data);
	}

	public function hapus_penghasilan() {
         if(isset($_POST['id_pe']))
        {
            $this->su->hapus_penghasilan();
            echo "<script>alert('Data Surat Pengasilan Berhasil Dihapus');
			 window.location.replace('../Surat/srtpenghasilan');
			</script>	
			";
        }
    }

	public function kirim_penghasilan() {
         if(isset($_POST['id_pe']))
        {
            $this->su->kirim_penghasilan();
            echo "<script>alert('Data Surat Penghasilan Berhasil Dikirim');
			 window.location.replace('../Surat/srtpenghasilan');
			</script>	
			";
        }
    }

	public function edit_penghasilan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_penghasilan();
		
		echo "<script>alert('Surat Penghasilan berhasil di update');
			 window.location.replace('../Surat/srtpenghasilan');
			</script>";
	   } else {
		 	if(isset($_POST['id_pe'])){
				$data['surat_penghasilan'] = $this->su->surat_penghasilan();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtpenghasilan/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtpenghasilan'));
		 }
	   }  
   }

   public function kirim_ulang_penghasilan() {
         if(isset($_POST['id_pe']))
        {
            $this->su->kirim_ulang_penghasilan();
            echo "<script>alert('Data Penghasilan Berhasil Dikirim');
			 window.location.replace('../Surat/srtpenghasilan');
			</script>	
			";
        }
    }

   public function srttdkpenghasilan(){
		$data['tidak_penghasilan']= $this->su->srt_tidak_penghasilan();
		$this->template->load('template','srttdkpenghasilan/surat_tidak_berpenghasilan',$data);
	}

	public function tambah_surat_tidak_penghasilan()
	{
		if (isset($_POST['tambah_surat_tidak_penghasilan'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srttdkpenghasilan/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srttdkpenghasilan'));
		 }
    }

	public function simpan_srttdkpenghasilan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srttdkpenghasilan(); 
		echo "<script>alert('Surat Tidak Penghasilan Berhasil Di simpan ');
			 window.location.replace('../Surat/srttdkpenghasilan');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srttdkpenghasilan'));
		 }
	}

	public  function surat_tidak_penghasilan()
	{
		$data=$this->su->surat_tidak_penghasilan();
		echo json_encode($data);
	}

	public function hapus_tidak_penghasilan() {
         if(isset($_POST['id_tp']))
        {
            $this->su->hapus_tidak_penghasilan();
            echo "<script>alert('Data Surat Tidak Pengasilan Berhasil Dihapus');
			 window.location.replace('../Surat/srttdkpenghasilan');
			</script>	
			";
        }
    }

	public function kirim_tidak_penghasilan() {
         if(isset($_POST['id_tp']))
        {
            $this->su->kirim_tidak_penghasilan();
            echo "<script>alert('Data Surat Tidak Penghasilan Berhasil Dikirim');
			 window.location.replace('../Surat/srttdkpenghasilan');
			</script>	
			";
        }
    }

	public function edit_tidak_penghasilan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_tidak_penghasilan();
		
		echo "<script>alert('Surat Tidak Penghasilan berhasil di update');
			 window.location.replace('../Surat/srttdkpenghasilan');
			</script>";
	   } else {
		 	if(isset($_POST['id_tp'])){
				$data['surat_tidak_penghasilan'] = $this->su->surat_tidak_penghasilan();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srttdkpenghasilan/edit', $data);
		 } else {
			 redirect(base_url('Surat/srttdkpenghasilan'));
		 }
	   }  
   }

   public function kirim_ulang_tidak_penghasilan() {
         if(isset($_POST['id_tp']))
        {
            $this->su->kirim_ulang_tidak_penghasilan();
            echo "<script>alert('Data Tidak Penghasilan Berhasil Dikirim');
			 window.location.replace('../Surat/srttdkpenghasilan');
			</script>	
			";
        }
    }

	public function srtskck(){
		$data['skck']= $this->su->srt_skck();
		$this->template->load('template','srtskck/surat_skck',$data);
	}

	public function tambah_skck()
	{
		if (isset($_POST['tambah_skck'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtskck/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtskck'));
		 }
    }

	public function simpan_srtskck()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtskck(); 
		echo "<script>alert('Surat SKCK Berhasil Di simpan ');
			 window.location.replace('../Surat/srtskck');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtskck'));
		 }
	}

	public  function surat_skck()
	{
		$data=$this->su->surat_skck();
		echo json_encode($data);
	}

	public function hapus_skck() {
         if(isset($_POST['id_sk']))
        {
            $this->su->hapus_skck();
            echo "<script>alert('Data Surat Skck Berhasil Dihapus');
			 window.location.replace('../Surat/srtskck');
			</script>	
			";
        }
    }

	public function kirim_skck() {
         if(isset($_POST['id_sk']))
        {
            $this->su->kirim_skck();
            echo "<script>alert('Data Surat SKCK Berhasil Dikirim');
			 window.location.replace('../Surat/srtskck');
			</script>	
			";
        }
    }

	public function edit_skck()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_skck();
		
		echo "<script>alert('Surat SKCK berhasil di update');
			 window.location.replace('../Surat/srtskck');
			</script>";
	   } else {
		 	if(isset($_POST['id_sk'])){
				$data['surat_skck'] = $this->su->surat_skck();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtskck/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtskck'));
		 }
	   }  
   }

   public function kirim_ulang_skck() {
         if(isset($_POST['id_sk']))
        {
            $this->su->kirim_ulang_skck();
            echo "<script>alert('Data SKCK Berhasil Dikirim');
			 window.location.replace('../Surat/srtskck');
			</script>	
			";
        }
    }

	public function srtcerai(){
		$data['cerai']= $this->su->srt_cerai();
		$this->template->load('template','srtcerai/surat_cerai',$data);
	}

	public function tambah_surat_cerai()
	{
		if (isset($_POST['tambah_surat_cerai'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtcerai/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtcerai'));
		 }
    }

	public function simpan_cerai()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtcerai(); 
		echo "<script>alert('Surat Cerai Berhasil Di simpan ');
			 window.location.replace('../Surat/srtcerai');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtcerai'));
		 }
	}

	public  function surat_cerai()
	{
		$data=$this->su->surat_cerai();
		echo json_encode($data);
	}

	public function hapus_cerai() {
         if(isset($_POST['id_ce']))
        {
            $this->su->hapus_cerai();
            echo "<script>alert('Data Surat Cerai Berhasil Dihapus');
			 window.location.replace('../Surat/srtcerai');
			</script>	
			";
        }
    }

	public function kirim_cerai() {
         if(isset($_POST['id_ce']))
        {
            $this->su->kirim_cerai();
            echo "<script>alert('Data Surat Cerai Berhasil Dikirim');
			 window.location.replace('../Surat/srtcerai');
			</script>	
			";
        }
    }

	public function edit_cerai()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_cerai();
		
		echo "<script>alert('Surat Cerai berhasil di update');
			 window.location.replace('../Surat/srtcerai');
			</script>";
	   } else {
		 	if(isset($_POST['id_ce'])){
				$data['surat_cerai'] = $this->su->surat_cerai();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtcerai/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtcerai'));
		 }
	   }  
   }

   public function kirim_ulang_cerai() {
         if(isset($_POST['id_ce']))
        {
            $this->su->kirim_ulang_cerai();
            echo "<script>alert('Data Cerai Berhasil Dikirim');
			 window.location.replace('../Surat/srtcerai');
			</script>	
			";
        }
    }

	public function srtaktanikah(){
		$data['akta_nikah']= $this->su->srt_akta_nikah();
		$this->template->load('template','srtaktanikah/srtaktanikah',$data);
	}

	public function tambah_surat_akta_nikah()
	{
		if (isset($_POST['tambah_surat_akta_nikah'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtaktanikah/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtaktanikah'));
		 }
    }

	public function simpan_akta_nikah()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_srtaktanikah(); 
		echo "<script>alert('Surat Akta Nikah Berhasil Di simpan ');
			 window.location.replace('../Surat/srtaktanikah');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtaktanikah'));
		 }
	}

	public  function surat_akta_nikah()
	{
		$data=$this->su->surat_akta_nikah();
		echo json_encode($data);
	}

	public function hapus_akta_nikah() {
         if(isset($_POST['id_an']))
        {
            $this->su->hapus_akta_nikah();
            echo "<script>alert('Data Surat Akta Nikah Berhasil Dihapus');
			 window.location.replace('../Surat/srtaktanikah');
			</script>	
			";
        }
    }

	public function kirim_akta_nikah() {
         if(isset($_POST['id_an']))
        {
            $this->su->kirim_akta_nikah();
            echo "<script>alert('Data Surat Akta Nikah Berhasil Dikirim');
			 window.location.replace('../Surat/srtaktanikah');
			</script>	
			";
        }
    }

	public function edit_akta_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_akta_nikah();
		
		echo "<script>alert('Surat Akta Nikah berhasil di update');
			 window.location.replace('../Surat/srtaktanikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_an'])){
				$data['surat_akta_nikah'] = $this->su->surat_akta_nikah();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtaktanikah/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtaktanikah'));
		 }
	   }  
   }

   public function kirim_ulang_akta_nikah() {
         if(isset($_POST['id_an']))
        {
            $this->su->kirim_ulang_akta_nikah();
            echo "<script>alert('Data Akta Nikah Berhasil Dikirim');
			 window.location.replace('../Surat/srtaktanikah');
			</script>	
			";
        }
    }

	public function srtskbd(){
		$data['skbd']= $this->su->srt_skbd();
		$this->template->load('template','srtskbd/surat_bersih_diri',$data);
	}

	public function tambah_surat_bersih_diri()
	{
		if (isset($_POST['tambah_surat_bersih_diri'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtskbd/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtskbd'));
		 }
    }

	public function simpan_skbd()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_skbd(); 
		echo "<script>alert('Surat Keterangan Bersih Diri Berhasil Di simpan ');
			 window.location.replace('../Surat/srtskbd');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtskbd'));
		 }
	}

	public  function surat_keterangan_bersih_diri()
	{
		$data=$this->su->surat_keterangan_bersih_diri();
		echo json_encode($data);
	}

	public function hapus_bersih_diri() {
         if(isset($_POST['id_skbd']))
        {
            $this->su->hapus_skbd();
            echo "<script>alert('Data Surat Keterangan Bersih Diri Berhasil Dihapus');
			 window.location.replace('../Surat/srtskbd');
			</script>	
			";
        }
    }

	public function kirim_bersih_diri() {
         if(isset($_POST['id_skbd']))
        {
            $this->su->kirim_bersih_diri();
            echo "<script>alert('Data Surat Keterangan Bersih Diri Berhasil Dikirim');
			 window.location.replace('../Surat/srtskbd');
			</script>	
			";
        }
    }

	public function edit_skbd()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_keterangan_bersih_diri();
		
		echo "<script>alert('Surat Keterangan Bersih Diri berhasil di update');
			 window.location.replace('../Surat/srtskbd');
			</script>";
	   } else {
		 	if(isset($_POST['id_skbd'])){
				$data['skbd'] = $this->su->surat_keterangan_bersih_diri();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtskbd/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtskbd'));
		 }
	   }  
   }

   public function kirim_ulang_bersih_diri() {
         if(isset($_POST['id_skbd']))
        {
            $this->su->kirim_ulang_bersih_diri();
            echo "<script>alert('Data Bersih Diri Berhasil Dikirim');
			 window.location.replace('../Surat/srtskbd');
			</script>	
			";
        }
    }

	public function srtketerangan(){
		$data['keterangan']= $this->su->srt_keterangan();
		$this->template->load('template','srtketerangan/surat_keterangan',$data);
	}

	public function tambah_surat_keterangan()
	{
		if (isset($_POST['tambah_surat_keterangan'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtketerangan/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtketerangan'));
		 }
    }

	public function simpan_surat_keterangan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_keterangan(); 
		echo "<script>alert('Surat Keterangan Berhasil Di simpan ');
			 window.location.replace('../Surat/srtketerangan');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtketerangan'));
		 }
	}

	public  function surat_keterangan()
	{
		$data=$this->su->surat_keterangan();
		echo json_encode($data);
	}

	public function hapus_surat_keterangan() {
         if(isset($_POST['id_ke']))
        {
            $this->su->hapus_keterangan();
            echo "<script>alert('Data Surat Keterangan Berhasil Dihapus');
			 window.location.replace('../Surat/srtketerangan');
			</script>	
			";
        }
    }

	public function kirim_surat_keterangan() {
         if(isset($_POST['id_ke']))
        {
            $this->su->kirim_keterangan();
            echo "<script>alert('Data Surat Keterangan Berhasil Dikirim');
			 window.location.replace('../Surat/srtketerangan');
			</script>	
			";
        }
    }

	public function edit_surat_keterangan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_keterangan();
		
		echo "<script>alert('Surat Keterangan berhasil di update');
			 window.location.replace('../Surat/srtketerangan');
			</script>";
	   } else {
		 	if(isset($_POST['id_ke'])){
				$data['keterangan'] = $this->su->surat_keterangan();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtketerangan/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtketerangan'));
		 }
	   }  
   }

   public function kirim_ulang_surat_keterangan() {
         if(isset($_POST['id_ke']))
        {
            $this->su->kirim_ulang_keterangan();
            echo "<script>alert('Data Keterangan Berhasil Dikirim');
			 window.location.replace('../Surat/srtketerangan');
			</script>	
			";
        }
    }

	public function srtusaha(){
		$data['usaha']= $this->su->srt_usaha();
		$this->template->load('template','srtusaha/surat_usaha',$data);
	}

	public function tambah_surat_usaha()
	{
		if (isset($_POST['tambah_surat_usaha'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtusaha/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtusaha'));
		 }
    }

	public function simpan_surat_usaha()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_usaha(); 
		echo "<script>alert('Surat Usaha Berhasil Di simpan ');
			 window.location.replace('../Surat/srtusaha');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtusaha'));
		 }
	}

	public  function surat_usaha()
	{
		$data=$this->su->surat_usaha();
		echo json_encode($data);
	}

	public function hapus_surat_usaha() {
         if(isset($_POST['id_us']))
        {
            $this->su->hapus_usaha();
            echo "<script>alert('Data Surat Usaha Berhasil Dihapus');
			 window.location.replace('../Surat/srtusaha');
			</script>	
			";
        }
    }

	public function kirim_surat_usaha() {
         if(isset($_POST['id_us']))
        {
            $this->su->kirim_usaha();
            echo "<script>alert('Data Surat Usaha Berhasil Dikirim');
			 window.location.replace('../Surat/srtusaha');
			</script>	
			";
        }
    }

	public function edit_surat_usaha()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_usaha();
		
		echo "<script>alert('Surat Usaha berhasil di update');
			 window.location.replace('../Surat/srtusaha');
			</script>";
	   } else {
		 	if(isset($_POST['id_us'])){
				$data['usaha'] = $this->su->surat_usaha();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtusaha/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtusaha'));
		 }
	   }  
   }

   public function kirim_ulang_surat_usaha() {
         if(isset($_POST['id_us']))
        {
            $this->su->kirim_ulang_usaha();
            echo "<script>alert('Data Usaha Berhasil Dikirim');
			 window.location.replace('../Surat/srtusaha');
			</script>	
			";
        }
    }

	public function srtortu(){
		$data['ortu']= $this->su->srt_ortu();
		$this->template->load('template','srtortu/surat_ortu',$data);
	}

	public function hapus_anak($id){
		$this->su->hapus_anak($id);
		echo "<script>alert('Data Anak Berhasil Di hapus ');
			 window.location.replace('../tambah_surat_ortu');
			</script>";	
	}

	public function tambah_surat_ortu()
	{
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtortu/tambah',$data);	
    }

	public function simpan_surat_ortu()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_ortu(); 
		echo "<script>alert('Surat Ortu Berhasil Di simpan ');
			 window.location.replace('../Surat/srtortu');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtortu'));
		 }
	}

	public  function surat_ortu()
	{
		$data=$this->su->surat_ortu();
		echo json_encode($data);
	}

	public function hapus_surat_ortu() {
         if(isset($_POST['id_or']))
        {
            $this->su->hapus_ortu();
            echo "<script>alert('Data Surat Ortu Berhasil Dihapus');
			 window.location.replace('../Surat/srtortu');
			</script>	
			";
        }
    }

	public function kirim_surat_ortu() {
         if(isset($_POST['id_or']))
        {
            $this->su->kirim_ortu();
            echo "<script>alert('Data Surat Ortu Berhasil Dikirim');
			 window.location.replace('../Surat/srtortu');
			</script>	
			";
        }
    }

	public function edit_surat_ortu()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_ortu();
		
		echo "<script>alert('Surat Ortu berhasil di update');
			 window.location.replace('../Surat/srtortu');
			</script>";
	   } else {
		 	if(isset($_POST['id_or'])){
				$data['ortu'] = $this->su->surat_ortu();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtortu/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtortu'));
		 }
	   }  
   }

   public function kirim_ulang_surat_ortu() {
         if(isset($_POST['id_or']))
        {
            $this->su->kirim_ulang_ortu();
            echo "<script>alert('Data Ortu Berhasil Dikirim');
			 window.location.replace('../Surat/srtortu');
			</script>	
			";
        }
    }

	public function srtsengketa(){
		$data['sengketa']= $this->su->srt_sengketa();
		$this->template->load('template','srtsengketa/surat_sengketa',$data);
	}

	public function tambah_surat_sengketa()
	{
		if (isset($_POST['tambah_surat_sengketa'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtsengketa/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtsengketa'));
		 }
    }

	public function simpan_surat_sengketa()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_sengketa(); 
		echo "<script>alert('Surat Sengketa Berhasil Di simpan ');
			 window.location.replace('../Surat/srtsengketa');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtsengketa'));
		 }
	}

	public  function surat_sengketa()
	{
		$data=$this->su->surat_sengketa();
		echo json_encode($data);
	}

	public function hapus_surat_sengketa() {
         if(isset($_POST['id_se']))
        {
            $this->su->hapus_sengketa();
            echo "<script>alert('Data Surat Sengketa Berhasil Dihapus');
			 window.location.replace('../Surat/srtsengketa');
			</script>	
			";
        }
    }

	public function kirim_surat_sengketa() {
         if(isset($_POST['id_se']))
        {
            $this->su->kirim_sengketa();
            echo "<script>alert('Data Surat Sengketa Berhasil Dikirim');
			 window.location.replace('../Surat/srtsengketa');
			</script>	
			";
        }
    }

	public function edit_surat_sengketa()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_sengketa();
		
		echo "<script>alert('Surat Sengketa berhasil di update');
			 window.location.replace('../Surat/srtsengketa');
			</script>";
	   } else {
		 	if(isset($_POST['id_se'])){
				$data['sengketa'] = $this->su->surat_sengketa();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtsengketa/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtsengeketa'));
		 }
	   }  
   }

   public function kirim_ulang_surat_sengketa() {
         if(isset($_POST['id_se']))
        {
            $this->su->kirim_ulang_sengketa();
            echo "<script>alert('Data Sengketa Berhasil Dikirim');
			 window.location.replace('../Surat/srtsengketa');
			</script>	
			";
        }
    }

	public function srtdomisili(){
		$data['domisili']= $this->su->srt_domisili();
		$this->template->load('template','srtdomisili/surat_domisili',$data);
	}

	public function tambah_surat_domisili()
	{
		if (isset($_POST['tambah_surat_domisili'])) {
		$data['pekerjaan'] = $this->su->pekerjaan();	 
		$data['kode'] = $this->su->buat_kode();	 
		$data['kecamatan'] = $this->su->kecamatan();
		$data['kelurahan'] = $this->su->get_kelurahan();
		$this->template->load('template','srtdomisili/tambah',$data);	
		 } else {
			redirect(base_url('Surat/srtdomisili'));
		 }
    }

	public function simpan_surat_domisili()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->su->simpan_domisili(); 
		echo "<script>alert('Surat Domisili Berhasil Di simpan ');
			 window.location.replace('../Surat/srtdomisili');
			</script>";	
	  } else {
			 redirect(base_url('Surat/srtdomisili'));
		 }
	}

	public  function surat_domisili()
	{
		$data=$this->su->surat_domisili();
		echo json_encode($data);
	}

	public function hapus_surat_domisili() {
         if(isset($_POST['id_do']))
        {
            $this->su->hapus_domisili();
            echo "<script>alert('Data Surat Domisili Berhasil Dihapus');
			 window.location.replace('../Surat/srtdomisili');
			</script>	
			";
        }
    }

	public function kirim_surat_domisili() {
         if(isset($_POST['id_do']))
        {
            $this->su->kirim_domisili();
            echo "<script>alert('Data Surat Domisili Berhasil Dikirim');
			 window.location.replace('../Surat/srtdomisili');
			</script>	
			";
        }
    }

	public function edit_surat_domisili()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->su->update_surat_domisili();
		
		echo "<script>alert('Surat Domisili berhasil di update');
			 window.location.replace('../Surat/srtdomisili');
			</script>";
	   } else {
		 	if(isset($_POST['id_do'])){
				$data['domisili'] = $this->su->surat_domisili();
				$data['kecamatan'] = $this->su->kecamatan();
				$data['kelurahan'] = $this->su->get_kelurahan();
				$data['pekerjaan'] = $this->su->pekerjaan();
				$data['status'] = $this->su->status_perkawinan();
				$data['agama'] = $this->su->agama();
				$this->template->load('template', 'srtdomisili/edit', $data);
		 } else {
			 redirect(base_url('Surat/srtdomisili'));
		 }
	   }  
   }

   public function kirim_ulang_surat_domisili() {
         if(isset($_POST['id_do']))
        {
            $this->su->kirim_ulang_domisili();
            echo "<script>alert('Data Domisili Berhasil Dikirim');
			 window.location.replace('../Surat/srtdomisili');
			</script>	
			";
        }
    }

   public function simpan_foto_kk()
   {
        $config['upload_path']="images/surat_nikah";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp()
   {
        $config['upload_path']="images/surat_nikah";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_belum_nikah()
   {
        $config['upload_path']="images/surat_blm_nikah";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_belum_nikah($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_belum_nikah()
   {
        $config['upload_path']="images/surat_blm_nikah";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_belum_nikah($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_waris()
   {
        $config['upload_path']="images/surat_waris";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_waris($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_waris()
   {
        $config['upload_path']="images/surat_waris";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_waris($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_kematian()
   {
        $config['upload_path']="images/surat_kematian";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_kematian($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_kematian()
   {
        $config['upload_path']="images/surat_kematian";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_kematian($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_tidak_mampu()
   {
        $config['upload_path']="images/surat_tidak_mampu";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_tidak_mampu($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_tidak_mampu()
   {
        $config['upload_path']="images/surat_tidak_mampu";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_tidak_mampu($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_penghasilan()
   {
        $config['upload_path']="images/surat_penghasilan";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_penghasilan($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_penghasilan()
   {
        $config['upload_path']="images/surat_penghasilan";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_penghasilan($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_tidak_penghasilan()
   {
        $config['upload_path']="images/surat_tidak_penghasilan";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_tidak_penghasilan($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_tidak_penghasilan()
   {
        $config['upload_path']="images/surat_tidak_penghasilan";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_tidak_penghasilan($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_skck()
   {
        $config['upload_path']="images/surat_skck";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_skck($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_skck()
   {
        $config['upload_path']="images/surat_skck";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_skck($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_cerai()
   {
        $config['upload_path']="images/surat_cerai";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_cerai($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_cerai()
   {
        $config['upload_path']="images/surat_cerai";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_cerai($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_akta_nikah()
   {
        $config['upload_path']="images/surat_akta_nikah";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_akta_nikah($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_akta_nikah()
   {
        $config['upload_path']="images/surat_akta_nikah";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_akta_nikah($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_skbd()
   {
        $config['upload_path']="images/surat_skbd";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_skbd($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_skbd()
   {
        $config['upload_path']="images/surat_skbd";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_skbd($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_keterangan()
   {
        $config['upload_path']="images/surat_keterangan";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_keterangan($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_keterangan()
   {
        $config['upload_path']="images/surat_keterangan";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_keterangan($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_usaha()
   {
        $config['upload_path']="images/surat_usaha";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_usaha($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_usaha()
   {
        $config['upload_path']="images/surat_usaha";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_usaha($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_sengketa()
   {
        $config['upload_path']="images/surat_sengketa";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_sengketa($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_sengketa()
   {
        $config['upload_path']="images/surat_sengketa";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_sengketa($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_ortu()
   {
        $config['upload_path']="images/surat_ortu";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_ortu($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_ortu()
   {
        $config['upload_path']="images/surat_ortu";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_ortu($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_kk_domisili()
   {
        $config['upload_path']="images/surat_domisili";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file2")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_kk_domisili($id_users,$image);
            echo json_decode($result);
        }
    }

	public function simpan_foto_ktp_domisili()
   {
        $config['upload_path']="images/surat_domisili";
        $config['allowed_types']='jpg|png|jpeg';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());

            $image= $data['upload_data']['file_name'];

            $id_users=$this->session->userdata('id_users');
            $result= $this->su->simpan_foto_ktp_domisili($id_users,$image);
            echo json_decode($result);
        }
    }
	
}

