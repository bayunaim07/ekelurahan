<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Verifikasi_surat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_verifikasi_operator','ve');
        $this->load->library('form_validation');        
		$this->load->library('datatables');  
    }

    public function index()
    {
		$data['surat']=$this->ve->surat();
        $this->template->load('template','operator/surat/surat',$data);
    } 

    function srtnikah(){
        $data['nikah'] = $this->ve->nikah();
        $this->template->load('template','surat_nikah/verifikasi_operator_nikah',$data);
    }

    function surat_nikah(){
        $data=$this->ve->srt_nikah();
		echo json_encode($data);
    }

    public function verifikasi_surat_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_nikah();
		
		echo "<script>alert('Surat Nikah berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtnikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_sn'])){
				$data['verifikasi_nikah'] = $this->ve->srt_nikah();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'surat_nikah/verifikasi_surat_nikah', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtnikah'));
		 }
	   }  
   }

   public function terima_nikah(){
    if (isset($_POST['id_sn'])) {
        $data['verifikasi_nikah'] = $this->ve->srt_nikah();
	    $this->template->load('template', 'surat_nikah/terima_verifikasi_nikah',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtnikah'));
		 }
   }

   function terima_surat_nikah(){
       if (isset($_POST['id_sn'])) { 
	    $this->ve->terima_surat_nikah(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtnikah');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtnikah'));
		 }
   }

   public function tolak_nikah(){
    if (isset($_POST['id_sn'])) {
        $data['verifikasi_nikah'] = $this->ve->srt_nikah();
	    $this->template->load('template', 'surat_nikah/tolak_verifikasi_nikah',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtnikah'));
		 }
   }

   function tolak_surat_nikah(){
       if (isset($_POST['id_sn'])) { 
	    $this->ve->tolak_surat_nikah(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtnikah');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtnikah'));
		 }
   }

   function cetak_surat_nikah(){
        if(isset($_POST['id_sn'])){
            $data['cetak'] = $this->ve->srt_nikah();
            $data['ayah'] = $this->ve->ayah();
            $data['ibu'] = $this->ve->ibu();
            $data['lurah'] = $this->ve->lurah();
            $data['nomor'] = $this->ve->nomor_surat();
            $this->load->view('rpt_sksrtnikahn1',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtnikah'));
        }
   }

   function selesai_surat_nikah(){
       if(isset($_POST['id_sn'])){
        $this->ve->selesai_surat_nikah(); 
        echo "<script>alert('Surat Nikah Sudah Selesai');
			 window.location.replace('srtnikah');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtnikah'));
        }
   }

    function srtblmnikah(){
        $data['belum_nikah'] = $this->ve->belum_menikah();
        $this->template->load('template','srtblmnikah/verifikasi_blm_nikah',$data);
    }

    function surat_belum_nikah(){
        $data=$this->ve->surat_belum_nikah();
		echo json_encode($data);
    }

     public function verifikasi_surat_belum_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_belum_nikah();
		
		echo "<script>alert('Surat Belum Nikah berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtblmnikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_bn'])){
				$data['verifikasi_belum_nikah'] = $this->ve->surat_belum_nikah();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtblmnikah/verifikasi_operator_belum_nikah', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtblmnikah'));
		 }
	   }  
   }

   public function tolak_belum_nikah(){
    if (isset($_POST['id_bn'])) {
        $data['verifikasi_belum_nikah'] = $this->ve->surat_belum_nikah();
	    $this->template->load('template', 'srtblmnikah/tolak_verifikasi_belum_nikah',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtblmnikah'));
		 }
   }

   function tolak_surat_belum_nikah(){
       if (isset($_POST['id_bn'])) { 
	    $this->ve->tolak_surat_belum_nikah(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtblmnikah');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtblmnikah'));
		 }
   }

   public function terima_belum_nikah(){
    if (isset($_POST['id_bn'])) {
        $data['verifikasi_belum_nikah'] = $this->ve->surat_belum_nikah();
	    $this->template->load('template', 'srtblmnikah/terima_verifikasi_belum_nikah',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtblmnikah'));
		 }
   }

   function terima_surat_belum_nikah(){
       if (isset($_POST['id_bn'])) { 
	    $this->ve->terima_surat_belum_nikah(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtblmnikah');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtblmnikah'));
		 }
   }

   function selesai_surat_belum_nikah(){
       if(isset($_POST['id_bn'])){
        $this->ve->selesai_surat_belum_nikah(); 
        echo "<script>alert('Surat Belum Nikah Sudah Selesai');
			 window.location.replace('srtblmnikah');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtblmnikah'));
        }
   }

   function cetak_surat_belum_nikah(){
        if(isset($_POST['id_bn'])){
            $data['cetak'] = $this->ve->cetak_surat_belum_nikah();
            $this->load->view('rpt_sksrtblmnikah',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtblmnikah'));
        }
   }

    function srtwaris(){
        $data['waris'] = $this->ve->waris();
        $this->template->load('template','srtwaris/verifikasi_waris',$data);
    }

    function surat_waris(){
        $data=$this->ve->surat_waris();
		echo json_encode($data);
    }

     public function verifikasi_surat_waris()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_waris();
		
		echo "<script>alert('Surat Waris berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtwaris');
			</script>";
	   } else {
		 	if(isset($_POST['id_wa'])){
				$data['verifikasi_waris'] = $this->ve->surat_waris();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtwaris/verifikasi_operator_waris', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtwaris'));
		 }
	   }  
   }

   public function tolak_waris(){
    if (isset($_POST['id_wa'])) {
        $data['verifikasi_waris'] = $this->ve->surat_waris();
	    $this->template->load('template', 'srtwaris/tolak_verifikasi_waris',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtwaris'));
		 }
   }

   function tolak_surat_waris(){
       if (isset($_POST['id_wa'])) { 
	    $this->ve->tolak_surat_waris(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtwaris');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtwaris'));
		 }
   }

   public function terima_waris(){
    if (isset($_POST['id_wa'])) {
        $data['verifikasi_waris'] = $this->ve->surat_waris();
	    $this->template->load('template', 'srtwaris/terima_verifikasi_waris',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtwaris'));
		 }
   }

   function terima_surat_waris(){
       if (isset($_POST['id_wa'])) { 
	    $this->ve->terima_surat_waris(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtwaris');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtwaris'));
		 }
   }

   function selesai_surat_waris(){
       if(isset($_POST['id_wa'])){
        $this->ve->selesai_surat_waris(); 
        echo "<script>alert('Surat Waris Sudah Selesai');
			 window.location.replace('srtwaris');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtwaris'));
        }
   }

   function cetak_surat_waris(){
        if(isset($_POST['id_wa'])){
            $data['cetak'] = $this->ve->cetak_surat_waris();
            $this->load->view('rpt_sksrtwaris',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtwaris'));
        }
   }

    function srtkematian(){
        $data['mati'] = $this->ve->mati();
        $this->template->load('template','srtkematian/verifikasi_kematian',$data);
    }

    function surat_kematian(){
        $data=$this->ve->surat_kematian();
		echo json_encode($data);
    }

     public function verifikasi_surat_kematian()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_kematian();
		
		echo "<script>alert('Surat Kematian berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtkematian');
			</script>";
	   } else {
		 	if(isset($_POST['id_ma'])){
				$data['verifikasi_kematian'] = $this->ve->surat_kematian();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtkematian/verifikasi_operator_mati', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtkematian'));
		 }
	   }  
   }

   public function tolak_kematian(){
    if (isset($_POST['id_ma'])) {
        $data['verifikasi_kematian'] = $this->ve->surat_kematian();
	    $this->template->load('template', 'srtkematian/tolak_verifikasi_kematian',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtkematian'));
		 }
   }

   function tolak_surat_kematian(){
       if (isset($_POST['id_ma'])) { 
	    $this->ve->tolak_surat_kematian(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtkematian');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtkematian'));
		 }
   }

   public function terima_kematian(){
    if (isset($_POST['id_ma'])) {
        $data['verifikasi_kematian'] = $this->ve->surat_kematian();
	    $this->template->load('template', 'srtkematian/terima_verifikasi_kematian',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtkematian'));
		 }
   }

   function terima_surat_kematian(){
       if (isset($_POST['id_ma'])) { 
	    $this->ve->terima_surat_kematian(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtkematian');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtkematian'));
		 }
   }

   function selesai_surat_kematian(){
       if(isset($_POST['id_ma'])){
        $this->ve->selesai_surat_kematian(); 
        echo "<script>alert('Surat Kematian Sudah Selesai');
			 window.location.replace('srtkematian');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtkematian'));
        }
   }

   function cetak_surat_kematian(){
        if(isset($_POST['id_ma'])){
            $data['cetak'] = $this->ve->cetak_surat_kematian();
            $this->load->view('rpt_sksrtkematian',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtkematian'));
        }
   }

    function srttdkmampu(){
        $data['tidak_mampu'] = $this->ve->tidak_mampu();
        $this->template->load('template','srttdkmampu/verifikasi_tidak_mampu',$data);
    }

    function surat_tidak_mampu(){
        $data=$this->ve->surat_tidak_mampu();
		echo json_encode($data);
    }

     public function verifikasi_surat_tidak_mampu()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_tidak_mampu();
		
		echo "<script>alert('Surat Tidak Mampu berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srttdkmampu');
			</script>";
	   } else {
		 	if(isset($_POST['id_tm'])){
				$data['verifikasi_tidak_mampu'] = $this->ve->surat_tidak_mampu();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srttdkmampu/verifikasi_operator_tidak_mampu', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srttdkmampu'));
		 }
	   }  
   }

   public function tolak_tidak_mampu(){
    if (isset($_POST['id_tm'])) {
        $data['verifikasi_tidak_mampu'] = $this->ve->surat_tidak_mampu();
	    $this->template->load('template', 'srttdkmampu/tolak_verifikasi_tidak_mampu',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srttdkmampu'));
		 }
   }

   function tolak_surat_tidak_mampu(){
       if (isset($_POST['id_tm'])) { 
	    $this->ve->tolak_surat_tidak_mampu(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srttdkmampu');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srttdkmampu'));
		 }
   }

   public function terima_tidak_mampu(){
    if (isset($_POST['id_tm'])) {
        $data['verifikasi_tidak_mampu'] = $this->ve->surat_tidak_mampu();
	    $this->template->load('template', 'srttdkmampu/terima_verifikasi_tidak_mampu',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srttdkmampu'));
		 }
   }

   function terima_surat_tidak_mampu(){
       if (isset($_POST['id_tm'])) { 
	    $this->ve->terima_surat_tidak_mampu(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srttdkmampu');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srttdkmampu'));
		 }
   }

   function selesai_surat_tidak_mampu(){
       if(isset($_POST['id_tm'])){
        $this->ve->selesai_surat_tidak_mampu(); 
        echo "<script>alert('Surat Tidak Mampu Sudah Selesai');
			 window.location.replace('srttdkmampu');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srttdkmampu'));
        }
   }

   function cetak_surat_tidak_mampu(){
        if(isset($_POST['id_tm'])){
            $data['cetak'] = $this->ve->cetak_surat_tidak_mampu();
            $this->load->view('rpt_sksrttdkmampu',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srttdkmampu'));
        }
   }

    function srtpenghasilan(){
        $data['penghasilan'] = $this->ve->penghasilan();
        $this->template->load('template','srtpenghasilan/verifikasi_penghasilan',$data);
    }

    function surat_penghasilan(){
        $data=$this->ve->surat_penghasilan();
		echo json_encode($data);
    }

     public function verifikasi_surat_penghasilan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_penghasilan();
		
		echo "<script>alert('Surat Penghasilan berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtpenghasilan');
			</script>";
	   } else {
		 	if(isset($_POST['id_pe'])){
				$data['verifikasi_penghasilan'] = $this->ve->surat_penghasilan();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtpenghasilan/verifikasi_operator_penghasilan', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtpenghasilan'));
		 }
	   }  
   }

   public function tolak_penghasilan(){
    if (isset($_POST['id_pe'])) {
        $data['verifikasi_penghasilan'] = $this->ve->surat_penghasilan();
	    $this->template->load('template', 'srtpenghasilan/tolak_verifikasi_penghasilan',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtpenghasilan'));
		 }
   }

   function tolak_surat_penghasilan(){
       if (isset($_POST['id_pe'])) { 
	    $this->ve->tolak_surat_penghasilan(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtpenghasilan');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtpenghasilan'));
		 }
   }

    public function terima_penghasilan(){
    if (isset($_POST['id_pe'])) {
        $data['verifikasi_penghasilan'] = $this->ve->surat_penghasilan();
	    $this->template->load('template', 'srtpenghasilan/terima_verifikasi_penghasilan',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtpenghasilan'));
		 }
   }

   function terima_surat_penghasilan(){
       if (isset($_POST['id_pe'])) { 
	    $this->ve->terima_surat_penghasilan(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtpenghasilan');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtpenghasilan'));
		 }
   }

   function selesai_surat_penghasilan(){
       if(isset($_POST['id_pe'])){
        $this->ve->selesai_surat_penghasilan(); 
        echo "<script>alert('Surat Penghasilan Sudah Selesai');
			 window.location.replace('srtpenghasilan');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtpenghasilan'));
        }
   }

   function cetak_surat_penghasilan(){
        if(isset($_POST['id_pe'])){
            $data['cetak'] = $this->ve->surat_penghasilan();
            $this->load->view('rpt_sksrtpenghasilan',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtpenghasilan'));
        }
   }

    function srttdkpenghasilan(){
        $data['tidak_penghasilan'] = $this->ve->tidak_penghasilan();
        $this->template->load('template','srttdkpenghasilan/verifikasi_tidak_penghasilan',$data);
    }

    function surat_tidak_penghasilan(){
        $data=$this->ve->surat_tidak_penghasilan();
		echo json_encode($data);
    }

     public function verifikasi_surat_tidak_penghasilan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_tidak_penghasilan();
		
		echo "<script>alert('Surat Tidak Penghasilan berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srttdkpenghasilan');
			</script>";
	   } else {
		 	if(isset($_POST['id_tp'])){
				$data['verifikasi_tidak_penghasilan'] = $this->ve->surat_tidak_penghasilan();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srttdkpenghasilan/verifikasi_operator_tidak_penghasilan', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
		 }
	   }  
   }

   public function tolak_tidak_penghasilan(){
    if (isset($_POST['id_tp'])) {
        $data['verifikasi_tidak_penghasilan'] = $this->ve->surat_tidak_penghasilan();
	    $this->template->load('template', 'srttdkpenghasilan/tolak_verifikasi_penghasilan',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
		 }
   }

   function tolak_surat_tidak_penghasilan(){
       if (isset($_POST['id_tp'])) { 
	    $this->ve->tolak_surat_tidak_penghasilan(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srttdkpenghasilan');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
		 }
   }

   public function terima_tidak_penghasilan(){
    if (isset($_POST['id_tp'])) {
        $data['verifikasi_tidak_penghasilan'] = $this->ve->surat_tidak_penghasilan();
	    $this->template->load('template', 'srttdkpenghasilan/terima_verifikasi_tidak_penghasilan',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
		 }
   }

   function terima_surat_tidak_penghasilan(){
       if (isset($_POST['id_tp'])) { 
	    $this->ve->terima_surat_tidak_penghasilan(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srttdkpenghasilan');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
		 }
   }

   function selesai_surat_tidak_penghasilan(){
       if(isset($_POST['id_tp'])){
        $this->ve->selesai_surat_tidak_penghasilan(); 
        echo "<script>alert('Surat Tidak Penghasilan Sudah Selesai');
			 window.location.replace('srttdkpenghasilan');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
        }
   }

   function cetak_surat_tidak_penghasilan(){
        if(isset($_POST['id_tp'])){
            $data['cetak'] = $this->ve->surat_tidak_penghasilan();
            $this->load->view('rpt_sksrttdkpenghasilan',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srttdkpenghasilan'));
        }
   }

    function srtskck(){
        $data['skck'] = $this->ve->skck();
        $this->template->load('template','srtskck/verifikasi_skck',$data);
    }

    function surat_skck(){
        $data=$this->ve->surat_skck();
		echo json_encode($data);
    }

     public function verifikasi_surat_skck()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_skck();
		
		echo "<script>alert('Surat SKCK berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtskck');
			</script>";
	   } else {
		 	if(isset($_POST['id_sk'])){
				$data['verifikasi_skck'] = $this->ve->surat_skck();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtskck/verifikasi_operator_skck', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtskck'));
		 }
	   }  
   }

   public function tolak_skck(){
    if (isset($_POST['id_sk'])) {
        $data['verifikasi_skck'] = $this->ve->surat_skck();
	    $this->template->load('template', 'srtskck/tolak_verifikasi_skck',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtskck'));
		 }
   }

   function tolak_surat_skck(){
       if (isset($_POST['id_sk'])) { 
	    $this->ve->tolak_surat_skck(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtskck');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtskck'));
		 }
   }

    public function terima_skck(){
    if (isset($_POST['id_sk'])) {
        $data['verifikasi_skck'] = $this->ve->surat_skck();
	    $this->template->load('template', 'srtskck/terima_verifikasi_skck',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtskck'));
		 }
   }

   function terima_surat_skck(){
       if (isset($_POST['id_sk'])) { 
	    $this->ve->terima_surat_skck(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtskck');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtskck'));
		 }
   }

   function selesai_surat_skck(){
       if(isset($_POST['id_sk'])){
        $this->ve->selesai_surat_skck(); 
        echo "<script>alert('Surat SKCK Penghasilan Sudah Selesai');
			 window.location.replace('srtskck');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtskck'));
        }
   }

   function cetak_surat_skck(){
        if(isset($_POST['id_sk'])){
            $data['cetak'] = $this->ve->cetak_surat_skck();
            $this->load->view('rpt_sksrtskck',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtskck'));
        }
   }

    function srtcerai(){
        $data['cerai'] = $this->ve->cerai();
        $this->template->load('template','srtcerai/verifikasi_cerai',$data);
    }

    function surat_cerai(){
        $data=$this->ve->surat_cerai();
		echo json_encode($data);
    }

     public function verifikasi_surat_cerai()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_cerai();
		
		echo "<script>alert('Surat Cerai berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtcerai');
			</script>";
	   } else {
		 	if(isset($_POST['id_ce'])){
				$data['verifikasi_cerai'] = $this->ve->surat_cerai();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtcerai/verifikasi_operator_cerai', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtcerai'));
		 }
	   }  
   }

   public function tolak_cerai(){
    if (isset($_POST['id_ce'])) {
        $data['verifikasi_cerai'] = $this->ve->surat_cerai();
	    $this->template->load('template', 'srtcerai/tolak_verifikasi_cerai',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtcerai'));
		 }
   }

   function tolak_surat_cerai(){
       if (isset($_POST['id_ce'])) { 
	    $this->ve->tolak_surat_cerai(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtcerai');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtcerai'));
		 }
   }

    public function terima_cerai(){
    if (isset($_POST['id_ce'])) {
        $data['verifikasi_cerai'] = $this->ve->surat_cerai();
	    $this->template->load('template', 'srtcerai/terima_verifikasi_cerai',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtcerai'));
		 }
   }

   function terima_surat_cerai(){
       if (isset($_POST['id_ce'])) { 
	    $this->ve->terima_surat_cerai(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtcerai');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtcerai'));
		 }
   }

   function selesai_surat_cerai(){
       if(isset($_POST['id_ce'])){
        $this->ve->selesai_surat_cerai(); 
        echo "<script>alert('Surat Cerai Penghasilan Sudah Selesai');
			 window.location.replace('srtcerai');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtcerai'));
        }
   }

   function cetak_surat_cerai(){
        if(isset($_POST['id_ce'])){
            $data['cetak'] = $this->ve->surat_cerai();
            $this->load->view('rpt_sksrtcerai',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtcerai'));
        }
   }

    function srtaktanikah(){
        $data['akta_nikah'] = $this->ve->akta_nikah();
        $this->template->load('template','srtaktanikah/verifikasi_akta_nikah',$data);
    }

    function surat_akta_nikah(){
        $data=$this->ve->surat_akta_nikah();
		echo json_encode($data);
    }

     public function verifikasi_surat_akta_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_akta_nikah();
		
		echo "<script>alert('Surat Akta Nikah berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtaktanikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_an'])){
				$data['verifikasi_akta_nikah'] = $this->ve->surat_akta_nikah();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtaktanikah/verifikasi_operator_akta_nikah', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtaktanikah'));
		 }
	   }  
   }

   public function tolak_akta_nikah(){
    if (isset($_POST['id_an'])) {
        $data['verifikasi_akta_nikah'] = $this->ve->surat_akta_nikah();
	    $this->template->load('template', 'srtaktanikah/tolak_verifikasi_akta_nikah',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtaktanikah'));
		 }
   }

   function tolak_surat_akta_nikah(){
       if (isset($_POST['id_an'])) { 
	    $this->ve->tolak_surat_akta_nikah(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtaktanikah');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtaktanikah'));
		 }
   }

   public function terima_akta_nikah(){
    if (isset($_POST['id_an'])) {
        $data['verifikasi_cerai'] = $this->ve->surat_akta_nikah();
	    $this->template->load('template', 'srtaktanikah/terima_verifikasi_akta_nikah',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtaktanikah'));
		 }
   }

   function terima_surat_akta_nikah(){
       if (isset($_POST['id_ce'])) { 
	    $this->ve->terima_surat_akta_nikah(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtaktanikah');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtaktanikah'));
		 }
   }

   function selesai_surat_akta_nikah(){
       if(isset($_POST['id_an'])){
        $this->ve->selesai_surat_akta_nikah(); 
        echo "<script>alert('Surat Akta Nikah Penghasilan Sudah Selesai');
			 window.location.replace('srtaktanikah');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtaktanikah'));
        }
   }

   function cetak_surat_akta_nikah(){
        if(isset($_POST['id_an'])){
            $data['cetak'] = $this->ve->surat_akta_nikah();
            $this->load->view('rpt_sksrtaktanikah',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtaktanikah'));
        }
   }

    function srtskbd(){
        $data['skbd'] = $this->ve->skbd();
        $this->template->load('template','srtskbd/verifikasi_skbd',$data);
    }
    
    function surat_skbd(){
        $data=$this->ve->surat_skbd();
		echo json_encode($data);
    }

     public function verifikasi_surat_skbd()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_skbd();
		
		echo "<script>alert('Surat SKBD berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtskbd');
			</script>";
	   } else {
		 	if(isset($_POST['id_skbd'])){
				$data['verifikasi_skbd'] = $this->ve->surat_skbd();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtskbd/verifikasi_operator_skbd', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtskbd'));
		 }
	   }  
   }

   public function tolak_skbd(){
    if (isset($_POST['id_skbd'])) {
        $data['verifikasi_skbd'] = $this->ve->surat_skbd();
	    $this->template->load('template', 'srtskbd/tolak_verifikasi_skbd',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtskbd'));
		 }
   }

   function tolak_surat_skbd(){
       if (isset($_POST['id_skbd'])) { 
	    $this->ve->tolak_surat_skbd(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtskbd');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtskbd'));
		 }
   }

   public function terima_skbd(){
    if (isset($_POST['id_skbd'])) {
        $data['verifikasi_skbd'] = $this->ve->surat_skbd();
	    $this->template->load('template', 'srtskbd/terima_verifikasi_skbd',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtskbd'));
		 }
   }

   function terima_surat_skbd(){
       if (isset($_POST['id_skbd'])) { 
	    $this->ve->terima_surat_skbd(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtskbd');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtskbd'));
		 }
   }

   function selesai_surat_skbd(){
       if(isset($_POST['id_skbd'])){
        $this->ve->selesai_surat_skbd(); 
        echo "<script>alert('Surat SKBD Penghasilan Sudah Selesai');
			 window.location.replace('srtskbd');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtskbd'));
        }
   }

   function cetak_surat_skbd(){
        if(isset($_POST['id_skbd'])){
            $data['cetak'] = $this->ve->surat_skbd();
            $this->load->view('rpt_sksrtskbd',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtskbd'));
        }
   }

    function srtketerangan(){
        $data['keterangan'] = $this->ve->keterangan();
        $this->template->load('template','srtketerangan/verifikasi_keterangan',$data);
    }
    
    function surat_keterangan(){
        $data=$this->ve->surat_keterangan();
		echo json_encode($data);
    }

     public function verifikasi_surat_keterangan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->ve->verifikasi_surat_keterangan();
		
		echo "<script>alert('Surat Keterangan berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_surat/srtketerangan');
			</script>";
	   } else {
		 	if(isset($_POST['id_ke'])){
				$data['verifikasi_keterangan'] = $this->ve->surat_keterangan();
				$data['kecamatan'] = $this->ve->kecamatan();
				$data['kelurahan'] = $this->ve->get_kelurahan();
				$data['pekerjaan'] = $this->ve->pekerjaan();
				$data['status'] = $this->ve->status_perkawinan();
				$data['agama'] = $this->ve->agama();
				$this->template->load('template', 'srtketerangan/verifikasi_operator_keterangan', $data);
		 } else {
			 redirect(base_url('Verifikasi_surat/srtketerangan'));
		 }
	   }  
   }

   public function tolak_keterangan(){
    if (isset($_POST['id_ke'])) {
        $data['verifikasi_keterangan'] = $this->ve->surat_keterangan();
	    $this->template->load('template', 'srtketerangan/tolak_verifikasi_keterangan',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtketerangan'));
		 }
   }

   function tolak_surat_keterangan(){
       if (isset($_POST['id_ke'])) { 
	    $this->ve->tolak_surat_keterangan(); 
        echo "<script>alert('Penolakan Data Berhasil Dilakukan');
			 window.location.replace('srtketerangan');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtketerangan'));
		 }
   }

   public function terima_keterangan(){
    if (isset($_POST['id_ke'])) {
        $data['verifikasi_keterangan'] = $this->ve->surat_keterangan();
	    $this->template->load('template', 'srtketerangan/terima_verifikasi_keterangan',$data);	
	 } else {
			 redirect(base_url('Verifikasi_surat/srtketerangan'));
		 }
   }

   function terima_surat_keterangan(){
       if (isset($_POST['id_ke'])) { 
	    $this->ve->terima_surat_keterangan(); 
        echo "<script>alert('Data Sudah Berhasil Dilakukan');
			 window.location.replace('srtketerangan');
			</script>";	
		
	  } else {
			 redirect(base_url('Verifikasi_surat/srtketerangan'));
		 }
   }

   function selesai_surat_keterangan(){
       if(isset($_POST['id_ke'])){
        $this->ve->selesai_surat_keterangan(); 
        echo "<script>alert('Surat Keterangan Penghasilan Sudah Selesai');
			 window.location.replace('srtketerangan');
			</script>";	
        } else{
            redirect(base_url('Verifikasi_surat/srtketerangan'));
        }
   }

   function cetak_surat_keterangan(){
        if(isset($_POST['id_ke'])){
            $data['cetak'] = $this->ve->surat_keterangan();
            $this->load->view('rpt_sksrtketerangan',$data);
        } else{
            redirect(base_url('Verifikasi_surat/srtketerangan'));
        }
   }

   function srtusaha(){
    $data['usaha'] = $this->ve->usaha();
    $this->template->load('template','srtusaha/verifikasi_usaha',$data);
}

function surat_usaha(){
    $data=$this->ve->surat_usaha();
    echo json_encode($data);
}

 public function verifikasi_surat_usaha()
{
   if(isset($_POST['edit'])) 
   {
    $this->ve->verifikasi_surat_usaha();
    
    echo "<script>alert('Surat Usaha berhasil di Verifikasi');
         window.location.replace('../Verifikasi_surat/srtusaha');
        </script>";
   } else {
         if(isset($_POST['id_us'])){
            $data['verifikasi_usaha'] = $this->ve->surat_usaha();
            $data['kecamatan'] = $this->ve->kecamatan();
            $data['kelurahan'] = $this->ve->get_kelurahan();
            $data['pekerjaan'] = $this->ve->pekerjaan();
            $data['status'] = $this->ve->status_perkawinan();
            $data['agama'] = $this->ve->agama();
            $this->template->load('template', 'srtusaha/verifikasi_operator_usaha', $data);
     } else {
         redirect(base_url('Verifikasi_surat/srtusaha'));
     }
   }  
}

public function tolak_usaha(){
if (isset($_POST['id_us'])) {
    $data['verifikasi_usaha'] = $this->ve->surat_usaha();
    $this->template->load('template', 'srtusaha/tolak_verifikasi_usaha',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtusaha'));
     }
}

function tolak_surat_usaha(){
   if (isset($_POST['id_us'])) { 
    $this->ve->tolak_surat_usaha(); 
    echo "<script>alert('Penolakan Data Berhasil Dilakukan');
         window.location.replace('srtusaha');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtusaha'));
     }
}

public function terima_usaha(){
if (isset($_POST['id_us'])) {
    $data['verifikasi_usaha'] = $this->ve->surat_usaha();
    $this->template->load('template', 'srtusaha/terima_verifikasi_usaha',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtusaha'));
     }
}

function terima_surat_usaha(){
   if (isset($_POST['id_us'])) { 
    $this->ve->terima_surat_usaha(); 
    echo "<script>alert('Data Sudah Berhasil Dilakukan');
         window.location.replace('srtusaha');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtusaha'));
     }
}

function selesai_surat_usaha(){
   if(isset($_POST['id_us'])){
    $this->ve->selesai_surat_usaha(); 
    echo "<script>alert('Surat Usaha Penghasilan Sudah Selesai');
         window.location.replace('srtusaha');
        </script>";	
    } else{
        redirect(base_url('Verifikasi_surat/srtusaha'));
    }
}

function cetak_surat_usaha(){
    if(isset($_POST['id_us'])){
        $data['cetak'] = $this->ve->cetak_surat_usaha();
        $this->load->view('rpt_sksrtusaha',$data);
    } else{
        redirect(base_url('Verifikasi_surat/srtusaha'));
    }
}

function srtsengketa(){
    $data['sengketa'] = $this->ve->sengketa();
    $this->template->load('template','srtsengketa/verifikasi_sengketa',$data);
}

function surat_sengketa(){
    $data=$this->ve->surat_sengketa();
    echo json_encode($data);
}

 public function verifikasi_surat_sengketa()
{
   if(isset($_POST['edit'])) 
   {
    $this->ve->verifikasi_surat_sengketa();
    
    echo "<script>alert('Surat Sengketa berhasil di Verifikasi');
         window.location.replace('../Verifikasi_surat/srtsengketa');
        </script>";
   } else {
         if(isset($_POST['id_se'])){
            $data['verifikasi_sengketa'] = $this->ve->surat_sengketa();
            $data['kecamatan'] = $this->ve->kecamatan();
            $data['kelurahan'] = $this->ve->get_kelurahan();
            $data['pekerjaan'] = $this->ve->pekerjaan();
            $data['status'] = $this->ve->status_perkawinan();
            $data['agama'] = $this->ve->agama();
            $this->template->load('template', 'srtsengketa/verifikasi_operator_sengketa', $data);
     } else {
         redirect(base_url('Verifikasi_surat/srtsengketa'));
     }
   }  
}

public function tolak_sengketa(){
if (isset($_POST['id_se'])) {
    $data['verifikasi_sengketa'] = $this->ve->surat_sengketa();
    $this->template->load('template', 'srtsengketa/tolak_verifikasi_sengketa',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtsengketa'));
     }
}

function tolak_surat_sengketa(){
   if (isset($_POST['id_se'])) { 
    $this->ve->tolak_surat_sengketa(); 
    echo "<script>alert('Penolakan Data Berhasil Dilakukan');
         window.location.replace('srtsengketa');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtsengketa'));
     }
}

public function terima_sengketa(){
if (isset($_POST['id_se'])) {
    $data['verifikasi_sengketa'] = $this->ve->surat_sengketa();
    $this->template->load('template', 'srtsengketa/terima_verifikasi_sengketa',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtsengketa'));
     }
}

function terima_surat_sengketa(){
   if (isset($_POST['id_se'])) { 
    $this->ve->terima_surat_sengketa(); 
    echo "<script>alert('Data Sudah Berhasil Dilakukan');
         window.location.replace('srtsengketa');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtsengketa'));
     }
}

function selesai_surat_sengketa(){
   if(isset($_POST['id_se'])){
    $this->ve->selesai_surat_sengketa(); 
    echo "<script>alert('Surat Sengketa Penghasilan Sudah Selesai');
         window.location.replace('srtsengketa');
        </script>";	
    } else{
        redirect(base_url('Verifikasi_surat/srtsengketa'));
    }
}

function cetak_surat_sengketa(){
    if(isset($_POST['id_se'])){
        $data['cetak'] = $this->ve->cetak_surat_sengketa();
        $this->load->view('rpt_sksrtsengketa',$data);
    } else{
        redirect(base_url('Verifikasi_surat/srtsengketa'));
    }
}

function srtdomisili(){
    $data['domisili'] = $this->ve->domisili();
    $this->template->load('template','srtdomisili/verifikasi_domisili',$data);
}

function surat_domisili(){
    $data=$this->ve->surat_domisili();
    echo json_encode($data);
}

 public function verifikasi_surat_domisili()
{
   if(isset($_POST['edit'])) 
   {
    $this->ve->verifikasi_surat_domisili();
    
    echo "<script>alert('Surat Domisili berhasil di Verifikasi');
         window.location.replace('../Verifikasi_surat/srtdomisili');
        </script>";
   } else {
         if(isset($_POST['id_do'])){
            $data['verifikasi_domisili'] = $this->ve->surat_domisili();
            $data['kecamatan'] = $this->ve->kecamatan();
            $data['kelurahan'] = $this->ve->get_kelurahan();
            $data['pekerjaan'] = $this->ve->pekerjaan();
            $data['status'] = $this->ve->status_perkawinan();
            $data['agama'] = $this->ve->agama();
            $this->template->load('template', 'srtdomisili/verifikasi_operator_domisili', $data);
     } else {
         redirect(base_url('Verifikasi_surat/srtdomisili'));
     }
   }  
}

public function tolak_domisili(){
if (isset($_POST['id_do'])) {
    $data['verifikasi_domisili'] = $this->ve->surat_domisili();
    $this->template->load('template', 'srtdomisili/tolak_verifikasi_domisili',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtdomisili'));
     }
}

function tolak_surat_domisili(){
   if (isset($_POST['id_do'])) { 
    $this->ve->tolak_surat_domisili(); 
    echo "<script>alert('Penolakan Data Berhasil Dilakukan');
         window.location.replace('srtdomisili');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtdomisili'));
     }
}

public function terima_domisili(){
if (isset($_POST['id_do'])) {
    $data['verifikasi_domisili'] = $this->ve->surat_domisili();
    $this->template->load('template', 'srtdomisili/terima_verifikasi_domisili',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtdomisili'));
     }
}

function terima_surat_domisili(){
   if (isset($_POST['id_do'])) { 
    $this->ve->terima_surat_domisili(); 
    echo "<script>alert('Data Sudah Berhasil Dilakukan');
         window.location.replace('srtdomisili');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtdomisili'));
     }
}

function selesai_surat_domisili(){
   if(isset($_POST['id_do'])){
    $this->ve->selesai_surat_domisili(); 
    echo "<script>alert('Surat Domisili Penghasilan Sudah Selesai');
         window.location.replace('srtdomisili');
        </script>";	
    } else{
        redirect(base_url('Verifikasi_surat/srtdomisili'));
    }
}

function cetak_surat_domisili(){
    if(isset($_POST['id_do'])){
        $data['cetak'] = $this->ve->cetak_surat_domisili();
        $this->load->view('rpt_sksrtdomisili',$data);
    } else{
        redirect(base_url('Verifikasi_surat/srtdomisili'));
    }
}


function srtortu(){
    $data['ortu'] = $this->ve->ortu();
    $this->template->load('template','srtortu/verifikasi_ortu',$data);
}

function surat_ortu(){
    $data=$this->ve->surat_ortu();
    echo json_encode($data);
}

 public function verifikasi_surat_ortu()
{
   if(isset($_POST['edit'])) 
   {
    $this->ve->verifikasi_surat_ortu();
    
    echo "<script>alert('Surat Ortu berhasil di Verifikasi');
         window.location.replace('../Verifikasi_surat/srtortu');
        </script>";
   } else {
         if(isset($_POST['id_or'])){
            $data['verifikasi_ortu'] = $this->ve->surat_ortu();
            $data['kecamatan'] = $this->ve->kecamatan();
            $data['kelurahan'] = $this->ve->get_kelurahan();
            $data['pekerjaan'] = $this->ve->pekerjaan();
            $data['status'] = $this->ve->status_perkawinan();
            $data['agama'] = $this->ve->agama();
            $this->template->load('template', 'srtortu/verifikasi_operator_ortu', $data);
     } else {
         redirect(base_url('Verifikasi_surat/srtortu'));
     }
   }  
}

public function tolak_ortu(){
if (isset($_POST['id_or'])) {
    $data['verifikasi_domisili'] = $this->ve->surat_ortu();
    $this->template->load('template', 'srtortu/tolak_verifikasi_ortu',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtortu'));
     }
}

function tolak_surat_ortu(){
   if (isset($_POST['id_or'])) { 
    $this->ve->tolak_surat_ortu(); 
    echo "<script>alert('Penolakan Data Berhasil Dilakukan');
         window.location.replace('srtortu');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtortu'));
     }
}

public function terima_ortu(){
if (isset($_POST['id_or'])) {
    $data['verifikasi_ortu'] = $this->ve->surat_ortu();
    $this->template->load('template', 'srtortu/terima_verifikasi_ortu',$data);	
 } else {
         redirect(base_url('Verifikasi_surat/srtortu'));
     }
}

function terima_surat_ortu(){
   if (isset($_POST['id_or'])) { 
    $this->ve->terima_surat_ortu(); 
    echo "<script>alert('Data Sudah Berhasil Dilakukan');
         window.location.replace('srtortu');
        </script>";	
    
  } else {
         redirect(base_url('Verifikasi_surat/srtortu'));
     }
}

function selesai_surat_ortu(){
   if(isset($_POST['id_or'])){
    $this->ve->selesai_surat_ortu(); 
    echo "<script>alert('Surat Ortu Penghasilan Sudah Selesai');
         window.location.replace('srtortu');
        </script>";	
    } else{
        redirect(base_url('Verifikasi_surat/srtortu'));
    }
}

function cetak_surat_ortu(){
    if(isset($_POST['id_or'])){
        $data['cetak'] = $this->ve->cetak_surat_ortu();
        $data['ayah'] = $this->ve->ayah_ortu();
        $data['ibu'] = $this->ve->ibu_ortu();
        $data['calon'] = $this->ve->calon_ortu();
        $data['lurah'] = $this->ve->lurah_ortu();
        $data['nomor'] = $this->ve->nomor_surat_ortu();
        $this->load->view('rpt_sksrtortu',$data);
    } else{
        redirect(base_url('Verifikasi_surat/srtortu'));
    }
}
    
}