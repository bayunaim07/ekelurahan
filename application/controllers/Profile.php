<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_profile_model');
        $this->load->model('Tbl_profile_model', 'user');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
    }

    public function index()
    {
		
       $this->template->load('template', 'profile/profile');
    } 
    
   
    
	
	
	public function update(){
		 if (isset($_POST['id'])) {
		 
	    $this->user->edit(); 
		 $this->user->simpan_pass(); 
	
		
		echo "<script>alert('Data Profile Berhasil diEdit');
			 window.location.replace('../Profile');
			</script>";	
	  } else {
			 redirect(base_url('Profile'));
		 }
	}		
	

}

