<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Referensi_camat extends CI_Controller
{
	
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_referensi_camat_model','rc');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
       
    }

    public function index()
    {
		$data['camat']=$this->rc->referensi_camat();
        $this->template->load('template','referensi_camat/referensi_camat',$data);
    } 
	
	public function tambah_camat()
	{
		$data['kecamatan']= $this->rc->kecamatan();
		$data['kelurahan']= $this->rc->kelurahan();
		$this->template->load('template','referensi_camat/tambah_camat',$data);	
		
    }
	
	public function simpan()
	 {
		 if (isset($_POST['simpan'])) {
		 
	    $this->rc->simpan(); 
		echo "<script>alert('Data Camat Berhasil Ditambah ');
			 window.location.replace('../Referensi_camat');
			</script>";	
	  } else {
			 redirect(base_urc('Referensi_camat'));
		 }
	}	
	
	
	public function edit()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->rc->update();
		
		echo "<script>alert('Data Camat berhasil di update');
			 window.location.replace('../Referensi_camat');
			</script>
			";
	   } else {
		 if(isset($_POST['id_camat'])){
		 $id=$this->input->post('id_camat');
		 $data['edit'] = $this->db->get_where('referensi_camat', array('id_camat' => $id))->row_Array();
		 $data['kecamatan']= $this->rc->kecamatan();
		 $data['kecamatan']= $this->rc->kecamatan();
		 $data['join_kec'] = $this->rc->get_join_kec();
         $data['join_kel'] = $this->rc->get_join_kel();
		 
		 
		
		 
		 } 
		

		 else {
			 redirect(base_urc('referensi_camat'));
		 }
		 $this->template->load('template', 'referensi_camat/edit_camat', $data);
	   }
		 
   }
	
   public function hapus()
   {
	   if(isset($_POST['id_camat'])) 
	   {
		$this->rc->hapus();
		
		
		echo "<script>alert('Data camat Berhasil Dihapus');
			 window.location.replace('../Referensi_camat');
			</script>	
			";
	   } else {
		 if(isset($_POST['id_hapus'])){
		 $id=$this->input->post('id_hapus');
		 
		$data['edit'] = $this->db->get_where('referensi_camat', array('id' => $id))->row_Array();
		
		 
		 } else {
			 redirect(base_urc('Referensi_camat'));
		 }
	   }  
		 $this->template->load('template', 'referensi_camat/hapus_camat', $data);
   }	   	
   
	
    
    

}

