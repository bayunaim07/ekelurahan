<?php
Class Login extends CI_Controller{
	 
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library(array('session', 'encryption','form_validation')); 
		
    }
    
    
    function index(){
        $this->load->view('auth/login');
		chek_seesion_login();
		
    }
    
    function cheklogin(){

        $this->rules();
        $username      = $this->input->post('username');
        //$password   = $this->input->post('password');
        $password = $this->input->post('password');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');

            // set default prefix (awalan) dan suffix (akhiran) pesan error
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } 
        else {
            $hashPass = password_hash($password,PASSWORD_DEFAULT);
            $test     = password_verify($password, $hashPass);
            // query chek users
            $this->db->where('username',$username);
            //$this->db->where('password',  $test);
            $users       = $this->db->get('user');
            
            if($users->num_rows()>0){
                $user = $users->row_array();
                if(password_verify($password,$user['password'])){
                    // retrive user data to session
                    $this->session->set_userdata($user);
                    redirect('Home');
                }else{
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert">Username atau password salah</div>');
                    redirect('Login');
                }
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert">Username atau Password Tidak Ditemukan</div>');
                redirect('Login');
            }
        }
    }
    
    function rules() 
    {
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        
        $this->form_validation->set_message('required', '{field} wajib diisi');


        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    function register(){
        $this->load->view('auth/register');
    }

    function ceknik() {
		$this->db2 = $this->load->database('binjai_satu_aplikasi', TRUE);
        $nik=$this->input->post('Nik');
        $this->db2->select('*');
		$this->db2->from('datanik');
        $this->db2->where('nik', $nik);
		$query= $this->db2->get()->row_array();
		$nik=isset($query['nik'])? $query['nik']:'';
        if($nik!=NULL){
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('username', $nik);
            $user= $this->db->get()->row_array();
			$username=isset($user['username'])? $user['username']:'';
            if($username!=NULL){
                echo json_encode($user);
            }
            else{
                echo json_encode($query);
            }
        }
    }

    function daftar(){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $this->input->post('email'));
        $user= $this->db->get()->row_array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE){
            echo json_encode([
                'errvalidation' =>  true,
                'validationitem' => [
                    'email' => form_error('email'),
                    'password' => form_error('password'),
                ],
                'status' => false
            ]);
            return;
        }

        if($user['email']!=NULL){
            echo json_encode([
                'status' => false,
                'message' => 'E-mail sudah dipakai'
            ]);
        }
        else {
            $password       = $this->input->post('password',TRUE);
            $options        = array("cost"=>4);
            $hashPassword   = password_hash($password,PASSWORD_BCRYPT,$options);
            //$verifToken = substr(strtr(base64_encode(random_bytes(32)), '+/', '-_'), 0, 32).'_'.time();
            $data = array(
                'username'=> $this->input->post('nik'),
                'full_name'=> $this->input->post('nama'),
                'email'=> $this->input->post('email'),
				'id_kel'=> $this->input->post('kel'),
				'id_kec'=> $this->input->post('kec'),
                'id_user_level' => '3',
                'password'      => $hashPassword,
                'is_aktif'  => 'y', 
            );
            $query = $this->db->insert('user', $data);
            
            if($query){
                echo json_encode(['status' => true]);
            }
        }  
    }

    /*public function send_email($verifToken)
    {
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
        $config = [
           'mailtype'  => 'html',
           'charset'   => 'utf-8',
           'protocol'  => 'smtp',
           'smtp_host' => 'smtp.gmail.com',
           'smtp_user' => 'wbsbinjaikota@gmail.com',  // Email gmail
           'smtp_pass'   => 'mcqy uorh hxhy ofll',  // Password gmail
           'smtp_crypto' => 'tls',
           'smtp_port'   => 587,
           'crlf'    => "\r\n",
           'newline' => "\r\n"
        ];

       // Load library email dan konfigurasinya
       $this->load->library('email', $config);

       // Email dan nama pengirim
       $this->email->from('no-reply@wbsbinjaikota.com', 'wbs.binjaikota.go.id');

       // Email penerima
       $this->email->to($email); // Ganti dengan email tujuan

       // Lampiran email, isi dengan url/path file
       //$this->email->attach('https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');

       // Subject email
       $this->email->subject('Verifikasi Registrasi User Aplikasi WBS Pemko Binjai | wbs.binjaikota.go.id');

       // Isi email
       $this->email->message("Ini adalah pesan mengenai akun anda.<br><br>Password : $pass <br><br><strong><a href='".base_url()."login/' target='_blank' rel='noopener'>KLIK DISINI</a></strong> untuk melanjutkan login aplikasi.");
       $this->email->send();
   }*/
   
    function logout(){
        $this->session->sess_destroy();
        $this->session->set_flashdata('status_login','Anda sudah berhasil keluar dari aplikasi');
        redirect('Beranda');
    }
}
