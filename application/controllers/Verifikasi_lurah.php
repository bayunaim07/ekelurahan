<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Verifikasi_lurah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
		$this->load->model('Tbl_verifikasi_lurah','lu');
        $this->load->library('form_validation');        
		$this->load->library('datatables');  
    }

    public function index()
    {
		$data['lurah']=$this->lu->surat();
        $this->template->load('template','lurah/surat/surat',$data);
    } 

    public function srtnikah(){
        $data['lurah'] = $this->lu->nikah();
        $this->template->load('template','lurah/surat_nikah/verifikasi_lurah',$data);
    }

    function surat_nikah(){
        $data=$this->lu->srt_nikah();
		echo json_encode($data);
    }

     public function verifikasi_surat_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_nikah();
		
		echo "<script>alert('Surat Nikah berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtnikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_sn'])){
				$data['verifikasi_nikah'] = $this->lu->srt_nikah();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_nikah/verifikasi_surat_nikah', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtnikah'));
		 }
	   }  
   }

    public function srtblmnikah(){
        $data['lurah'] = $this->lu->surat_belum_nikah();
        $this->template->load('template','lurah/surat_belum_nikah/verifikasi_lurah',$data);
    }

    function surat_belum_nikah(){
        $data=$this->lu->surat_belum_nikah();
		echo json_encode($data);
    }

     public function verifikasi_surat_belum_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_belum_nikah();
		
		echo "<script>alert('Surat Belum Nikah berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtblmnikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_bn'])){
				$data['verifikasi_belum_nikah'] = $this->lu->surat_belum_nikah();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_belum_nikah/verifikasi_surat_belum_nikah', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtblmnikah'));
		 }
	   }  
   }

    public function srtwaris(){
        $data['lurah'] = $this->lu->waris();
        $this->template->load('template','lurah/surat_waris/verifikasi_lurah',$data);
    }

    function surat_waris(){
        $data=$this->lu->surat_waris();
		echo json_encode($data);
    }

     public function verifikasi_surat_waris()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_waris();
		
		echo "<script>alert('Surat Waris berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtwaris');
			</script>";
	   } else {
		 	if(isset($_POST['id_wa'])){
				$data['verifikasi_waris'] = $this->lu->surat_waris();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_waris/verifikasi_surat_waris', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtwaris'));
		 }
	   }  
   }

    public function srtkematian(){
        $data['lurah'] = $this->lu->mati();
        $this->template->load('template','lurah/surat_kematian/verifikasi_lurah',$data);
    }

     function surat_kematian(){
        $data=$this->lu->surat_kematian();
		echo json_encode($data);
    }

     public function verifikasi_surat_kematian()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_kematian();
		
		echo "<script>alert('Surat Kematian berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtkematian');
			</script>";
	   } else {
		 	if(isset($_POST['id_ma'])){
				$data['verifikasi_kematian'] = $this->lu->surat_kematian();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_kematian/verifikasi_surat_kematian', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtkematian'));
		 }
	   }  
   }

    public function srttdkmampu(){
        $data['lurah'] = $this->lu->tidak_mampu();
        $this->template->load('template','lurah/surat_tidak_mampu/verifikasi_lurah',$data);
    }

    function surat_tidak_mampu(){
        $data=$this->lu->surat_tidak_mampu();
		echo json_encode($data);
    }

     public function verifikasi_surat_tidak_mampu()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_tidak_mampu();
		
		echo "<script>alert('Surat Tidak Mampu berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srttdkmampu');
			</script>";
	   } else {
		 	if(isset($_POST['id_tm'])){
				$data['verifikasi_tidak_mampu'] = $this->lu->surat_tidak_mampu();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_tidak_mampu/verifikasi_surat_tidak_mampu', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srttdkmampu'));
		 }
	   }  
   }

    public function srtpenghasilan(){
        $data['lurah'] = $this->lu->penghasilan();
        $this->template->load('template','lurah/surat_penghasilan/verifikasi_lurah',$data);
    }

    function surat_penghasilan(){
        $data=$this->lu->surat_penghasilan();
		echo json_encode($data);
    }

     public function verifikasi_surat_penghasilan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_penghasilan();
		
		echo "<script>alert('Surat Penghasilan berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtpenghasilan');
			</script>";
	   } else {
		 	if(isset($_POST['id_pe'])){
				$data['verifikasi_penghasilan'] = $this->lu->surat_penghasilan();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_penghasilan/verifikasi_surat_penghasilan', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtpenghasilan'));
		 }
	   }  
   }

    public function srttdkpenghasilan(){
        $data['lurah'] = $this->lu->tidak_penghasilan();
        $this->template->load('template','lurah/surat_tidak_penghasilan/verifikasi_lurah',$data);
    }

    function surat_tidak_penghasilan(){
        $data=$this->lu->surat_tidak_penghasilan();
		echo json_encode($data);
    }

     public function verifikasi_surat_tidak_penghasilan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_tidak_penghasilan();
		
		echo "<script>alert('Surat Tidak Penghasilan berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srttdkpenghasilan');
			</script>";
	   } else {
		 	if(isset($_POST['id_tp'])){
				$data['verifikasi_tidak_penghasilan'] = $this->lu->surat_tidak_penghasilan();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_tidak_penghasilan/verifikasi_surat_tidak_penghasilan', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srttdkpenghasilan'));
		 }
	   }  
   }

    public function srtskck(){
        $data['lurah'] = $this->lu->skck();
        $this->template->load('template','lurah/surat_skck/verifikasi_lurah',$data);
    }

    function surat_skck(){
        $data=$this->lu->surat_skck();
		echo json_encode($data);
    }

     public function verifikasi_surat_skck()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_skck();
		
		echo "<script>alert('Surat SKCK berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtskck');
			</script>";
	   } else {
		 	if(isset($_POST['id_sk'])){
				$data['verifikasi_skck'] = $this->lu->surat_skck();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_skck/verifikasi_surat_skck', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtskck'));
		 }
	   }  
   }

    public function srtcerai(){
        $data['lurah'] = $this->lu->cerai();
        $this->template->load('template','lurah/surat_cerai/verifikasi_lurah',$data);
    }

    function surat_cerai(){
        $data=$this->lu->surat_cerai();
		echo json_encode($data);
    }

     public function verifikasi_surat_cerai()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_cerai();
		
		echo "<script>alert('Surat Cerai berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtcerai');
			</script>";
	   } else {
		 	if(isset($_POST['id_ce'])){
				$data['verifikasi_cerai'] = $this->lu->surat_cerai();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_cerai/verifikasi_surat_cerai', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtcerai'));
		 }
	   }  
   }

    public function srtaktanikah(){
        $data['lurah'] = $this->lu->akta_nikah();
        $this->template->load('template','lurah/surat_akta_nikah/verifikasi_lurah',$data);
    }

    function surat_akta_nikah(){
        $data=$this->lu->surat_akta_nikah();
		echo json_encode($data);
    }

     public function verifikasi_surat_akta_nikah()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_akta_nikah();
		
		echo "<script>alert('Surat Akta Nikah berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtaktanikah');
			</script>";
	   } else {
		 	if(isset($_POST['id_an'])){
				$data['verifikasi_akta_nikah'] = $this->lu->surat_akta_nikah();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_akta_nikah/verifikasi_surat_akta_nikah', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtaktanikah'));
		 }
	   }  
   }

    public function srtskbd(){
        $data['lurah'] = $this->lu->skbd();
        $this->template->load('template','lurah/surat_skbd/verifikasi_lurah',$data);
    }

    function surat_skbd(){
        $data=$this->lu->surat_skbd();
		echo json_encode($data);
    }

     public function verifikasi_surat_skbd()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_skbd();
		
		echo "<script>alert('Surat SKBD berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtskbd');
			</script>";
	   } else {
		 	if(isset($_POST['id_skbd'])){
				$data['verifikasi_skbd'] = $this->lu->surat_skbd();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_skbd/verifikasi_surat_skbd', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtskbd'));
		 }
	   }  
   }

    public function srtketerangan(){
        $data['lurah'] = $this->lu->keterangan();
        $this->template->load('template','lurah/surat_keterangan/verifikasi_lurah',$data);
    }

    function surat_keterangan(){
        $data=$this->lu->surat_keterangan();
		echo json_encode($data);
    }

     public function verifikasi_surat_keterangan()
   {
	   if(isset($_POST['edit'])) 
	   {
		$this->lu->verifikasi_surat_keterangan();
		
		echo "<script>alert('Surat Keterangan berhasil di Verifikasi');
			 window.location.replace('../Verifikasi_lurah/srtketerangan');
			</script>";
	   } else {
		 	if(isset($_POST['id_ke'])){
				$data['verifikasi_keterangan'] = $this->lu->surat_keterangan();
				$data['kecamatan'] = $this->lu->kecamatan();
				$data['kelurahan'] = $this->lu->get_kelurahan();
				$data['pekerjaan'] = $this->lu->pekerjaan();
				$data['status'] = $this->lu->status_perkawinan();
				$data['agama'] = $this->lu->agama();
				$this->template->load('template', 'lurah/surat_keterangan/verifikasi_surat_keterangan', $data);
		 } else {
			 redirect(base_url('Verifikasi_lurah/srtketerangan'));
		 }
	   }  
   }

   public function srtusaha(){
	$data['lurah'] = $this->lu->usaha();
	$this->template->load('template','lurah/surat_usaha/verifikasi_lurah',$data);
}

   function surat_usaha(){
	$data=$this->lu->surat_usaha();
	echo json_encode($data);
}

 public function verifikasi_surat_usaha()
{
   if(isset($_POST['edit'])) 
   {
	$this->lu->verifikasi_surat_usaha();
	
	echo "<script>alert('Surat Usaha berhasil di Verifikasi');
		 window.location.replace('../Verifikasi_lurah/srtusaha');
		</script>";
   } else {
		 if(isset($_POST['id_us'])){
			$data['verifikasi_usaha'] = $this->lu->surat_usaha();
			$data['kecamatan'] = $this->lu->kecamatan();
			$data['kelurahan'] = $this->lu->get_kelurahan();
			$data['pekerjaan'] = $this->lu->pekerjaan();
			$data['status'] = $this->lu->status_perkawinan();
			$data['agama'] = $this->lu->agama();
			$this->template->load('template', 'lurah/surat_usaha/verifikasi_surat_usaha', $data);
	 } else {
		 redirect(base_url('Verifikasi_lurah/srtusaha'));
	 }
   }  
}

public function srtdomisili(){
	$data['domisili'] = $this->lu->domisili();
	$this->template->load('template','lurah/surat_domisili/verifikasi_lurah',$data);
}

   function surat_domisili(){
	$data=$this->lu->surat_domisili();
	echo json_encode($data);
}

 public function verifikasi_surat_domisili()
{
   if(isset($_POST['edit'])) 
   {
	$this->lu->verifikasi_surat_domisili();
	
	echo "<script>alert('Surat Domisili berhasil di Verifikasi');
		 window.location.replace('../Verifikasi_lurah/srtdomisili');
		</script>";
   } else {
		 if(isset($_POST['id_do'])){
			$data['verifikasi_domisili'] = $this->lu->surat_domisili();
			$data['kecamatan'] = $this->lu->kecamatan();
			$data['kelurahan'] = $this->lu->get_kelurahan();
			$data['pekerjaan'] = $this->lu->pekerjaan();
			$data['status'] = $this->lu->status_perkawinan();
			$data['agama'] = $this->lu->agama();
			$this->template->load('template', 'lurah/surat_domisili/verifikasi_surat_domisili', $data);
	 } else {
		 redirect(base_url('Verifikasi_lurah/srtdomisili'));
	 }
   }  
}

public function srtsengketa(){
	$data['lurah'] = $this->lu->sengketa();
	$this->template->load('template','lurah/surat_sengketa/verifikasi_lurah',$data);
}

   function surat_sengketa(){
	$data=$this->lu->surat_sengketa();
	echo json_encode($data);
}

 public function verifikasi_surat_sengketa()
{
   if(isset($_POST['edit'])) 
   {
	$this->lu->verifikasi_surat_sengketa();
	
	echo "<script>alert('Surat Sengketa berhasil di Verifikasi');
		 window.location.replace('../Verifikasi_lurah/srtsengketa');
		</script>";
   } else {
		 if(isset($_POST['id_se'])){
			$data['verifikasi_sengketa'] = $this->lu->surat_sengketa();
			$data['kecamatan'] = $this->lu->kecamatan();
			$data['kelurahan'] = $this->lu->get_kelurahan();
			$data['pekerjaan'] = $this->lu->pekerjaan();
			$data['status'] = $this->lu->status_perkawinan();
			$data['agama'] = $this->lu->agama();
			$this->template->load('template', 'lurah/surat_sengketa/verifikasi_surat_sengketa', $data);
	 } else {
		 redirect(base_url('Verifikasi_lurah/srtsengketa'));
	 }
   }  
}


public function srtortu(){
	$data['lurah'] = $this->lu->ortu();
	$this->template->load('template','lurah/surat_ortu/verifikasi_lurah',$data);
}

   function surat_ortu(){
	$data=$this->lu->surat_ortu();
	echo json_encode($data);
}

 public function verifikasi_surat_ortu()
{
   if(isset($_POST['edit'])) 
   {
	$this->lu->verifikasi_surat_ortu();
	
	echo "<script>alert('Surat Izin Orang Tua berhasil di Verifikasi');
		 window.location.replace('../Verifikasi_lurah/srtortu');
		</script>";
   } else {
		 if(isset($_POST['id_or'])){
			$data['verifikasi_ortu'] = $this->lu->surat_ortu();
			$data['kecamatan'] = $this->lu->kecamatan();
			$data['kelurahan'] = $this->lu->get_kelurahan();
			$data['pekerjaan'] = $this->lu->pekerjaan();
			$data['status'] = $this->lu->status_perkawinan();
			$data['agama'] = $this->lu->agama();
			$this->template->load('template', 'lurah/surat_ortu/verifikasi_surat_ortu', $data);
	 } else {
		 redirect(base_url('Verifikasi_lurah/srtortu'));
	 }
   }  
}

}
