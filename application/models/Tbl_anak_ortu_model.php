<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_anak_ortu_model extends CI_Model
{	

	public function pekerjaan()
    {
        $this->db->select('*');
		$this->db->from('mstpekerjaan');
		$query=$this->db->get();
		return $query->result();
    }

	public function agama(){
		$this->db->select('*');
		$this->db->from('mstagama');
		$query=$this->db->get();
		return $query->result();
	}

	public function simpan_anak(){

		$data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'warga_negara' => $this->input->post('warga_negara'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			);
            
        $query = $this->db->insert('anak_ortu', $data);
	}
	

	function update_anak(){
		$data = array(
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'warga_negara' => $this->input->post('warga_negara'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			);
		$id=$this->input->post('id_anak');
        $this->db->where('id_anak', $id);
        $this->db->update('anak_ortu',$data);
	}

	public function anak(){
		$this->db->select('*');
		$this->db->from('anak_ortu');
		$query= $this->db->get();
		return $query->result();
	}
		
	

	

}
?>