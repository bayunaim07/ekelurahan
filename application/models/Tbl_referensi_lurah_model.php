<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_referensi_lurah_model extends CI_Model
{

public function referensi_lurah()
    {
        $this->db->select('*');
		$this->db->from('referensi_lurah');
		$this->db->join('kecamatan','referensi_lurah.id_kec=kecamatan.id_kec');
        $this->db->join('kelurahan','referensi_lurah.id_kel=kelurahan.id_kel');
		$query=$this->db->get();
		return $query->result();	
    }

	function kecamatan()
    {
        $this->db->order_by('id_kec', 'ASC');
        return $this->db->from('kecamatan')->get()->result();
    }
 
	function kelurahan()
    {
        $this->db->order_by('id_kel', 'ASC');
        return $this->db->from('kelurahan')->get()->result();
    }

	public function get_join_kec()
	{
	 	$id=$this->input->post('id_lurah');
		$this->db->select('*');
		$this->db->join('kecamatan','referensi_lurah.id_kec=kecamatan.id_kec');
		$this->db->from('referensi_lurah');
		$this->db->where('id_lurah', $id);
		$query= $this->db->get();
        return $query->result();	
	}	
    public function get_join_kel()
	{
	 	$id=$this->input->post('id_lurah');
		$this->db->select('*');
		$this->db->join('kelurahan','referensi_lurah.id_kel=kelurahan.id_kel');
		$this->db->from('referensi_lurah');
		$this->db->where('id_lurah', $id);
		$query= $this->db->get();
        return $query->result();	
	}


Public function simpan() 

{
			$foto = $this->upload_foto();
			$data = array(
				'nip'     		=> $this->input->post('nip'),
				'nama_lurah'    => $this->input->post('nama_lurah'),
				'id_kec'     	=> $this->input->post('id_kec'),
				'id_kel'     	=> $this->input->post('id_kel'),
				'kode_lurah'	=> $this->input->post('kode_lurah'),
		  		'kop'        	=> $foto['file_name']
			);
   	
	$this->db->insert('referensi_lurah', $data);
} 



Public function update() 
	{
			
	$foto = $this->upload_foto();
   if($foto['file_name']==''){
	$data = array(
		'nip'     		=> $this->input->post('nip'),
		'nama_lurah'    => $this->input->post('nama_lurah'),
		'id_kec'     	=> $this->input->post('id_kec'),
		'id_kel'     	=> $this->input->post('id_kel'),
		'kode_lurah'	=> $this->input->post('kode_lurah')
   
	   
   );     
   } else {	
   $data = array(
	'nip'     		=> $this->input->post('nip'),
	'nama_lurah'    => $this->input->post('nama_lurah'),
	'id_kec'     	=> $this->input->post('id_kec'),
	'id_kel'     	=> $this->input->post('id_kel'),
	'kode_lurah'	=> $this->input->post('kode_lurah'),
    'kop'        	=> $foto['file_name']
   );     
   }
            
			$id=$this->input->post('id_lurah');
            $this->db->where('id_lurah', $id);
			$this->db->update('referensi_lurah', $data);
		
    }	


public function	hapus()
	{
		$id=$this->input->post('id_lurah');
		$this->db->where('id_lurah', $id);
		
		$this->db->delete('referensi_lurah');
	}		
	
	public function upload_foto()
	{
        $config['upload_path']          = './assets/assets/images/kop_surat';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 10000;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
        $this->load->library('upload', $config);
        $this->upload->do_upload('kop');
        return $this->upload->data();
    }			
	

	

}
?>