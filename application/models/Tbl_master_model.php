<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_master_model extends CI_Model
{

public function master_surat()
    {
        $this->db->select('*');
		$this->db->from('master_surat');
		$query=$this->db->get();
		return $query->result();
    }

function kecamatan()
    {
        $this->db->order_by('id_kec', 'ASC');
        return $this->db->from('kecamatan')->get()->result();
    }
 
 function kelurahan($id_kec)
    {
        $this->db->where('id_kec', $id_kec);
       
        return $this->db->from('kelurahan')->get()->result();
    }


Public function simpan() 
{
		$kode=$this->input->post('kode');
		
		$this->db->select('*');
		$this->db->from('master_surat');
		$this->db->where('kode',$kode);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('Kode = ".$kode." Surat Sudah Pernah diinput');
				 window.location.replace('../Master_surat');
				</script>";	
	}else{
		
			
             $data = array(
			'kode'     	 => $this->input->post('kode'),
			'nama_surat' => $this->input->post('nama_surat')
			
			
			
			);
            
                
                
        
        $this->db->insert('master_surat', $data);
	}
  } 



Public function update() 
	{
			
             $data = array(
			'kode'     	 => $this->input->post('kode'),
			'nama_surat' => $this->input->post('nama_surat')
			
		
			
			
			);
            
			$id=$this->input->post('id');
            $this->db->where('id', $id);
			$this->db->update('master_surat', $data);
		
    }	


public function	hapus()
	{
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		
		$this->db->delete('master_surat');
	}		
	
	

}
?>