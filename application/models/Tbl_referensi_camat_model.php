<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_referensi_camat_model extends CI_Model
{

public function referensi_camat()
    {
        $this->db->select('*');
		$this->db->from('referensi_camat');
		$this->db->join('kecamatan','referensi_camat.id_kec=kecamatan.id_kec');
        $this->db->join('kelurahan','referensi_camat.id_kel=kelurahan.id_kel');
		$query=$this->db->get();
		return $query->result();	
    }

	function kecamatan()
    {
        $this->db->order_by('id_kec', 'ASC');
        return $this->db->from('kecamatan')->get()->result();
    }
 
	function kelurahan()
    {
        $this->db->order_by('id_kel', 'ASC');
        return $this->db->from('kelurahan')->get()->result();
    }

	public function get_join_kec()
	{
	 	$id=$this->input->post('id_camat');
		$this->db->select('*');
		$this->db->join('kecamatan','referensi_camat.id_kec=kecamatan.id_kec');
		$this->db->from('referensi_camat');
		$this->db->where('id_camat', $id);
		$query= $this->db->get();
        return $query->result();	
	}	
    public function get_join_kel()
	{
	 	$id=$this->input->post('id_camat');
		$this->db->select('*');
		$this->db->join('kelurahan','referensi_camat.id_kel=kelurahan.id_kel');
		$this->db->from('referensi_camat');
		$this->db->where('id_camat', $id);
		$query= $this->db->get();
        return $query->result();	
	}


Public function simpan() 

{
			
			$data = array(
				'nip_camat'     		=> $this->input->post('nip_camat'),
				'nama_camat'    => $this->input->post('nama_camat'),
				'id_kec'     	=> $this->input->post('id_kec'),
				'id_kel'     	=> $this->input->post('id_kel'),
				'pangkat_camat'		=> $this->input->post('pangkat_camat'),
				'golongan_camat'		=> $this->input->post('golongan_camat'),
				'jabatan_camat'		=> $this->input->post('jabatan_camat'),
			);
   	
	$this->db->insert('referensi_camat', $data);
} 



Public function update() 
	{
			
	$foto = $this->upload_foto();
   if($foto['file_name']==''){
	$data = array(
		'nip_camat'     		=> $this->input->post('nip_camat'),
		'nama_camat'    => $this->input->post('nama_camat'),
		'id_kec'     	=> $this->input->post('id_kec'),
		'id_kel'     	=> $this->input->post('id_kel'),
		'pangkat_camat'		=> $this->input->post('pangkat_camat'),
		'golongan_camat'		=> $this->input->post('golongan_camat'),
		'jabatan_camat'		=> $this->input->post('jabatan_camat')
   
	   
   );     
   } else {	
   $data = array(
	'nip_camat'     		=> $this->input->post('nip_camat'),
	'nama_camat'    => $this->input->post('nama_camat'),
	'id_kec'     	=> $this->input->post('id_kec'),
	'id_kel'     	=> $this->input->post('id_kel'),
	'pangkat_camat'		=> $this->input->post('pangkat_camat'),
	'golongan_camat'		=> $this->input->post('golongan_camat'),
	'jabatan_camat'		=> $this->input->post('jabatan_camat'),
	   );     
   }
            
			$id=$this->input->post('id_camat');
            $this->db->where('id_camat', $id);
			$this->db->update('referensi_camat', $data);
		
    }	


public function	hapus()
	{
		$id=$this->input->post('id_camat');
		$this->db->where('id_camat', $id);
		
		$this->db->delete('referensi_camat');
	}		
	
	public function upload_foto()
	{
        $config['upload_path']          = './assets/assets/images/kop_surat';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 10000;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
        $this->load->library('upload', $config);
        $this->upload->do_upload('kop');
        return $this->upload->data();
    }			
	

	

}
?>