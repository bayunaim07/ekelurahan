<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_surat_model extends CI_Model
{

public function master_surat()
    {
        $this->db->select('*');
		$this->db->from('master_surat');
		$this->db->where('aktif_surat',1);
		$query=$this->db->get();
		return $query->result();
    }
public function pekerjaan()
    {
        $this->db->select('*');
		$this->db->from('mstpekerjaan');
		$query=$this->db->get();
		return $query->result();
    }
public function kecamatan()
    {
        $this->db->select('*');
		$this->db->from('kecamatan');
		$query=$this->db->get();
		return $query->result();
    }
	
	public function status_perkawinan(){
		$this->db->select('*');
		$this->db->from('status_perkawinan');
		$query=$this->db->get();
		return $query->result();
	}

	public function agama(){
		$this->db->select('*');
		$this->db->from('mstagama');
		$query=$this->db->get();
		return $query->result();
	}

	function get_kelurahan()
    {
        $this->db->select('*');
		$this->db->from('kelurahan');
		$query=$this->db->get();
		return $query->result();
    }

 function kelurahan($id_kec)
    {
       $this->db->where('id_kec', $id_kec);
        return $this->db->from('kelurahan')->get()->result();
    }

public function srt_nikah()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtnikah.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtnikah.agama=mstagama.id');
		$this->db->from('srtnikah');
		$this->db->where('srtnikah.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	function profil(){
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->from('profil');
		$this->db->join('mstagama','profil.agama=mstagama.id');
		$this->db->where('profil.id_users',$id);
		$query=$this->db->get();
		return $query->result();
	}

Public function simpan_srtnikah() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtnikah');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtnikah');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto1();
		$foto_ktp = $this->upload_foto();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'id_surat'=> 1,
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'warga_negara' => $this->input->post('warga_negara'),
			'jenis_daftar' => 1,
			'tgldaftar' => date("Y-m-d"),
			'jam_daftar' => date('H:i:s'),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'pasangan_terdahulu'=> $this->input->post('pasangan_terdahulu'),
			'jenkel' => $this->input->post('jenkel'),
			'nama_ayah' => $this->input->post('nama_ayah'),
			'nik_ayah' => $this->input->post('nik_ayah'),
			'bangsa_ayah' => $this->input->post('bangsa_ayah'),
			'alamat_ayah' => $this->input->post('alamat_ayah'),
			'tempat_ayah' => $this->input->post('tempat_ayah'),
			'tgllahir_ayah' => $this->input->post('tgllahir_ayah'),
			'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
			'agama_ayah' => $this->input->post('agama_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'nik_ibu'=> $this->input->post('nik_ibu'),
			'bangsa_ibu' => $this->input->post('bangsa_ibu'),
			'alamat_ibu' => $this->input->post('alamat_ibu'),
			'tempat_ibu' => $this->input->post('tempat_ibu'),
			'tgllahir_ibu' => $this->input->post('tgllahir_ibu'),
			'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
			'agama_ibu' => $this->input->post('agama_ibu'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtnikah', $data);
		
	}
  } 
 

  function update_surat_nikah(){
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'warga_negara' => $this->input->post('warga_negara'),
			'nama_ayah' => $this->input->post('nama_ayah'),
			'nik_ayah' => $this->input->post('nik_ayah'),
			'bangsa_ayah' => $this->input->post('bangsa_ayah'),
			'alamat_ayah' => $this->input->post('alamat_ayah'),
			'tempat_ayah' => $this->input->post('tempat_ayah'),
			'tgllahir_ayah' => $this->input->post('tgllahir_ayah'),
			'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'nik_ibu' => $this->input->post('nik_ibu'),
			'bangsa_ibu' => $this->input->post('bangsa_ibu'),
			'alamat_ibu' => $this->input->post('alamat_ibu'),
			'tempat_ibu' => $this->input->post('tempat_ibu'),
			'tgllahir_ibu' => $this->input->post('tgllahir_ibu'),
			'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			
        );
        $id=$this->input->post('id_sn');
        $this->db->where('id_sn', $id);
        $this->db->update('srtnikah',$data);
  }

  function hapus_nikah(){
	 	$id=$this->input->post('id_sn');
        $this->db->where('id_sn', $id);
		$this->db->delete('srtnikah');
  }

  function kirim_nikah(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_sn');
        $this->db->where('id_sn', $id);
        $this->db->update('srtnikah',$data);
  }

  function kirim_ulang_nikah(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_sn');
        $this->db->where('id_sn', $id);
        $this->db->update('srtnikah',$data);
  }

  function upload_foto1(){
       	$config['upload_path']          = './images/surat_nikah';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto(){
       	$config['upload_path']          = './images/surat_nikah';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

  public function srt_nikah_blm()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtblmnikah.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtblmnikah.agama=mstagama.id');
		$this->db->from('srtblmnikah');
		$this->db->where('srtblmnikah.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	
  Public function simpan_srtblmnikah() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtblmnikah');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtblmnikah');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_blm_nikah1();
		$foto_ktp = $this->upload_foto_blm_nikah();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'id_surat' => 2,
			'kk' => $this->input->post('kk'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'peruntukan' => $this->input->post('peruntukan'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtblmnikah', $data);
		
	}
  }

	function update_surat_belum_nikah(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'kk'		=> $this->input->post('kk'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'peruntukan' => $this->input->post('peruntukan'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_bn');
        $this->db->where('id_bn', $id);
        $this->db->update('srtblmnikah',$data);
  }

  function upload_foto_blm_nikah(){
       	$config['upload_path']          = './images/surat_blm_nikah';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_blm_nikah1(){
       	$config['upload_path']          = './images/surat_blm_nikah';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

  function hapus_blm_nikah(){
	 	$id=$this->input->post('id_bn');
        $this->db->where('id_bn', $id);
		$this->db->delete('srtblmnikah');
  }

  function kirim_blm_nikah(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_bn');
        $this->db->where('id_bn', $id);
        $this->db->update('srtblmnikah',$data);
  }

  function kirim_ulang_blm_nikah(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_bn');
        $this->db->where('id_bn', $id);
        $this->db->update('srtblmnikah',$data);
  }

   Public function simpan_srtwaris() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtwaris');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtwaris');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_waris();
		$foto_ktp = $this->upload_foto_waris1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'id_surat' => 3,
			'nama' => $this->input->post('nama'),
			'tempat_kematian' => $this->input->post('tempat_kematian'),
			'tglmati_ayah' => $this->input->post('tglmati_ayah'),
			'jlhanak' => $this->input->post('jlhanak'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'nik_ibu' => $this->input->post('nik_ibu'),
			'tglmati_ibu' => $this->input->post('tglmati_ibu'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtwaris', $data);
		
	}
  } 

	function upload_foto_waris(){
       	$config['upload_path']          = './images/surat_Waris';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_waris1(){
       	$config['upload_path']          = './images/surat_Waris';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_waris()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->from('srtwaris');
		$this->db->where('srtwaris.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	function update_surat_waris(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tglmati_ayah' => $this->input->post('tglmati_ayah'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'nik_ibu' => $this->input->post('nik_ibu'),
			'tglmati_ibu' => $this->input->post('tglmati_ibu'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_wa');
        $this->db->where('id_wa', $id);
        $this->db->update('srtwaris',$data);
  }

   function hapus_waris(){
	 	$id=$this->input->post('id_wa');
        $this->db->where('id_wa', $id);
		$this->db->delete('srtwaris');
  }

  function kirim_waris(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_wa');
        $this->db->where('id_wa', $id);
        $this->db->update('srtwaris',$data);
  }

  function kirim_ulang_waris(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_wa');
        $this->db->where('id_wa', $id);
        $this->db->update('srtwaris',$data);
  }

  Public function simpan_srtkematian() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtkematian');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtkematian');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_kematian();
		$foto_ktp = $this->upload_foto_kematian1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'kk'     	 => $this->input->post('kk'),
			'id_surat' => 4,
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'warga_negara' => $this->input->post('warga_negara'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'tglmati'=> $this->input->post('tglmati'),
			'tempatmati'=> $this->input->post('tempatmati'),
			'sebab'=> $this->input->post('sebab'),
			'namaket'=> $this->input->post('namaket'),
			'visum'=> $this->input->post('visum'),
			'pelapor'=> $this->input->post('pelapor'),
			'hubungan' => $this->input->post('hubungan'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtkematian', $data);
		
	}
  } 

	function upload_foto_kematian(){
       	$config['upload_path']          = './images/surat_kematian';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_kematian1(){
       	$config['upload_path']          = './images/surat_kematian';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_kematian()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtkematian.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtkematian.agama=mstagama.id');
		$this->db->from('srtkematian');
		$this->db->where('srtkematian.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_kematian(){
	 	$id=$this->input->post('id_ma');
        $this->db->where('id_ma', $id);
		$this->db->delete('srtkematian');
  }

  function kirim_kematian(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_ma');
        $this->db->where('id_ma', $id);
        $this->db->update('srtkematian',$data);
  }

  function update_surat_kematian(){
		
        $data = array(
			'nik'     	 => $this->input->post('nik'),
			'kk'     	 => $this->input->post('kk'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'warga_negara' => $this->input->post('warga_negara'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'tglmati'=> $this->input->post('tglmati'),
			'tempatmati'=> $this->input->post('tempatmati'),
			'sebab'=> $this->input->post('sebab'),
			'namaket'=> $this->input->post('namaket'),
			'visum'=> $this->input->post('visum'),
			'pelapor'=> $this->input->post('pelapor'),
			'hubungan' => $this->input->post('hubungan'),
        );
        $id=$this->input->post('id_ma');
        $this->db->where('id_ma', $id);
        $this->db->update('srtkematian',$data);
  }

  function kirim_ulang_kematian(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_ma');
        $this->db->where('id_ma', $id);
        $this->db->update('srtkematian',$data);
  }

  Public function simpan_srttidakmampu() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srttdkmampu');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srttdkmampu');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_tidak_mampu();
		$foto_ktp = $this->upload_foto_tidak_mampu1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'id_surat' => 6,
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srttdkmampu', $data);
		
	}
  } 

	function upload_foto_tidak_mampu(){
       	$config['upload_path']          = './images/surat_tidak_mampu';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_tidak_mampu1(){
       	$config['upload_path']          = './images/surat_tidak_mampu';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_tidak_mampu()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srttdkmampu.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srttdkmampu.agama=mstagama.id');
		$this->db->from('srttdkmampu');
		$this->db->where('srttdkmampu.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_tidak_mampu(){
	 	$id=$this->input->post('id_tm');
        $this->db->where('id_tm', $id);
		$this->db->delete('srttdkmampu');
  }

  function kirim_tidak_mampu(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_tm');
        $this->db->where('id_tm', $id);
        $this->db->update('srttdkmampu',$data);
  }

  function update_surat_tidak_mampu(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_tm');
        $this->db->where('id_tm', $id);
        $this->db->update('srttdkmampu',$data);
  }

  function kirim_ulang_tidak_mampu(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_tm');
        $this->db->where('id_tm', $id);
        $this->db->update('srttdkmampu',$data);
  }

  Public function simpan_srtpenghasilan() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtpenghasilan');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtpenghasilan');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_penghasilan();
		$foto_ktp = $this->upload_foto_penghasilan1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtpenghasilan', $data);
		
	}
  } 

	function upload_foto_penghasilan(){
       	$config['upload_path']          = './images/surat_penghasilan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_penghasilan1(){
       	$config['upload_path']          = './images/surat_penghasilan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_penghasilan()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtpenghasilan.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtpenghasilan.agama=mstagama.id');
		$this->db->from('srtpenghasilan');
		$this->db->where('srtpenghasilan.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_penghasilan(){
	 	$id=$this->input->post('id_pe');
        $this->db->where('id_pe', $id);
		$this->db->delete('srtpenghasilan');
  }

  function kirim_penghasilan(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_pe');
        $this->db->where('id_pe', $id);
        $this->db->update('srtpenghasilan',$data);
  }

  function update_surat_penghasilan(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_pe');
        $this->db->where('id_pe', $id);
        $this->db->update('srtpenghasilan',$data);
  }

  function kirim_ulang_penghasilan(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_pe');
        $this->db->where('id_pe', $id);
        $this->db->update('srtpenghasilan',$data);
  }

  Public function simpan_srttdkpenghasilan() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srttdkpenghasilan');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srttdkpenghasilan');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_tidak_penghasilan();
		$foto_ktp = $this->upload_foto_tidak_penghasilan1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srttdkpenghasilan', $data);
		
	}
  } 

	function upload_foto_tidak_penghasilan(){
       	$config['upload_path']          = './images/surat_tidak_penghasilan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_tidak_penghasilan1(){
       	$config['upload_path']          = './images/surat_tidak_penghasilan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_tidak_penghasilan()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srttdkpenghasilan.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srttdkpenghasilan.agama=mstagama.id');
		$this->db->from('srttdkpenghasilan');
		$this->db->where('srttdkpenghasilan.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_tidak_penghasilan(){
	 	$id=$this->input->post('id_tp');
        $this->db->where('id_tp', $id);
		$this->db->delete('srttdkpenghasilan');
  }

  function kirim_tidak_penghasilan(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_tp');
        $this->db->where('id_tp', $id);
        $this->db->update('srttdkpenghasilan',$data);
  }

  function update_surat_tidak_penghasilan(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_tp');
        $this->db->where('id_tp', $id);
        $this->db->update('srttdkpenghasilan',$data);
  }

  function kirim_ulang_tidak_penghasilan(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_tp');
        $this->db->where('id_tp', $id);
        $this->db->update('srttdkpenghasilan',$data);
  }

  Public function simpan_srtskck() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtskck');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtskck');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_skck();
		$foto_ktp = $this->upload_foto_skck1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'kk' => $this->input->post('kk'),
			'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'id_surat' => 9,
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'peruntukan' => $this->input->post('peruntukan'),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtskck', $data);
		
	}
  } 

	function upload_foto_skck(){
       	$config['upload_path']          = './images/surat_skck';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_skck1(){
       	$config['upload_path']          = './images/surat_skck';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_skck()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtskck.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtskck.agama=mstagama.id');
		$this->db->from('srtskck');
		$this->db->where('srtskck.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_skck(){
	 	$id=$this->input->post('id_sk');
        $this->db->where('id_sk', $id);
		$this->db->delete('srtskck');
  }

  function kirim_skck(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_sk');
        $this->db->where('id_sk', $id);
        $this->db->update('srtskck',$data);
  }

  function update_surat_skck(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'peruntukan' => $this->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_sk');
        $this->db->where('id_sk', $id);
        $this->db->update('srtskck',$data);
  }

  function kirim_ulang_skck(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_sk');
        $this->db->where('id_sk', $id);
        $this->db->update('srtskck',$data);
  }

  Public function simpan_srtcerai() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtcerai');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtcerai');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_cerai();
		$foto_ktp = $this->upload_foto_cerai1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtcerai', $data);
		
	}
  } 

	function upload_foto_cerai(){
       	$config['upload_path']          = './images/surat_cerai';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_cerai1(){
       	$config['upload_path']          = './images/surat_cerai';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_cerai()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtcerai.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtcerai.agama=mstagama.id');
		$this->db->from('srtcerai');
		$this->db->where('srtcerai.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_cerai(){
	 	$id=$this->input->post('id_ce');
        $this->db->where('id_ce', $id);
		$this->db->delete('srtcerai');
  }

  function kirim_cerai(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_ce');
        $this->db->where('id_ce', $id);
        $this->db->update('srtcerai',$data);
  }

  function update_surat_cerai(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_ce');
        $this->db->where('id_ce', $id);
        $this->db->update('srtcerai',$data);
  }

  function kirim_ulang_cerai(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_ce');
        $this->db->where('id_ce', $id);
        $this->db->update('srtcerai',$data);
  }

  Public function simpan_srtaktanikah() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtaktanikah');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtaktanikah');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_akta_nikah();
		$foto_ktp = $this->upload_foto_akta_nikah1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtaktanikah', $data);
		
	}
  } 

	function upload_foto_akta_nikah(){
       	$config['upload_path']          = './images/surat_akta_nikah';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_akta_nikah1(){
       	$config['upload_path']          = './images/surat_akta_nikah';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_akta_nikah()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtaktanikah.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtaktanikah.agama=mstagama.id');
		$this->db->from('srtaktanikah');
		$this->db->where('srtaktanikah.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_akta_nikah(){
	 	$id=$this->input->post('id_an');
        $this->db->where('id_an', $id);
		$this->db->delete('srtaktanikah');
  }

  function kirim_akta_nikah(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_an');
        $this->db->where('id_an', $id);
        $this->db->update('srtaktanikah',$data);
  }

  function update_surat_akta_nikah(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_an');
        $this->db->where('id_an', $id);
        $this->db->update('srtaktanikah',$data);
  }

  function kirim_ulang_akta_nikah(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_an');
        $this->db->where('id_an', $id);
        $this->db->update('srtaktanikah',$data);
  }

  Public function simpan_skbd() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtskbd');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtskbd');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_skbd();
		$foto_ktp = $this->upload_foto_skbd1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'nama_ayah' => $this->input->post('nama_ayah'),
			'alamat_ayah' => $this->input->post('alamat_ayah'),
			'tempat_ayah' => $this->input->post('tempat_ayah'),
			'tgllahir_ayah' => $this->input->post('tgllahir_ayah'),
			'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'alamat_ibu' => $this->input->post('alamat_ibu'),
			'tempat_ibu' => $this->input->post('tempat_ibu'),
			'tgllahir_ibu' => $this->input->post('tgllahir_ibu'),
			'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtskbd', $data);
		
	}
  } 

	function upload_foto_skbd(){
       	$config['upload_path']          = './images/surat_skbd';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_skbd1(){
       	$config['upload_path']          = './images/surat_skbd';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_keterangan_bersih_diri()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtskbd.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtskbd.agama=mstagama.id');
		$this->db->from('srtskbd');
		$this->db->where('srtskbd.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_skbd(){
	 	$id=$this->input->post('id_skbd');
        $this->db->where('id_skbd', $id);
		$this->db->delete('srtskbd');
  }

  function kirim_bersih_diri(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_skbd');
        $this->db->where('id_skbd', $id);
        $this->db->update('srtskbd',$data);
  }

  function update_surat_keterangan_bersih_diri(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_skbd');
        $this->db->where('id_skbd', $id);
        $this->db->update('srtskbd',$data);
  }

   function kirim_ulang_bersih_diri(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_skbd');
        $this->db->where('id_skbd', $id);
        $this->db->update('srtskbd',$data);
  }

  Public function simpan_keterangan() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtketerangan');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtketerangan');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_surat_keterangan();
		$foto_ktp = $this->upload_foto_surat_keterangan1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtketerangan', $data);
		
	}
  } 

	function upload_foto_surat_keterangan(){
       	$config['upload_path']          = './images/surat_keterangan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_surat_keterangan1(){
       	$config['upload_path']          = './images/surat_keterangan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_keterangan()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtketerangan.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtketerangan.agama=mstagama.id');
		$this->db->from('srtketerangan');
		$this->db->where('srtketerangan.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_keterangan(){
	 	$id=$this->input->post('id_ke');
        $this->db->where('id_ke', $id);
		$this->db->delete('srtketerangan');
  }

  function kirim_keterangan(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_ke');
        $this->db->where('id_ke', $id);
        $this->db->update('srtketerangan',$data);
  }

  function update_surat_keterangan(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_ke');
        $this->db->where('id_ke', $id);
        $this->db->update('srtketerangan',$data);
  }

  function kirim_ulang_keterangan(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_ke');
        $this->db->where('id_ke', $id);
        $this->db->update('srtketerangan',$data);
  }

  Public function simpan_usaha() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtusaha');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtusaha');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_surat_usaha();
		$foto_ktp = $this->upload_foto_surat_usaha1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'id_surat' => 18,
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtusaha', $data);
		
	}
  } 

	function upload_foto_surat_usaha(){
       	$config['upload_path']          = './images/surat_usaha';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_surat_usaha1(){
       	$config['upload_path']          = './images/surat_usaha';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_usaha()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtusaha.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtusaha.agama=mstagama.id');
		$this->db->from('srtusaha');
		$this->db->where('srtusaha.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_usaha(){
	 	$id=$this->input->post('id_us');
        $this->db->where('id_us', $id);
		$this->db->delete('srtusaha');
  }

  function kirim_usaha(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_us');
        $this->db->where('id_us', $id);
        $this->db->update('srtusaha',$data);
  }

  function update_surat_usaha(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_us');
        $this->db->where('id_us', $id);
        $this->db->update('srtusaha',$data);
  }

  function kirim_ulang_usaha(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_us');
        $this->db->where('id_us', $id);
        $this->db->update('srtusaha',$data);
  }

  Public function simpan_sengketa() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtsengketa');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtsengketa');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_surat_sengketa();
		$foto_ktp = $this->upload_foto_surat_sengketa1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'id_surat' => 19,
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtsengketa', $data);
		
	}
  } 

	function upload_foto_surat_sengketa(){
       	$config['upload_path']          = './images/surat_sengketa';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_surat_sengketa1(){
       	$config['upload_path']          = './images/surat_sengketa';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_sengketa()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtsengketa.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtsengketa.agama=mstagama.id');
		$this->db->from('srtsengketa');
		$this->db->where('srtsengketa.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_sengketa(){
	 	$id=$this->input->post('id_se');
        $this->db->where('id_se', $id);
		$this->db->delete('srtsengketa');
  }

  function kirim_sengketa(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_se');
        $this->db->where('id_se', $id);
        $this->db->update('srtsengketa',$data);
  }

  function update_surat_sengketa(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_se');
        $this->db->where('id_se', $id);
        $this->db->update('srtsengketa',$data);
  }

  function kirim_ulang_sengketa(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_se');
        $this->db->where('id_se', $id);
        $this->db->update('srtsengketa',$data);
  }

  Public function simpan_domisili() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtdomisili');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtdomisili');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_surat_domisili();
		$foto_ktp = $this->upload_foto_surat_domisili1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'id_surat' => 21,
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtdomisili', $data);
		
	}
  } 

	function upload_foto_surat_domisili(){
       	$config['upload_path']          = './images/surat_domisili';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_surat_domisili1(){
       	$config['upload_path']          = './images/surat_domisili';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_domisili()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtdomisili.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtdomisili.agama=mstagama.id');
		$this->db->from('srtdomisili');
		$this->db->where('srtdomisili.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	 function hapus_domisili(){
	 	$id=$this->input->post('id_do');
        $this->db->where('id_do', $id);
		$this->db->delete('srtdomisili');
  }

  function kirim_domisili(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_do');
        $this->db->where('id_do', $id);
        $this->db->update('srtdomisili',$data);
  }

  function update_surat_domisili(){
		
        $data = array(
           	'nik'     	 => $this->input->post('nik'),
			'kk' => $this->input->post('kk'),
			'peruntukan' => $this->input->post('peruntukan'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'alamat' => $this->input->post('alamat'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
        );
        $id=$this->input->post('id_do');
        $this->db->where('id_do', $id);
        $this->db->update('srtdomisili',$data);
  }

  function kirim_ulang_domisili(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_do');
        $this->db->where('id_do', $id);
        $this->db->update('srtdomisili',$data);
  }

  Public function simpan_ortu() 
{
		$nik=$this->input->post('nik');
		
		$this->db->select('*');
		$this->db->from('srtortu');
		$this->db->where('nik',$nik);
		
		$query=$this->db->get();
	if ($query->num_rows()>0){
			 echo "<script>alert('NIk = ".$nik."  Sudah Pernah diinput');
				 window.location.replace('../Surat/srtortu');
				</script>";	
	}else{
		$foto_kk = $this->upload_foto_surat_ortu();
		$foto_ktp = $this->upload_foto_surat_ortu1();

             $data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'id_surat' => 20,
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'alamat' => $this->input->post('alamat'),
			'warga_negara' => $this->input->post('warga_negara'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'nama_ayah' => $this->input->post('nama_ayah'),
			'bangsa_ayah' => $this->input->post('bangsa_ayah'),
			'agama_ayah' => $this->input->post('agama_ayah'),
			'alamat_ayah' => $this->input->post('alamat_ayah'),
			'tempat_ayah' => $this->input->post('tempat_ayah'),
			'tgllahir_ayah' => $this->input->post('tgllahir_ayah'),
			'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'bangsa_ibu' => $this->input->post('bangsa_ibu'),
			'agama_ibu' => $this->input->post('agama_ibu'),
			'alamat_ibu' => $this->input->post('alamat_ibu'),
			'tempat_ibu' => $this->input->post('tempat_ibu'),
			'tgllahir_ibu' => $this->input->post('tgllahir_ibu'),
			'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
			'nama_calon' => $this->input->post('nama_calon'),
			'bangsa_calon' => $this->input->post('bangsa_calon'),
			'agama_calon' => $this->input->post('agama_calon'),
			'alamat_calon' => $this->input->post('alamat_calon'),
			'tempat_calon' => $this->input->post('tempat_calon'),
			'tgllahir_calon' => $this->input->post('tgllahir_calon'),
			'pekerjaan_calon' => $this->input->post('pekerjaan_calon'),
			'foto_kk'      => $foto_kk['file_name'],
			'foto_ktp' =>$foto_ktp['file_name'],
			);
            
        $query = $this->db->insert('srtortu', $data);
		
	}
  } 

	function upload_foto_surat_ortu(){
       	$config['upload_path']          = './images/surat_ortu';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_kk');
        return $this->upload->data();
    }

	function upload_foto_surat_ortu1(){
       	$config['upload_path']          = './images/surat_ortu';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_ktp');
        return $this->upload->data();
    }

	public function surat_ortu()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtortu.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtortu.agama=mstagama.id');
		$this->db->from('srtortu');
		$this->db->where('srtortu.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	public function anak()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->from('anak_ortu');
		$this->db->where('anak_ortu.id_users',$id);
		$query=$this->db->get();
		return $query->result_array();
    }

	function hapus_ahli($id){
		$this->db->where('id_ahli', $id);
		$this->db->delete('ahli_waris');
	}

	public function ahli_waris()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->from('ahli_waris');
		$this->db->where('ahli_waris.id_users',$id);
		$query=$this->db->get();
		return $query->result_array();
    }

	public function anak_ortu()
    {
		$id=$this->session->userdata('id_users');	
        $this->db->select('*');
		$this->db->from('anak_ortu');
		$this->db->where('anak_ortu.id_users',$id);
		$query=$this->db->get();
		return $query->result();
    }

	function hapus_anak($id){
		$this->db->where('id_anak', $id);
		$this->db->delete('anak_ortu');
	}

	function hapus_ortu(){
	 	$id=$this->input->post('id_or');
        $this->db->where('id_or', $id);
		$this->db->delete('srtortu');
	}

  function kirim_ortu(){
	 	$data = array(
           	'cekopt' => 0
        );
        
        $id=$this->input->post('id_or');
        $this->db->where('id_or', $id);
        $this->db->update('srtortu',$data);
  }

  function update_surat_ortu(){
		
        $data = array(
			'nik'     	 => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'agama' => $this->input->post('agama'),
			'tgldaftar' => date("Y-m-d"),
			'tglsurat' => date("Y-m-d"),
			'noreg' => $this->input->post('noreg'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'alamat' => $this->input->post('alamat'),
			'warga_negara' => $this->input->post('warga_negara'),
			'pekerjaan'=> $this->input->post('pekerjaan'),
			'jenkel' => $this->input->post('jenkel'),
			'namakel' => $this->input->post('namakel'),
			'namakec' => $this->input->post('namakec'),
			'status'=> $this->input->post('status'),
			'nama_ayah' => $this->input->post('nama_ayah'),
			'bangsa_ayah' => $this->input->post('bangsa_ayah'),
			'agama_ayah' => $this->input->post('agama_ayah'),
			'alamat_ayah' => $this->input->post('alamat_ayah'),
			'tempat_ayah' => $this->input->post('tempat_ayah'),
			'tgllahir_ayah' => $this->input->post('tgllahir_ayah'),
			'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
			'nama_ibu' => $this->input->post('nama_ibu'),
			'bangsa_ibu' => $this->input->post('bangsa_ibu'),
			'agama_ibu' => $this->input->post('agama_ibu'),
			'alamat_ibu' => $this->input->post('alamat_ibu'),
			'tempat_ibu' => $this->input->post('tempat_ibu'),
			'tgllahir_ibu' => $this->input->post('tgllahir_ibu'),
			'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
			'nama_calon' => $this->input->post('nama_calon'),
			'bangsa_calon' => $this->input->post('bangsa_calon'),
			'agama_calon' => $this->input->post('agama_calon'),
			'alamat_calon' => $this->input->post('alamat_calon'),
			'tempat_calon' => $this->input->post('tempat_calon'),
			'tgllahir_calon' => $this->input->post('tgllahir_calon'),
			'pekerjaan_calon' => $this->input->post('pekerjaan_calon'),
        );
        $id=$this->input->post('id_or');
        $this->db->where('id_or', $id);
        $this->db->update('srtortu',$data);
  }

  function kirim_ulang_ortu(){
	 	$data = array(
           	'cekopt' => 0,
			'keterangan' => '-',
        );
        
        $id=$this->input->post('id_or');
        $this->db->where('id_or', $id);
        $this->db->update('srtortu',$data);
  }

  public function buat_kode()   
	{
		  $this->db->select('RIGHT(srtnikah.nosurat,4) as kode', FALSE);
		  $this->db->order_by('nosurat','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('srtnikah');         
		  if($query->num_rows() <> 0){      
		        
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		       
		   $kode = 1;    
		  }
		  $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); 
		  
		  return $kodemax;  
	}

Public function update() 
	{
			
             $data = array(
			'kode'     	 => $this->input->post('kode'),
			'nama_surat' => $this->input->post('nama_surat')
			);
            
			$id=$this->input->post('id');
            $this->db->where('id', $id);
			$this->db->update('master_surat', $data);
		
    }	


public function	hapus()
	{
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		
		$this->db->delete('master_surat');
	}	
	
	public function srt_blm_menikah()
    {
        $this->db->select('*');
		$this->db->from('srtblmnikah');
		$query=$this->db->get();
		return $query->result();
    }

	public function srt_waris()
    {
        $this->db->select('*');
		$this->db->from('srtwaris');
		$query=$this->db->get();
		return $query->result();
    }

	public function srt_kematian(){
		$this->db->select('*');
		$this->db->from('srtkematian');
		$query=$this->db->get();
		return $query->result();
	}

	public function srt_tidak_mampu(){
		$this->db->select('*');
		$this->db->from('srttdkmampu');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_penghasilan(){
		$this->db->select('*');
		$this->db->from('srtpenghasilan');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_tidak_penghasilan(){
		$this->db->select('*');
		$this->db->from('srttdkpenghasilan');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_skck(){
		$this->db->select('*');
		$this->db->from('srtskck');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_cerai(){
		$this->db->select('*');
		$this->db->from('srtcerai');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_skbd(){
		$this->db->select('*');
		$this->db->from('srtskbd');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_keterangan(){
		$this->db->select('*');
		$this->db->from('srtketerangan');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_usaha(){
		$this->db->select('*');
		$this->db->from('srtusaha');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_domisili(){
		$this->db->select('*');
		$this->db->from('srtdomisili');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_sengketa(){
		$this->db->select('*');
		$this->db->from('srtsengketa');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_ortu(){
		$this->db->select('*');
		$this->db->from('srtortu');
		$query= $this->db->get();
		return $query->result();
	}

	public function srt_akta_nikah(){
		$this->db->select('*');
		$this->db->from('srtaktanikah');
		$query=$this->db->get();
		return $query->result();
	}

	function simpan_foto_kk($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtnikah', $data);

        return true;
    }

	function simpan_foto_ktp($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtnikah', $data);

        return true;
    }

	function simpan_foto_kk_belum_nikah($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtblmnikah', $data);

        return true;
    }

	function simpan_foto_ktp_belum_nikah($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtblmnikah', $data);

        return true;
    }

	function simpan_foto_kk_waris($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtwaris', $data);

        return true;
    }

	function simpan_foto_ktp_waris($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtwaris', $data);

        return true;
    }

	function simpan_foto_kk_kematian($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtkematian', $data);

        return true;
    }

	function simpan_foto_ktp_kematian($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtkematian', $data);

        return true;
    }

	function simpan_foto_kk_tidak_mampu($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srttdkmampu', $data);

        return true;
    }

	function simpan_foto_ktp_tidak_mampu($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srttdkmampu', $data);

        return true;
    }

	function simpan_foto_kk_penghasilan($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtpenghasilan', $data);

        return true;
    }

	function simpan_foto_ktp_penghasilan($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtpenghasilan', $data);

        return true;
    }

	function simpan_foto_kk_tidak_penghasilan($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srttdkpenghasilan', $data);

        return true;
    }

	function simpan_foto_ktp_tidak_penghasilan($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srttdkpenghasilan', $data);

        return true;
    }

	function simpan_foto_kk_skck($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtskck', $data);

        return true;
    }

	function simpan_foto_ktp_skck($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtskck', $data);

        return true;
    }

	function simpan_foto_kk_cerai($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtcerai', $data);

        return true;
    }

	function simpan_foto_ktp_cerai($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtcerai', $data);

        return true;
    }

	function simpan_foto_kk_akta_nikah($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtaktanikah', $data);

        return true;
    }

	function simpan_foto_ktp_akta_nikah($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtaktanikah', $data);

        return true;
    }

	function simpan_foto_kk_skbd($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtskbd', $data);

        return true;
    }

	function simpan_foto_ktp_skbd($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtskbd', $data);

        return true;
    }

	function simpan_foto_kk_keterangan($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtketerangan', $data);

        return true;
    }

	function simpan_foto_ktp_keterangan($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtketerangan', $data);

        return true;
    }

	function simpan_foto_kk_usaha($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtusaha', $data);

        return true;
    }

	function simpan_foto_ktp_usaha($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtusaha', $data);

        return true;
    }

	function simpan_foto_kk_domisili($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtdomisili', $data);

        return true;
    }

	function simpan_foto_ktp_domisili($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtdomisili', $data);

        return true;
    }

	function simpan_foto_kk_sengketa($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtsengketa', $data);

        return true;
    }

	function simpan_foto_ktp_sengketa($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtsengketa', $data);

        return true;
    }

	function simpan_foto_kk_ortu($id_users,$image){
        $data = array(
            'foto_kk' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtortu', $data);

        return true;
    }

	function simpan_foto_ktp_ortu($id_users,$image){
        $data = array(
            'foto_ktp' => $image
        );

        $this->db->where('id_users', $id_users);
        $this->db->update('srtortu', $data);

        return true;
    }

}
?>