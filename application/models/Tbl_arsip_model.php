<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_arsip_model extends CI_Model
{
    public function surat(){
        $this->db->select('*');
        $this->db->from('master_surat');
        $this->db->where('master_surat.aktif_surat',1);
        $query = $this->db->get();
        return $query->result();
    }

    public function pekerjaan(){
        $this->db->select('*');
		$this->db->from('mstpekerjaan');
		$query=$this->db->get();
		return $query->result();
    }
    public function kecamatan(){
        $this->db->select('*');
		$this->db->from('kecamatan');
		$query=$this->db->get();
		return $query->result();
    }
	
	public function status_perkawinan(){
		$this->db->select('*');
		$this->db->from('status_perkawinan');
		$query=$this->db->get();
		return $query->result();
	}

	public function agama(){
		$this->db->select('*');
		$this->db->from('mstagama');
		$query=$this->db->get();
		return $query->result();
	}

	function get_kelurahan(){
        $this->db->select('*');
		$this->db->from('kelurahan');
		$query=$this->db->get();
		return $query->result();
    }

    function kelurahan($id_kec){
        $this->db->where('id_kec', $id_kec);
        return $this->db->from('kelurahan')->get()->result();
    }

    public function nikah(){
        $this->db->select('*');
        $this->db->from('srtnikah');
        $query = $this->db->get();
        return $query->result();
    }

    public function srt_nikah(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtnikah.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtnikah.agama=mstagama.id');
        $this->db->join('kelurahan','srtnikah.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtnikah.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtnikah.status=status_perkawinan.id');
		$this->db->from('srtnikah');
        $this->db->where('srtnikah.namakel',$kel);
        // $this->db->where('srtnikah.cekopt',);
		$query=$this->db->get();
		return $query->result();
    }

    public function belum_menikah(){
        $this->db->select('*');
        $this->db->from('srtblmnikah');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_belum_nikah(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtblmnikah.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtblmnikah.agama=mstagama.id');
        $this->db->join('kelurahan','srtblmnikah.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtblmnikah.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtblmnikah.status=status_perkawinan.id');
		$this->db->from('srtblmnikah');
        $this->db->where('srtblmnikah.namakel',$kel);
        $this->db->where('srtblmnikah.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function waris(){
        $this->db->select('*');
        $this->db->from('srtwaris');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_waris(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
        $this->db->join('kelurahan','srtwaris.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtwaris.namakec=kecamatan.id_kec');
		$this->db->from('srtwaris');
        $this->db->where('srtwaris.namakel',$kel);
        $this->db->where('srtwaris.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function mati(){
        $this->db->select('*');
        $this->db->from('srtkematian');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_kematian(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtkematian.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtkematian.agama=mstagama.id');
        $this->db->join('kelurahan','srtkematian.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtkematian.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtkematian.status=status_perkawinan.id');
		$this->db->from('srtkematian');
        $this->db->where('srtkematian.namakel',$kel);
        $this->db->where('srtkematian.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function tidak_mampu(){
        $this->db->select('*');
        $this->db->from('srttdkmampu');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_tidak_mampu(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srttdkmampu.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srttdkmampu.agama=mstagama.id');
        $this->db->join('kelurahan','srttdkmampu.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srttdkmampu.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srttdkmampu.status=status_perkawinan.id');
		$this->db->from('srttdkmampu');
        $this->db->where('srttdkmampu.namakel',$kel);
        $this->db->where('srttdkmampu.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function penghasilan(){
        $this->db->select('*');
        $this->db->from('srtpenghasilan');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_penghasilan(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtpenghasilan.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtpenghasilan.agama=mstagama.id');
        $this->db->join('kelurahan','srtpenghasilan.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtpenghasilan.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtpenghasilan.status=status_perkawinan.id');
		$this->db->from('srtpenghasilan');
        $this->db->where('srtpenghasilan.namakel',$kel);
        $this->db->where('srtpenghasilan.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function tidak_penghasilan(){
        $this->db->select('*');
        $this->db->from('srttdkpenghasilan');
        $query = $this->db->get();
        return $query->result();
    }

    
    public function surat_tidak_penghasilan(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srttdkpenghasilan.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srttdkpenghasilan.agama=mstagama.id');
        $this->db->join('kelurahan','srttdkpenghasilan.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srttdkpenghasilan.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srttdkpenghasilan.status=status_perkawinan.id');
		$this->db->from('srttdkpenghasilan');
        $this->db->where('srttdkpenghasilan.namakel',$kel);
        $this->db->where('srttdkpenghasilan.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function skck(){
        $this->db->select('*');
        $this->db->from('srtskck');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_skck(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtskck.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtskck.agama=mstagama.id');
        $this->db->join('kelurahan','srtskck.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtskck.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtskck.status=status_perkawinan.id');
		$this->db->from('srtskck');
        $this->db->where('srtskck.namakel',$kel);
        $this->db->where('srtskck.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function cerai(){
        $this->db->select('*');
        $this->db->from('srtcerai');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_cerai(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtcerai.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtcerai.agama=mstagama.id');
        $this->db->join('kelurahan','srtcerai.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtcerai.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtcerai.status=status_perkawinan.id');
		$this->db->from('srtcerai');
        $this->db->where('srtcerai.namakel',$kel);
        $this->db->where('srtcerai.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }


     public function akta_nikah(){
        $this->db->select('*');
        $this->db->from('srtaktanikah');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_akta_nikah(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtaktanikah.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtaktanikah.agama=mstagama.id');
        $this->db->join('kelurahan','srtaktanikah.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtaktanikah.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtaktanikah.status=status_perkawinan.id');
		$this->db->from('srtaktanikah');
        $this->db->where('srtaktanikah.namakel',$kel);
        $this->db->where('srtaktanikah.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }


    
    public function skbd(){
        $this->db->select('*');
        $this->db->from('srtskbd');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_skbd(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtskbd.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtskbd.agama=mstagama.id');
        $this->db->join('kelurahan','srtskbd.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtskbd.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtskbd.status=status_perkawinan.id');
		$this->db->from('srtskbd');
        $this->db->where('srtskbd.namakel',$kel);
        $this->db->where('srtskbd.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }
    
    public function keterangan(){
        $this->db->select('*');
        $this->db->from('srtketerangan');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_keterangan(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtketerangan.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtketerangan.agama=mstagama.id');
        $this->db->join('kelurahan','srtketerangan.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtketerangan.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtketerangan.status=status_perkawinan.id');
		$this->db->from('srtketerangan');
        $this->db->where('srtketerangan.namakel',$kel);
        $this->db->where('srtketerangan.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function usaha(){
        $this->db->select('*');
        $this->db->from('srtusaha');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_usaha(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtusaha.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtusaha.agama=mstagama.id');
        $this->db->join('kelurahan','srtusaha.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtusaha.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtusaha.status=status_perkawinan.id');
		$this->db->from('srtusaha');
        $this->db->where('srtusaha.namakel',$kel);
        $this->db->where('srtusaha.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function sengketa(){
        $this->db->select('*');
        $this->db->from('srtsengketa');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_sengketa(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtsengketa.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtsengketa.agama=mstagama.id');
        $this->db->join('kelurahan','srtsengketa.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtsengketa.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtsengketa.status=status_perkawinan.id');
		$this->db->from('srtsengketa');
        $this->db->where('srtsengketa.namakel',$kel);
        $this->db->where('srtsengketa.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function ortu(){
        $this->db->select('*');
        $this->db->from('srtortu');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_ortu(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtortu.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtortu.agama=mstagama.id');
        $this->db->join('kelurahan','srtortu.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtortu.namakec=kecamatan.id_kec');
		$this->db->from('srtortu');
        $this->db->where('srtortu.namakel',$kel);
        $this->db->where('srtortu.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

    public function domisili(){
        $this->db->select('*');
        $this->db->from('srtdomisili');
        $query = $this->db->get();
        return $query->result();
    }

    public function surat_domisili(){
        $kel = $this->session->userdata('id_kel');	
        $this->db->select('*');
		$this->db->join('mstpekerjaan','srtdomisili.pekerjaan=mstpekerjaan.id');
		$this->db->join('mstagama','srtdomisili.agama=mstagama.id');
        $this->db->join('kelurahan','srtdomisili.namakel=kelurahan.id_kel');
        $this->db->join('kecamatan','srtdomisili.namakec=kecamatan.id_kec');
        $this->db->join('status_perkawinan','srtdomisili.status=status_perkawinan.id');
		$this->db->from('srtdomisili');
        $this->db->where('srtdomisili.namakel',$kel);
        $this->db->where('srtdomisili.cekopt', 3);
		$query=$this->db->get();
		return $query->result();
    }

   
}