<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_profil_model extends CI_Model
{

public function get()
    {
        $this->db->select('*');
		$this->db->from('profil');
		$this->db->order_by('id_pro','DESC');
		$query=$this->db->get();
		return $query->result();	
    }

	public function agama(){
		$this->db->select('*');
		$this->db->from('mstagama');
		$query=$this->db->get();
		return $query->result();
	}

	function data_user(){
		$id=$this->session->userdata('id_users');	
		$this->db->select('*');
		$this->db->join('profil','user.id_users=profil.id_users');
		$this->db->from('user');
		$this->db->where('user.id_users',$id);
		$query=$this->db->get();
		return $query->result();
	}

	function simpan_profil(){
		$data = array(
				'id_users'  => $this->session->userdata('id_users'),
				'kk'     	=> $this->input->post('kk'),
				'nik'    	=> $this->input->post('nik'),
				'nama'     	=> $this->input->post('nama'),
				'tempat'    => $this->input->post('tempat'),
				'tgllahir'	=> $this->input->post('tgllahir'),
		  		'jenkel'    => $this->input->post('jenkel'),
				'agama'     => $this->input->post('agama'),
				'bangsa'	=> $this->input->post('bangsa'),
		);
		$query = $this->db->insert('profil', $data);
	}


Public function edit() {
		$data = array(
				'kk'     	=> $this->input->post('kk'),
				'nik'    	=> $this->input->post('nik'),
				'nama'     	=> $this->input->post('nama'),
				'tempat'    => $this->input->post('tempat'),
				'tgllahir'	=> $this->input->post('tgllahir'),
		  		'jenkel'    => $this->input->post('jenkel'),
				'bangsa'	=> $this->input->post('bangsa'),
				'agama'     => $this->input->post('agama')
    );     
   
            
			$id=$this->input->post('id_pro');
            $this->db->where('id_pro', $id);
			$this->db->update('profil', $data);
		
    }	



		
	


}
?>