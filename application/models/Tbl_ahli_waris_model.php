<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_ahli_waris_model extends CI_Model
{	

	public function pekerjaan()
    {
        $this->db->select('*');
		$this->db->from('mstpekerjaan');
		$query=$this->db->get();
		return $query->result();
    }

	public function agama(){
		$this->db->select('*');
		$this->db->from('mstagama');
		$query=$this->db->get();
		return $query->result();
	}

	public function simpan_anak(){

		$data = array(
			'id_users'=> $this->session->userdata('id_users'),
			'nama' => $this->input->post('nama'),
			'nik' => $this->input->post('nik'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'alamat' => $this->input->post('alamat'),
			'jenkel' => $this->input->post('jenkel'),
			'status' => $this->input->post('status'),
			);
            
        $query = $this->db->insert('ahli_waris', $data);
	}
	

	function update_anak(){
		$data = array(
			'nama' => $this->input->post('nama'),
			'nik' => $this->input->post('nik'),
			'tempat' => $this->input->post('tempat'),
			'tgllahir' => $this->input->post('tgllahir'),
			'jenkel' => $this->input->post('jenkel'),
			'status' => $this->input->post('status'),
			'alamat' => $this->input->post('alamat'),
			);
		$id=$this->input->post('id_ahli');
        $this->db->where('id_ahli', $id);
        $this->db->update('ahli_waris',$data);
	}

	public function ahli_waris(){
		$this->db->select('*');
		$this->db->from('ahli_waris');
		$query= $this->db->get();
		return $query->result();
	}
		
	

	

}
?>