/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 127.0.0.1:8889
 Source Schema         : ekelurahan

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 15/03/2022 11:35:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ahli_waris
-- ----------------------------
DROP TABLE IF EXISTS `ahli_waris`;
CREATE TABLE `ahli_waris` (
  `id_ahli` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(255) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tempat` varchar(255) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jenkel` varchar(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ahli`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ahli_waris
-- ----------------------------
BEGIN;
INSERT INTO `ahli_waris` (`id_ahli`, `id_users`, `nik`, `nama`, `tempat`, `tgllahir`, `alamat`, `jenkel`, `status`) VALUES (30, 7, '1231313131323232', 'Medan', 'medan', '1111-11-11', 'Medan', 'Laki-Laki', 'Anak Kandung');
COMMIT;

-- ----------------------------
-- Table structure for anak_ortu
-- ----------------------------
DROP TABLE IF EXISTS `anak_ortu`;
CREATE TABLE `anak_ortu` (
  `id_anak` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tempat` varchar(255) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `warga_negara` varchar(255) DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_anak`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of anak_ortu
-- ----------------------------
BEGIN;
INSERT INTO `anak_ortu` (`id_anak`, `id_users`, `nama`, `tempat`, `tgllahir`, `warga_negara`, `agama`, `pekerjaan`, `alamat`) VALUES (27, 7, 'adad1', 'medan', '1111-11-11', 'Indonesia1', '1', '1', 'Medan');
COMMIT;

-- ----------------------------
-- Table structure for hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_level` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses
-- ----------------------------
BEGIN;
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (1, 1, 1);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (4, 1, 2);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (6, 1, 4);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (63, 3, 30);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (64, 3, 1);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (65, 2, 1);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (66, 2, 31);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (67, 4, 32);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (68, 4, 1);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (69, 2, 33);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (70, 1, 34);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (71, 1, 35);
INSERT INTO `hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES (72, 3, 36);
COMMIT;

-- ----------------------------
-- Table structure for kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan` (
  `id_kec` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(2) DEFAULT NULL,
  `nama_kec` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kec`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kecamatan
-- ----------------------------
BEGIN;
INSERT INTO `kecamatan` (`id_kec`, `kode`, `nama_kec`) VALUES (1, '01', 'BINJAI BARAT');
INSERT INTO `kecamatan` (`id_kec`, `kode`, `nama_kec`) VALUES (2, '02', 'BINJAI KOTA');
INSERT INTO `kecamatan` (`id_kec`, `kode`, `nama_kec`) VALUES (3, '03', 'BINJAI TIMUR');
INSERT INTO `kecamatan` (`id_kec`, `kode`, `nama_kec`) VALUES (4, '04', 'BINJAI UTARA');
INSERT INTO `kecamatan` (`id_kec`, `kode`, `nama_kec`) VALUES (5, '05', 'BINJAI SELATAN');
COMMIT;

-- ----------------------------
-- Table structure for kelurahan
-- ----------------------------
DROP TABLE IF EXISTS `kelurahan`;
CREATE TABLE `kelurahan` (
  `id_kel` int(11) NOT NULL AUTO_INCREMENT,
  `id_kec` int(11) DEFAULT NULL,
  `kode` varchar(4) DEFAULT NULL,
  `nama_kel` varchar(50) DEFAULT NULL,
  `nama_kec` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_kel`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelurahan
-- ----------------------------
BEGIN;
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (1, 1, '0101', 'BANDAR SENEMBAH', 'BINJAI BARAT', '20719');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (2, 1, '0102', 'LIMAU MUNGKUR', 'BINJAI BARAT', '20717');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (3, 1, '0103', 'SUKA RAMAI', 'BINJAI BARAT', '20717');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (4, 1, '0104', 'PAYA ROBA', 'BINJAI BARAT', '20718');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (5, 1, '0105', 'LIMAU SUNDAI', 'BINJAI BARAT', '20716');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (6, 1, '0106', 'SUKA MAJU', 'BINJAI BARAT', '20719');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (7, 2, '0201', 'PEKAN BINJAI', 'BINJAI KOTA', '20711');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (8, 2, '0202', 'BINJAI', 'BINJAI KOTA', '20712');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (9, 2, '0203', 'KARTINI', 'BINJAI KOTA', '20713');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (10, 2, '0204', 'SETIA', 'BINJAI KOTA', '20713');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (11, 2, '0205', 'SATRIA', 'BINJAI KOTA', '20714');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (12, 2, '0206', 'TANGSI', 'BINJAI KOTA', '20714');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (13, 2, '0207', 'BERNGAM', 'BINJAI KOTA', '20715');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (14, 3, '0301', 'TANAH TINGGI', 'BINJAI TIMUR', '20731');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (15, 3, '0302', 'TIMBANG LANGKAT', 'BINJAI TIMUR', '20732');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (16, 3, '0303', 'MENCIRIM', 'BINJAI TIMUR', '20733');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (17, 3, '0304', 'TUNGGURONO', 'BINJAI TIMUR', '20734');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (18, 3, '0305', 'SUMBER MULYO REJO', 'BINJAI TIMUR', '20735');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (19, 3, '0306', 'DATARAN TINGGI', 'BINJAI TIMUR', '20736');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (20, 3, '0307', 'SUMBER KARYA', 'BINJAI TIMUR', '20737');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (21, 4, '0401', 'JATI NEGARA', 'BINJAI UTARA', '20741');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (22, 4, '0402', 'NANGKA', 'BINJAI UTARA', '20742');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (23, 4, '0403', 'PAHLAWAN', 'BINJAI UTARA', '20743');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (24, 4, '0404', 'KEBUN LADA', 'BINJAI UTARA', '20744');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (25, 4, '0405', 'DAMAI', 'BINJAI UTARA', '20745');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (26, 4, '0406', 'JATI KARYA', 'BINJAI UTARA', '20746');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (27, 4, '0407', 'CENGKEH TURI', 'BINJAI UTARA', '20747');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (28, 4, '0408', 'JATI MAKMUR', 'BINJAI UTARA', '20746');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (29, 4, '0409', 'JATI UTOMO', 'BINJAI UTARA', '20746');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (30, 5, '0501', 'RAMBUNG TIMUR', 'BINJAI SELATAN', '20721');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (31, 5, '0502', 'RAMBUNG DALAM', 'BINJAI SELATAN', '20722');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (32, 5, '0503', 'RAMBUNG BARAT', 'BINJAI SELATAN', '20723');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (33, 5, '0504', 'BINJAI ESTATE', 'BINJAI SELATAN', '20724');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (34, 5, '0505', 'TANAH MERAH', 'BINJAI SELATAN', '20725');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (35, 5, '0506', 'TANAH SERIBU', 'BINJAI SELATAN', '20726');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (36, 5, '0507', 'PUJIDADI', 'BINJAI SELATAN', '20727');
INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `kode`, `nama_kel`, `nama_kec`, `kode_pos`) VALUES (37, 5, '0508', 'BHAKTI KARYA', 'BINJAI SELATAN', '20728');
COMMIT;

-- ----------------------------
-- Table structure for master_surat
-- ----------------------------
DROP TABLE IF EXISTS `master_surat`;
CREATE TABLE `master_surat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `nama_surat` varchar(100) DEFAULT NULL,
  `aktif_surat` varchar(1) DEFAULT NULL,
  `modul1` varchar(50) DEFAULT NULL,
  `modul2` varchar(50) DEFAULT NULL,
  `modul3` varchar(50) DEFAULT NULL,
  `modul4` varchar(50) DEFAULT NULL,
  `modul5` varchar(50) DEFAULT NULL,
  `modul6` varchar(50) DEFAULT NULL,
  `modul7` varchar(50) DEFAULT NULL,
  `modul8` varchar(50) DEFAULT NULL,
  `modul9` varchar(50) DEFAULT NULL,
  `modul10` varchar(50) DEFAULT NULL,
  `modul11` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_surat
-- ----------------------------
BEGIN;
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (1, '474.2', 'Surat Pengantar Perkawinan', '1', 'srtnikah', 'vfkasrtnikah', 'vfseksrtnikah', 'vflusrtnikah', 'sksrtnikah', 'rptsksrtnikah', 'rptsksrtnikah2', 'rptsksrtnikah3', 'rptsksrtnikah4', 'rptsksrtnikah5', 'srtnikahkota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (2, '470', 'Surat Belum Menikah Untuk Bekerja', '1', 'srtblmnikah', 'vfkasrtblmnikah', 'vfseksrtblmnikah', 'vflusrtblmnikah', 'sksrtblmnikah', 'rptsksrtblmnikah', 'rptsksrtblmnikah2', 'rptsksrtblmnikah3', 'rptsksrtblmnikah4', 'rptsksrtblmnikah5', 'srtblmnikahkota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (3, '470', 'Surat Keterangan Ahli Waris', '1', 'srtwaris', 'vfkasrtwaris', 'vfseksrtwaris', 'vflusrtwaris', 'sksrtwaris', 'rptsksrtwaris', 'rptsksrtwaris2', 'rptsksrtwaris3', 'rptsksrtwaris4', 'rptsksrtwaris5', 'srtwariskota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (4, '474.3', 'Surat Pengantar Kematian', '1', 'srtkematian', 'vfkasrtkematian', 'vfseksrtkematian', 'vflusrtkematian', 'sksrtkematian', 'rptsksrtkematian', 'rptsksrtkematian2', 'rptsksrtkematian3', 'rptsksrtkematian4', 'rptsksrtkematian5', 'srtkematiankota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (6, '401', 'Surat Keterangan Miskin', '1', 'srttdkmampu', 'vfkasrttdkmampu', 'vfseksrttdkmampu', 'vflusrttdkmampu', 'sksrttdkmampu', 'rptsksrttdkmampu', 'rptsksrttdkmampu2', 'rptsksrttdkmampu3', 'rptsksrttdkmampu4', 'rptsksrttdkmampu5', 'srttdkmampukota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (7, '06', 'Surat Keterangan Berpenghasilan', '0', 'srtpenghasilan', 'vfkasrtpenghasilan', 'vfseksrtpenghasilan', 'vflusrtpenghasilan', 'sksrtpenghasilan', 'rptsksrtpenghasilan', 'rptsksrtpenghasilan2', 'rptsksrtpenghasilan3', 'rptsksrtpenghasilan4', 'rptsksrtpenghasilan5', 'srtpenghasilankota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (8, '07', 'Surat Keterangan Tidak Berpenghasilan', '0', 'srttdkpenghasilan', 'vfkasrttdkpenghasilan', 'vfseksrttdkpenghasilan', 'vflusrttdkpenghasilan', 'sksrttdkpenghasilan', 'rptsksrttdkpenghasilan', 'rptsksrttdkpenghasilan2', 'rptsksrttdkpenghasilan3', 'rptsksrttdkpenghasilan4', 'rptsksrttdkpenghasilan5', 'srttdkpenghasilankota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (9, '470', 'Surat Keterangan Pengurusan SKCK', '1', 'srtskck', 'vfkasrtskck', 'vfseksrtskck', 'vflusrtskck', 'sksrtskck', 'rptsksrtskck', 'rptsksrtskck2', 'rptsksrtskck3', 'rptsksrtskck4', 'rptsksrtskck5', 'srtskckkota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (10, '09', 'Surat Keterangan Pengurusan Perceraian', '0', 'srtcerai', 'vfkasrtcerai', 'vfseksrtcerai', 'vflusrtcerai', 'sksrtcerai', 'rptsksrtcerai', 'rptsksrtcerai2', 'rptsksrtcerai3', 'rptsksrtcerai4', 'rptsksrtcerai5', 'srtceraikota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (11, '10', 'Surat Keterangan Akta Pernikahan', '0', 'srtaktanikah', 'vfkasrtaktanikah', 'vfseksrtaktanikah', 'vflusrtaktanikah', 'sksrtaktanikah', 'rptskaktanikah', 'rptsksrtaktanikah2', 'rptskaktanikah3', 'rptsksrtaktanikah4', 'rptsksrtaktanikah5', 'srtaktanikahkota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (12, '11', 'Surat Keterangan Bersih Diri (SKBD)', '0', 'srtskbd', 'vfkasrtskbd', 'vfseksrtskbd', 'vflusrtskbd', 'sksrtskbd', 'rptsksrtskbd', 'rptsksrtskbd2', 'rptsksrtskbd3', 'rptsksrtskbd4', 'rptsksrtskbd5', 'srtskbdkota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (17, '12', 'Surat Keterangan Lain', '0', 'srtketerangan', 'vfkasrtketerangan', 'vfseksrtketerangan', 'vflusrtketerangan', 'sksrtketerangan', 'rptsksrtketerangan', 'rptsksrtketerangan2', 'rptsksrtketerangan3', 'rptsksrtketerangan4', 'rptsksrtketerangan5', 'srtskketerangankota');
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (18, '470', 'Surat Keterangan Usaha', '1', 'srtusaha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (19, '470', 'Surat Laporan Silang Sengketa', '1', 'srtsengketa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (20, '474.2', 'Surat Izin Orang Tua', '1', 'srtortu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `master_surat` (`id`, `kode`, `nama_surat`, `aktif_surat`, `modul1`, `modul2`, `modul3`, `modul4`, `modul5`, `modul6`, `modul7`, `modul8`, `modul9`, `modul10`, `modul11`) VALUES (21, '470', 'Surat Domilisi', '1', 'srtdomisili', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `url` varchar(30) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(3) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL COMMENT 'y=yes,n=no',
  `urut` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (1, 'Home', 'Home', 'fa fa-home text-primary', 0, 'y', '1');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (2, 'Master Surat', 'Master_surat', 'fa fa-th text-info', 0, 'y', '2');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (4, 'Pengaturan', 'Identitas', 'fa fa-cog', 0, 'n', '3');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (30, 'Pengajuan Surat', 'Surat', 'fa fa-envelope-o text-success', 0, 'y', '4');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (31, 'Verifikasi Surat', 'Verifikasi_surat', 'fa fa-envelope-o text-success', 0, 'y', '5');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (32, 'Verifikasi Lurah', 'Verifikasi_lurah', 'fa fa-envelope-o text-success', 0, 'y', '6');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (33, 'Arsip', 'Arsip', 'fa fa-envelope-o text-primary', 0, 'y', '7');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (34, 'Manajemen User', 'User', 'fa fa-envelope-o text-primary', 0, 'y', '8');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (35, 'Referensi Lurah', 'Referensi_lurah', 'fa fa-envelope-o text-primary', 0, 'y', '9');
INSERT INTO `menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`, `urut`) VALUES (36, 'Profil', 'Profil', 'fa fa-user text-primary', 0, 'y', '10');
COMMIT;

-- ----------------------------
-- Table structure for mstagama
-- ----------------------------
DROP TABLE IF EXISTS `mstagama`;
CREATE TABLE `mstagama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mstagama
-- ----------------------------
BEGIN;
INSERT INTO `mstagama` (`id`, `agama`) VALUES (1, 'Islam');
INSERT INTO `mstagama` (`id`, `agama`) VALUES (2, 'Kristen Protestan');
INSERT INTO `mstagama` (`id`, `agama`) VALUES (3, 'Kristen Katholik');
INSERT INTO `mstagama` (`id`, `agama`) VALUES (4, 'Hindu');
INSERT INTO `mstagama` (`id`, `agama`) VALUES (5, 'Budha');
INSERT INTO `mstagama` (`id`, `agama`) VALUES (6, 'Konghucu');
COMMIT;

-- ----------------------------
-- Table structure for mstpekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `mstpekerjaan`;
CREATE TABLE `mstpekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapekerjaan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mstpekerjaan
-- ----------------------------
BEGIN;
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (1, 'PNS');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (2, 'TNI/POLRI');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (3, 'Petani');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (4, 'Pedagang');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (5, 'Nelayan');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (6, 'Wiraswasta');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (7, 'Ibu Rumah Tangga');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (8, 'Karyawan BUMN');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (9, 'Karyawan BUMD');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (10, 'Pegawai Bank');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (11, 'Pegawai Swasta');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (12, 'Buruh');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (13, 'Pelajar/Mahasiswa');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (14, 'Pelajar');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (15, 'Belum Bekerja');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (16, 'Pembantu Rumah Tangga');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (17, 'Pensiunan');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (18, 'Perawat');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (19, 'Bidan');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (20, 'Dokter');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (21, 'Pilot');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (22, 'Pramugari');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (23, 'Pramugara');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (24, 'Tidak Bekerja');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (25, 'Guru');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (26, 'Dosen');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (27, 'Apoteker');
INSERT INTO `mstpekerjaan` (`id`, `namapekerjaan`) VALUES (28, 'Pegawai Honor');
COMMIT;

-- ----------------------------
-- Table structure for password
-- ----------------------------
DROP TABLE IF EXISTS `password`;
CREATE TABLE `password` (
  `id_password` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of password
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for profil
-- ----------------------------
DROP TABLE IF EXISTS `profil`;
CREATE TABLE `profil` (
  `id_pro` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `kk` varchar(16) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `tgllahir` date NOT NULL,
  `jenkel` varchar(20) NOT NULL,
  `agama` varchar(20) NOT NULL,
  PRIMARY KEY (`id_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of profil
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for referensi_lurah
-- ----------------------------
DROP TABLE IF EXISTS `referensi_lurah`;
CREATE TABLE `referensi_lurah` (
  `id_lurah` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(255) NOT NULL,
  `nama_lurah` varchar(255) NOT NULL,
  `id_kel` int(11) NOT NULL,
  `id_kec` int(11) NOT NULL,
  `kode_lurah` varchar(5) NOT NULL,
  `kop` varchar(255) NOT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_lurah`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of referensi_lurah
-- ----------------------------
BEGIN;
INSERT INTO `referensi_lurah` (`id_lurah`, `nip`, `nama_lurah`, `id_kel`, `id_kec`, `kode_lurah`, `kop`, `jabatan`) VALUES (1, '12312313213', 'naim', 1, 1, 'KM', 'kop.png', 'Penata TK I');
COMMIT;

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `nama_setting` varchar(50) DEFAULT NULL,
  `value` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setting
-- ----------------------------
BEGIN;
INSERT INTO `setting` (`id_setting`, `nama_setting`, `value`) VALUES (1, 'Tampil Menu', 'ya');
COMMIT;

-- ----------------------------
-- Table structure for srtaktanikah
-- ----------------------------
DROP TABLE IF EXISTS `srtaktanikah`;
CREATE TABLE `srtaktanikah` (
  `id_an` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `jenis_daftar` varchar(2) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_an`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtaktanikah
-- ----------------------------
BEGIN;
INSERT INTO `srtaktanikah` (`id_an`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `jenis_daftar`, `foto_kk`, `foto_ktp`) VALUES (4, 7, '2022-01-28', '202201280002', '2022-01-28', '0002', '1231231231231231', 'Udin', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'medan', NULL, NULL, 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(2)5.png', 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(2)4.png');
COMMIT;

-- ----------------------------
-- Table structure for srtblmnikah
-- ----------------------------
DROP TABLE IF EXISTS `srtblmnikah`;
CREATE TABLE `srtblmnikah` (
  `id_bn` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `kk` varchar(255) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_bn`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtblmnikah
-- ----------------------------
BEGIN;
INSERT INTO `srtblmnikah` (`id_bn`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `kk`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `tglpernyataan`, `foto_kk`, `foto_ktp`, `id_surat`) VALUES (4, 7, '2022-02-25', '202202252314', '2022-02-25', '1234', '123123123123123', '1231231231231231', 'da', 'medan', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Melamar Pekerjaan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, NULL, 'e2767e7972a169ec65d6616a5410e8052.jpeg', 'image8-22.jpg', '2');
COMMIT;

-- ----------------------------
-- Table structure for srtcerai
-- ----------------------------
DROP TABLE IF EXISTS `srtcerai`;
CREATE TABLE `srtcerai` (
  `id_ce` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ce`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtcerai
-- ----------------------------
BEGIN;
INSERT INTO `srtcerai` (`id_ce`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `ket`, `foto_kk`, `foto_ktp`) VALUES (4, 7, '2022-01-28', '202201280002', '2022-01-28', '0002', '1231231231231231', 'bayu', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, NULL, 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(2)7.png', 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(2)6.png');
COMMIT;

-- ----------------------------
-- Table structure for srtdomisili
-- ----------------------------
DROP TABLE IF EXISTS `srtdomisili`;
CREATE TABLE `srtdomisili` (
  `id_do` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `kk` varchar(255) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_do`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtdomisili
-- ----------------------------
BEGIN;
INSERT INTO `srtdomisili` (`id_do`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `kk`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `tglpernyataan`, `foto_kk`, `foto_ktp`, `id_surat`) VALUES (1, 7, '2022-03-04', '202203042314', '2022-03-04', '1234', '123123123123123', '1231231231231231', 'adad', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-03-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', 'Melamar Pekerjaan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan Denai', NULL, NULL, 'image8-21.jpg', 'e2767e7972a169ec65d6616a5410e8051.jpeg', '21');
COMMIT;

-- ----------------------------
-- Table structure for srtkematian
-- ----------------------------
DROP TABLE IF EXISTS `srtkematian`;
CREATE TABLE `srtkematian` (
  `id_ma` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` longtext,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `namareject` varchar(50) DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `harimati` varchar(10) DEFAULT NULL,
  `tglmati` date DEFAULT NULL,
  `tempatmati` varchar(200) DEFAULT NULL,
  `namaket` varchar(100) DEFAULT NULL,
  `instansi` varchar(100) DEFAULT NULL,
  `hubungan` varchar(100) DEFAULT NULL,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `cekreject` char(1) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  `sebab` varchar(255) DEFAULT NULL,
  `visum` varchar(255) DEFAULT NULL,
  `pelapor` varchar(255) DEFAULT NULL,
  `kk` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_ma`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtkematian
-- ----------------------------
BEGIN;
INSERT INTO `srtkematian` (`id_ma`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `waktulurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `namareject`, `waktucetak`, `tahun`, `bulan`, `keterangan`, `harimati`, `tglmati`, `tempatmati`, `namaket`, `instansi`, `hubungan`, `lurah`, `nip`, `pangkat_gol`, `alamat`, `cek1`, `cek2`, `cek3`, `cek4`, `cekreject`, `foto_kk`, `foto_ktp`, `id_surat`, `sebab`, `visum`, `pelapor`, `kk`) VALUES (7, 7, '2022-03-10', '202203102333', '2022-03-10', '1234', '1231231231231231', 'Markonah', 'medan', '1111-11-11', 'Laki-Laki', '1', '1', '1', 'medan', '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, '1111-11-11', 'RSUD', 'Bayu', NULL, 'anak Kandung', NULL, NULL, NULL, 'Medan', NULL, NULL, NULL, NULL, NULL, 'kursi-teras-staking-22.jpg', 'kursi-teras-staking-23.jpg', '4', 'Sakit', '12312313', 'Udin', '123123123123123');
COMMIT;

-- ----------------------------
-- Table structure for srtketerangan
-- ----------------------------
DROP TABLE IF EXISTS `srtketerangan`;
CREATE TABLE `srtketerangan` (
  `id_ke` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(255) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` varchar(255) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `lurah` varchar(100) DEFAULT NULL,
  `nip_lurah` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `ket1` longtext,
  `ket2` longtext,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ke`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtketerangan
-- ----------------------------
BEGIN;
INSERT INTO `srtketerangan` (`id_ke`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `lurah`, `nip_lurah`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `ket1`, `ket2`, `foto_kk`, `foto_ktp`) VALUES (4, 7, '2022-01-31', '202201310002', '2022-01-31', '0002', '1231231231231231', 'Udin', 'Dolok ', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, NULL, NULL, 'kursi-teras-staking-2.jpg', 'Kursi-Taman-Minimalis-19a-scaled.jpg');
COMMIT;

-- ----------------------------
-- Table structure for srtnikah
-- ----------------------------
DROP TABLE IF EXISTS `srtnikah`;
CREATE TABLE `srtnikah` (
  `id_sn` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `jam_daftar` time DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `jam_surat` time DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` text,
  `lurah` varchar(100) DEFAULT NULL,
  `nip_lurah` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `alamat_ayah` varchar(200) DEFAULT NULL,
  `tempat_ayah` varchar(50) DEFAULT NULL,
  `tgllahir_ayah` date DEFAULT NULL,
  `nik_ayah` varchar(50) DEFAULT NULL,
  `bangsa_ayah` varchar(50) DEFAULT NULL,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `nama_ibu` varchar(100) DEFAULT NULL,
  `alamat_ibu` varchar(200) DEFAULT NULL,
  `tempat_ibu` varchar(50) DEFAULT NULL,
  `tgllahir_ibu` date DEFAULT NULL,
  `nik_ibu` varchar(50) DEFAULT NULL,
  `bangsa_ibu` varchar(50) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `agama_ayah` varchar(50) DEFAULT NULL,
  `agama_ibu` varchar(50) DEFAULT NULL,
  `pasangan_terdahulu` varchar(50) DEFAULT NULL,
  `ketpria` longtext,
  `ketwanita` longtext,
  `jenis_daftar` varchar(2) DEFAULT NULL,
  `foto_kk` text NOT NULL,
  `foto_ktp` text NOT NULL,
  `id_surat` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_sn`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtnikah
-- ----------------------------
BEGIN;
INSERT INTO `srtnikah` (`id_sn`, `id_users`, `tgldaftar`, `jam_daftar`, `noreg`, `tglsurat`, `jam_surat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `lurah`, `nip_lurah`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `nama_ayah`, `alamat_ayah`, `tempat_ayah`, `tgllahir_ayah`, `nik_ayah`, `bangsa_ayah`, `pekerjaan_ayah`, `nama_ibu`, `alamat_ibu`, `tempat_ibu`, `tgllahir_ibu`, `nik_ibu`, `bangsa_ibu`, `pekerjaan_ibu`, `agama_ayah`, `agama_ibu`, `pasangan_terdahulu`, `ketpria`, `ketwanita`, `jenis_daftar`, `foto_kk`, `foto_ktp`, `id_surat`) VALUES (21, 7, '2022-03-14', '14:43:12', '202203140001', NULL, NULL, '1234', '1231231231231231', 'Medan', 'medan', '1111-11-11', 'Laki-Laki', '1', '3', '1', 'Indonesia', '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, 'aaa', 'medan', 'medan', '1111-11-11', '1231231231313', 'Indonesia', '4', 'bu', 'medan', 'medan', '1111-11-11', '12312312313', 'Indonesia', '4', '2', '3', NULL, NULL, NULL, '1', 'Kursi-Teras-Jati-Jepara2.jpg', 'kursi-teras-jati2.jpg', '1');
COMMIT;

-- ----------------------------
-- Table structure for srtortu
-- ----------------------------
DROP TABLE IF EXISTS `srtortu`;
CREATE TABLE `srtortu` (
  `id_or` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `kk` varchar(255) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  `nama_ayah` varchar(255) DEFAULT NULL,
  `nik_ayah` varchar(255) DEFAULT NULL,
  `bangsa_ayah` varchar(255) DEFAULT NULL,
  `agama_ayah` varchar(255) DEFAULT NULL,
  `alamat_ayah` varchar(255) DEFAULT NULL,
  `tempat_ayah` varchar(255) DEFAULT NULL,
  `tgllahir_ayah` date DEFAULT NULL,
  `pekerjaan_ayah` varchar(255) DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  `nik_ibu` varchar(255) DEFAULT NULL,
  `bangsa_ibu` varchar(255) DEFAULT NULL,
  `agama_ibu` varchar(255) DEFAULT NULL,
  `alamat_ibu` varchar(255) DEFAULT NULL,
  `tempat_ibu` varchar(255) DEFAULT NULL,
  `tgllahir_ibu` date DEFAULT NULL,
  `pekerjaan_ibu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_or`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtortu
-- ----------------------------
BEGIN;
INSERT INTO `srtortu` (`id_or`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `kk`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `tglpernyataan`, `foto_kk`, `foto_ktp`, `id_surat`, `nama_ayah`, `nik_ayah`, `bangsa_ayah`, `agama_ayah`, `alamat_ayah`, `tempat_ayah`, `tgllahir_ayah`, `pekerjaan_ayah`, `nama_ibu`, `nik_ibu`, `bangsa_ibu`, `agama_ibu`, `alamat_ibu`, `tempat_ibu`, `tgllahir_ibu`, `pekerjaan_ibu`) VALUES (9, 7, '2022-03-08', '202203082333', '2022-03-08', '1234', NULL, '1231231231231231', 'Medan', 'medan', '1111-11-11', NULL, '1', NULL, '1', NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, NULL, 'e2767e7972a169ec65d6616a5410e8055.jpeg', 'image8-27.jpg', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bu', '12312312313', 'Indonesia', '5', 'medan', 'medan', '1232-03-12', '1');
COMMIT;

-- ----------------------------
-- Table structure for srtpenghasilan
-- ----------------------------
DROP TABLE IF EXISTS `srtpenghasilan`;
CREATE TABLE `srtpenghasilan` (
  `id_pe` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `penghasilan` decimal(24,0) DEFAULT NULL,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pe`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtpenghasilan
-- ----------------------------
BEGIN;
INSERT INTO `srtpenghasilan` (`id_pe`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `penghasilan`, `tglpernyataan`, `foto_kk`, `foto_ktp`) VALUES (6, 7, '2022-01-27', '202201270002', '2022-01-27', '0002', '1231231231231231', 'aASa', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'medan', NULL, NULL, NULL, 'bayi1.jpg', 'bayi.jpg');
COMMIT;

-- ----------------------------
-- Table structure for srtsengketa
-- ----------------------------
DROP TABLE IF EXISTS `srtsengketa`;
CREATE TABLE `srtsengketa` (
  `id_se` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `kk` varchar(255) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_se`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtsengketa
-- ----------------------------
BEGIN;
INSERT INTO `srtsengketa` (`id_se`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `kk`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `tglpernyataan`, `foto_kk`, `foto_ktp`, `id_surat`) VALUES (2, 7, '2022-03-04', '202203042314', '2022-03-04', '1234', NULL, '1231231231231231', 'adad', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-03-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', 'Pengajuan sertifikat di badan pertanahan nasional kota binjai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan Denai', NULL, NULL, 'e2767e7972a169ec65d6616a5410e8052.jpeg', 'e2767e7972a169ec65d6616a5410e8053.jpeg', '19');
COMMIT;

-- ----------------------------
-- Table structure for srtskbd
-- ----------------------------
DROP TABLE IF EXISTS `srtskbd`;
CREATE TABLE `srtskbd` (
  `id_skbd` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `suku` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(50) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `camat` varchar(100) DEFAULT NULL,
  `nip_camat` varchar(50) DEFAULT NULL,
  `pangkat_camat` varchar(50) DEFAULT NULL,
  `koramil` varchar(50) DEFAULT NULL,
  `nrp` varchar(100) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `alamat_ayah` varchar(200) DEFAULT NULL,
  `tempat_ayah` varchar(50) DEFAULT NULL,
  `tgllahir_ayah` date DEFAULT NULL,
  `nik_ayah` varchar(50) DEFAULT NULL,
  `suku_ayah` varchar(50) DEFAULT NULL,
  `bangsa_ayah` varchar(50) DEFAULT NULL,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `nama_ibu` varchar(100) DEFAULT NULL,
  `alamat_ibu` varchar(200) DEFAULT NULL,
  `tempat_ibu` varchar(50) DEFAULT NULL,
  `tgllahir_ibu` date DEFAULT NULL,
  `nik_ibu` varchar(50) DEFAULT NULL,
  `suku_ibu` varchar(50) DEFAULT NULL,
  `bangsa_ibu` varchar(50) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `warga_negara` varchar(20) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_skbd`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtskbd
-- ----------------------------
BEGIN;
INSERT INTO `srtskbd` (`id_skbd`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `suku`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `camat`, `nip_camat`, `pangkat_camat`, `koramil`, `nrp`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `nama_ayah`, `alamat_ayah`, `tempat_ayah`, `tgllahir_ayah`, `nik_ayah`, `suku_ayah`, `bangsa_ayah`, `pekerjaan_ayah`, `nama_ibu`, `alamat_ibu`, `tempat_ibu`, `tgllahir_ibu`, `nik_ibu`, `suku_ibu`, `bangsa_ibu`, `pekerjaan_ibu`, `warga_negara`, `foto_kk`, `foto_ktp`) VALUES (3, 7, '2022-01-31', '202201310002', '2022-01-31', '0002', '1231231231231231', 'bayu naim', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'medan', NULL, 'Udin', 'medan', 'Medan', '1111-11-11', NULL, NULL, NULL, '4', 'Markona', 'medan', 'Medan', '1111-11-11', NULL, NULL, NULL, '1', NULL, 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(1)2.png', 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(2)8.png');
COMMIT;

-- ----------------------------
-- Table structure for srtskck
-- ----------------------------
DROP TABLE IF EXISTS `srtskck`;
CREATE TABLE `srtskck` (
  `id_sk` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `namareject` varchar(50) DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `kk` varchar(20) DEFAULT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_sk`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtskck
-- ----------------------------
BEGIN;
INSERT INTO `srtskck` (`id_sk`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `namareject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `foto_kk`, `foto_ktp`, `kk`, `id_surat`) VALUES (4, 7, '2022-03-01', '202203012314', '2022-03-01', '1234', '1231231231231231', 'as', 'medan', '3222-02-12', 'Laki-Laki', '2', '1', '1', NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ke Polres Binjai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, 'e2767e7972a169ec65d6616a5410e8054.jpeg', 'e2767e7972a169ec65d6616a5410e8055.jpeg', '123123123123123', '9');
COMMIT;

-- ----------------------------
-- Table structure for srttdkmampu
-- ----------------------------
DROP TABLE IF EXISTS `srttdkmampu`;
CREATE TABLE `srttdkmampu` (
  `id_tm` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `namareject` varchar(50) DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` char(1) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  `kk` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_tm`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srttdkmampu
-- ----------------------------
BEGIN;
INSERT INTO `srttdkmampu` (`id_tm`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `namareject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `foto_kk`, `foto_ktp`, `id_surat`, `kk`) VALUES (3, 7, '2022-03-01', '202203012314', '2022-03-01', '1234', '1231231231231231', 'da', 'medan', '1221-12-12', 'Laki-Laki', '1', '1', '1', NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ke Dinas Sosial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, 'e2767e7972a169ec65d6616a5410e8054.jpeg', 'e2767e7972a169ec65d6616a5410e8055.jpeg', '6', '123123123123123');
COMMIT;

-- ----------------------------
-- Table structure for srttdkpenghasilan
-- ----------------------------
DROP TABLE IF EXISTS `srttdkpenghasilan`;
CREATE TABLE `srttdkpenghasilan` (
  `id_tp` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `pernyataan` longtext,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tp`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srttdkpenghasilan
-- ----------------------------
BEGIN;
INSERT INTO `srttdkpenghasilan` (`id_tp`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `pernyataan`, `tglpernyataan`, `foto_kk`, `foto_ktp`) VALUES (4, 7, '2022-01-27', '202201270002', '2022-01-27', '0002', '1231231231231231', 'bayu', 'Dolok K', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'medan', NULL, NULL, NULL, 'WhatsApp_Image_2021-12-30_at_14_13_516.jpeg', 'Social_media_can_also_be_used_to_share_interesting_facts,_true_stories,_and_other_important_information__When_creating_your_own,_include_the_most_relevant_points_in_your_design__(1)2.png');
COMMIT;

-- ----------------------------
-- Table structure for srtusaha
-- ----------------------------
DROP TABLE IF EXISTS `srtusaha`;
CREATE TABLE `srtusaha` (
  `id_us` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `kk` varchar(255) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(100) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `tglpernyataan` date DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `id_surat` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_us`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtusaha
-- ----------------------------
BEGIN;
INSERT INTO `srtusaha` (`id_us`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `kk`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `warga_negara`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `tglpernyataan`, `foto_kk`, `foto_ktp`, `id_surat`) VALUES (5, 7, '2022-03-02', '202203022314', '2022-03-02', '1234', '123123123123123', '1231231231231231', 'adad12345', '1231231231231231', '1111-11-11', 'Laki-Laki', '1', '1', '1', NULL, '3', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, '2022-03-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', 'Melamar Pekerjaan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medan', NULL, NULL, 'e2767e7972a169ec65d6616a5410e80510.jpeg', 'e2767e7972a169ec65d6616a5410e80511.jpeg', '18');
COMMIT;

-- ----------------------------
-- Table structure for srtwaris
-- ----------------------------
DROP TABLE IF EXISTS `srtwaris`;
CREATE TABLE `srtwaris` (
  `id_wa` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `tgldaftar` date DEFAULT NULL,
  `noreg` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `nosurat` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `cekopt` varchar(1) DEFAULT NULL,
  `tglcekopt` date DEFAULT NULL,
  `waktuopt` time DEFAULT NULL,
  `userentry` varchar(50) DEFAULT NULL,
  `ceklurah` varchar(1) DEFAULT NULL,
  `tglceklurah` date DEFAULT NULL,
  `namalurah` varchar(50) DEFAULT NULL,
  `kodekel` varchar(4) DEFAULT NULL,
  `namakel` varchar(100) DEFAULT NULL,
  `kodekec` varchar(2) DEFAULT NULL,
  `namakec` varchar(50) DEFAULT NULL,
  `cekcetak` varchar(1) DEFAULT NULL,
  `tglcetak` date DEFAULT NULL,
  `namacetak` varchar(50) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `reject` varchar(200) DEFAULT NULL,
  `tglreject` date DEFAULT NULL,
  `waktucetak` time DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `waktulurah` time DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `peruntukan` longtext,
  `lurah` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pangkat_gol` varchar(50) DEFAULT NULL,
  `ahliwaris1` varchar(100) DEFAULT NULL,
  `ahliwaris2` varchar(100) DEFAULT NULL,
  `ahliwaris3` varchar(100) DEFAULT NULL,
  `ahliwaris4` varchar(100) DEFAULT NULL,
  `cek1` varchar(1) DEFAULT NULL,
  `cek2` varchar(1) DEFAULT NULL,
  `cek3` varchar(1) DEFAULT NULL,
  `cek4` varchar(1) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `cekreject` varchar(1) DEFAULT NULL,
  `alamatahliwaris1` varchar(200) DEFAULT NULL,
  `nikahliwaris1` varchar(50) DEFAULT NULL,
  `tempatahliwaris1` varchar(100) DEFAULT NULL,
  `tgllahirahliwaris1` date DEFAULT NULL,
  `alamatahliwaris2` varchar(200) DEFAULT NULL,
  `nikahliwaris2` varchar(50) DEFAULT NULL,
  `tempatahliwaris2` varchar(100) DEFAULT NULL,
  `tgllahirahliwaris2` date DEFAULT NULL,
  `alamatahliwaris3` varchar(200) DEFAULT NULL,
  `nikahliwaris3` varchar(50) DEFAULT NULL,
  `tempatahliwaris3` varchar(100) DEFAULT NULL,
  `tgllahirahliwaris3` date DEFAULT NULL,
  `alamatahliwaris4` varchar(200) DEFAULT NULL,
  `nikahliwaris4` varchar(50) DEFAULT NULL,
  `tempatahliwaris4` varchar(200) DEFAULT NULL,
  `tgllahirahliwaris4` date DEFAULT NULL,
  `saksi1` varchar(100) DEFAULT NULL,
  `saksi2` varchar(100) DEFAULT NULL,
  `namakepling` varchar(100) DEFAULT NULL,
  `namalingk` varchar(50) DEFAULT NULL,
  `camat` varchar(100) DEFAULT NULL,
  `nip_camat` varchar(50) DEFAULT NULL,
  `pangkat_golcamat` varchar(100) DEFAULT NULL,
  `ahliwaris5` varchar(100) DEFAULT NULL,
  `alamatahliwaris5` varchar(200) DEFAULT NULL,
  `nikahliwaris5` varchar(50) DEFAULT NULL,
  `tempatahliwaris5` varchar(100) DEFAULT NULL,
  `tgllahirahliwaris5` date DEFAULT NULL,
  `ahliwaris6` varchar(100) DEFAULT NULL,
  `alamatahliwaris6` varchar(200) DEFAULT NULL,
  `nikahliwaris6` varchar(50) DEFAULT NULL,
  `tempatahliwaris6` varchar(100) DEFAULT NULL,
  `tgllahirahliwaris6` date DEFAULT NULL,
  `sebabkematian` varchar(50) DEFAULT NULL,
  `tglkematian` date DEFAULT NULL,
  `nosuratkematian` varchar(50) DEFAULT NULL,
  `tglsuratkematian` date DEFAULT NULL,
  `jlhanak` smallint(6) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `tempat_kematian` varchar(100) DEFAULT NULL,
  `foto_kk` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `tglmati_ayah` date DEFAULT NULL,
  `nik_ibu` varchar(20) DEFAULT NULL,
  `tglmati_ibu` date DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_wa`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of srtwaris
-- ----------------------------
BEGIN;
INSERT INTO `srtwaris` (`id_wa`, `id_users`, `tgldaftar`, `noreg`, `tglsurat`, `nosurat`, `nik`, `nama`, `tempat`, `tgllahir`, `jenkel`, `pekerjaan`, `status`, `agama`, `cekopt`, `tglcekopt`, `waktuopt`, `userentry`, `ceklurah`, `tglceklurah`, `namalurah`, `kodekel`, `namakel`, `kodekec`, `namakec`, `cekcetak`, `tglcetak`, `namacetak`, `level`, `reject`, `tglreject`, `waktucetak`, `tahun`, `bulan`, `waktulurah`, `keterangan`, `peruntukan`, `lurah`, `nip`, `pangkat_gol`, `ahliwaris1`, `ahliwaris2`, `ahliwaris3`, `ahliwaris4`, `cek1`, `cek2`, `cek3`, `cek4`, `alamat`, `cekreject`, `alamatahliwaris1`, `nikahliwaris1`, `tempatahliwaris1`, `tgllahirahliwaris1`, `alamatahliwaris2`, `nikahliwaris2`, `tempatahliwaris2`, `tgllahirahliwaris2`, `alamatahliwaris3`, `nikahliwaris3`, `tempatahliwaris3`, `tgllahirahliwaris3`, `alamatahliwaris4`, `nikahliwaris4`, `tempatahliwaris4`, `tgllahirahliwaris4`, `saksi1`, `saksi2`, `namakepling`, `namalingk`, `camat`, `nip_camat`, `pangkat_golcamat`, `ahliwaris5`, `alamatahliwaris5`, `nikahliwaris5`, `tempatahliwaris5`, `tgllahirahliwaris5`, `ahliwaris6`, `alamatahliwaris6`, `nikahliwaris6`, `tempatahliwaris6`, `tgllahirahliwaris6`, `sebabkematian`, `tglkematian`, `nosuratkematian`, `tglsuratkematian`, `jlhanak`, `warga_negara`, `tempat_kematian`, `foto_kk`, `foto_ktp`, `tglmati_ayah`, `nik_ibu`, `tglmati_ibu`, `nama_ibu`) VALUES (9, 7, '2022-03-14', '202203142333', '2022-03-14', '1234', '1231231231231231', 'Medan', NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kursi-teras-jati2.jpg', 'kursi-teras-jati3.jpg', '1111-11-11', '12312312313', '1111-11-11', 'bu');
COMMIT;

-- ----------------------------
-- Table structure for status_perkawinan
-- ----------------------------
DROP TABLE IF EXISTS `status_perkawinan`;
CREATE TABLE `status_perkawinan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_perkawinan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_perkawinan
-- ----------------------------
BEGIN;
INSERT INTO `status_perkawinan` (`id`, `status_perkawinan`) VALUES (1, 'Belum Menikah');
INSERT INTO `status_perkawinan` (`id`, `status_perkawinan`) VALUES (2, 'Menikah');
INSERT INTO `status_perkawinan` (`id`, `status_perkawinan`) VALUES (3, 'Janda');
INSERT INTO `status_perkawinan` (`id`, `status_perkawinan`) VALUES (4, 'Duda');
COMMIT;

-- ----------------------------
-- Table structure for tbl_identitas
-- ----------------------------
DROP TABLE IF EXISTS `tbl_identitas`;
CREATE TABLE `tbl_identitas` (
  `id_identitas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `nama_web` varchar(200) DEFAULT NULL,
  `lokasi` text,
  `no_wa` varchar(25) DEFAULT NULL,
  `logo_header` varchar(200) DEFAULT NULL,
  `kop_surat` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `dpt` decimal(10,0) DEFAULT NULL,
  `buka_menu` varchar(2) DEFAULT '0',
  `edit_menu` varchar(2) DEFAULT NULL,
  `hapus_menu` varchar(2) DEFAULT NULL,
  `tgl_hitung` date DEFAULT NULL,
  `jam_hitung` time DEFAULT NULL,
  PRIMARY KEY (`id_identitas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_identitas
-- ----------------------------
BEGIN;
INSERT INTO `tbl_identitas` (`id_identitas`, `nama`, `alamat`, `kode`, `logo`, `no_telp`, `facebook`, `twitter`, `email`, `nama_web`, `lokasi`, `no_wa`, `logo_header`, `kop_surat`, `url`, `dpt`, `buka_menu`, `edit_menu`, `hapus_menu`, `tgl_hitung`, `jam_hitung`) VALUES (1, 'Kominfo Binjai', 'Jl.  Veteran', NULL, 'logopemko.png', '', NULL, '', 'admin@binjaikota.go.id', 'eKelurahan', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.9315184900015!2d98.48119751421811!3d3.6031562973780407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3030d61febc2f041%3A0xfa6da0541fb6b89e!2sBinjai%20Command%20Center!5e0!3m2!1sen!2sid!4v1606189062124!5m2!1sen!2sid\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', '6285297163598', 'logo_man2.png', NULL, 'http://deskpilkada.binjaikota.go.id', 0, '1', '1', NULL, '2020-12-09', '05:51:00');
COMMIT;

-- ----------------------------
-- Table structure for tbl_setting
-- ----------------------------
DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `nama_setting` varchar(50) NOT NULL,
  `value` varchar(40) NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_setting
-- ----------------------------
BEGIN;
INSERT INTO `tbl_setting` (`id_setting`, `nama_setting`, `value`) VALUES (1, 'Tampil Menu', 'ya');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_users` int(2) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `images` text,
  `id_user_level` int(11) DEFAULT NULL,
  `is_aktif` enum('y','n') DEFAULT 'y',
  `email` varchar(200) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `id_tps` int(11) DEFAULT NULL,
  `id_kec` int(11) DEFAULT NULL,
  `id_kel` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` (`id_users`, `full_name`, `username`, `password`, `images`, `id_user_level`, `is_aktif`, `email`, `no_hp`, `alamat`, `id_tps`, `id_kec`, `id_kel`, `status`) VALUES (6, 'Admin', 'admin', '$2y$04$oix9vRtVJuxLo97ILZkWKuLh3tQu7CUyCLS9EXnMRM44xdSAIZ5kq', 'logopemko.png', 1, 'y', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id_users`, `full_name`, `username`, `password`, `images`, `id_user_level`, `is_aktif`, `email`, `no_hp`, `alamat`, `id_tps`, `id_kec`, `id_kel`, `status`) VALUES (7, 'bayu', 'bayu', '$2y$04$oix9vRtVJuxLo97ILZkWKuLh3tQu7CUyCLS9EXnMRM44xdSAIZ5kq', 'logopemko.png', 3, 'y', NULL, NULL, NULL, NULL, 1, 1, NULL);
INSERT INTO `user` (`id_users`, `full_name`, `username`, `password`, `images`, `id_user_level`, `is_aktif`, `email`, `no_hp`, `alamat`, `id_tps`, `id_kec`, `id_kel`, `status`) VALUES (8, 'op1', 'op1', '$2y$04$oix9vRtVJuxLo97ILZkWKuLh3tQu7CUyCLS9EXnMRM44xdSAIZ5kq', 'logopemko.png', 2, 'y', NULL, NULL, NULL, NULL, 1, 1, NULL);
INSERT INTO `user` (`id_users`, `full_name`, `username`, `password`, `images`, `id_user_level`, `is_aktif`, `email`, `no_hp`, `alamat`, `id_tps`, `id_kec`, `id_kel`, `status`) VALUES (9, 'lurah1', 'lurah1', '$2y$04$oix9vRtVJuxLo97ILZkWKuLh3tQu7CUyCLS9EXnMRM44xdSAIZ5kq', 'logopemko.png', 4, 'y', NULL, NULL, NULL, NULL, 1, 1, NULL);
INSERT INTO `user` (`id_users`, `full_name`, `username`, `password`, `images`, `id_user_level`, `is_aktif`, `email`, `no_hp`, `alamat`, `id_tps`, `id_kec`, `id_kel`, `status`) VALUES (10, 'op2', 'op2', '$2y$04$AjPiI5mccIKpzWuVE0.AMe/aH0PseQ3xYCi94Ms/.vryo6jWV0ZFO', 'tahapan.png', 1, 'y', 'op2@gmail.com', '123', 'medan', NULL, 1, 2, NULL);
COMMIT;

-- ----------------------------
-- Table structure for user_level
-- ----------------------------
DROP TABLE IF EXISTS `user_level`;
CREATE TABLE `user_level` (
  `id_user_level` int(2) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_user_level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_level
-- ----------------------------
BEGIN;
INSERT INTO `user_level` (`id_user_level`, `nama_level`) VALUES (1, 'Admin');
INSERT INTO `user_level` (`id_user_level`, `nama_level`) VALUES (2, 'Operator Kelurahan');
INSERT INTO `user_level` (`id_user_level`, `nama_level`) VALUES (3, 'User');
INSERT INTO `user_level` (`id_user_level`, `nama_level`) VALUES (4, 'Lurah');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;